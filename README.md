
# Gift Code Redemption (GCR) app
The GCR project consist of 3 Laravel/Lumen apps as follows: 

* GCR app

* GCR API

* User API

This is the GCR app created using Laravel 5.7.


## Project Setup
* Place the GCR app, GCR API, and User API in the root directory such as the **htdocs** folder if you are using XAMPP.
* GCR uses single-sign-on so unzip SAML.zip and place the whole **SAML** folder in the root directory as well.
* Create a database and import **gcr.sql** file to it.
* Create a second database and import **saml.sql** file to it.

## How to access GCR admin backoffice
* Enter **/admin** after the domain name in the address bar of your web browser as in following example:
```
http://yourdomain.com/admin
```

* To login use following credentials:
```
username: brand@brand.com
password: 123qwe
```
/**
 *
 * Version          Date            By              Description
 * -------------    ----------      -----------     -------------------------------------------
 * TMS_onBoarding	30May2016		Sunny J SHI     add tms_fallback.js
 */

if(typeof(TMS) == "undefined"){ 
window.HSBC=window.HSBC||{};HSBC.SITE=HSBC.SITE||{};HSBC.PAGE=HSBC.PAGE||{};HSBC.EXT=HSBC.EXT||{};HSBC.LOG=HSBC.LOG||{};HSBC.DCS=HSBC.DCS||{};window.WebTrends=undefined;window.DCSext=window.DCSext||{};window.dcsGetHSBCCookie=window.dcsGetHSBCCookie||function(name){return"";}
window.dcsVar=function(){};window.dcsMultiTrack=function(){};window.dcsMapHSBC=function(){};window.dcsMeta=function(){};window.dcsFunc=function(){};window.dcsTag=function(){};(function(){"use strict";window.TMS=window.TMS||{};var TMS=window.TMS;TMS.call_queue=[];TMS.copy=function(a,b,c){var utagLoaderGvCopy=function(d,e,f){e={};for(f in d){if(d.hasOwnProperty(f)&&typeof d[f]!="function")e[f]=d[f];}
return e;};b={};for(c in utagLoaderGvCopy(a)){if(a[c]instanceof Array){b[c]=a[c].slice(0);}else{b[c]=a[c];}}
return b;};TMS.trackEvent=function(event_name,data){var new_data=TMS.copy(data);TMS.call_queue.push({type:event_name,data:new_data});};TMS.trackView=function(data){var new_data=TMS.copy(data);TMS.call_queue.push({type:"view",data:new_data});};})();
var utag_data = utag_data || {};
};

var oldWinLoad = window.onload;
window.onload = function(e) {
                if (oldWinLoad) {
                        oldWinLoad();
                }
                                var siteSearch = document.getElementsByClassName("hsbcDivletSearch");
                                if (siteSearch.length>0) siteSearch[0].style.display="none";
}

function hideIS() {
        var siteSearchIS = document.getElementsByName("SearchPageForm");
        if (siteSearchIS.length>0) {
                console.log("siteSearch="+siteSearchIS);
                siteSearchIS[0].style.display="none";
                clearInterval(myVarIS);
        }
}
var myVarIS = null;

if (window.location.hostname.indexOf("hsbcamanah")>0) {
        myVarIS = setInterval(hideIS, 1000);
}
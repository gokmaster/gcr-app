
$(document).ready(function(){
    $( function() {
        $( "#start_date" ).datepicker({
            dateFormat: "dd-mm-yy",
            onSelect: function(selected) {
                $("#end_date").datepicker("option","minDate", selected)
            } // end date should not be less than start date
        });
        
        $( "#end_date" ).datepicker({
            dateFormat: "dd-mm-yy",
            onSelect: function(selected) {
                $("#start_date").datepicker("option","maxDate", selected)
            } // start date should not be greater than end date
        });
    });
});
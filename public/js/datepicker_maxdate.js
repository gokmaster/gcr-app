
$(document).ready(function(){
    $( function() {
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var today = (day<10 ? '0' : '') + day + '-' + (month<10 ? '0' : '') + month + '-' + d.getFullYear();

        $( "#start_date" ).datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: today,
            onSelect: function(selected) {
                $("#end_date").datepicker("option","minDate", selected)
            } // end date should not be less than start date
        });
        
        $( "#end_date" ).datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: today,
            onSelect: function(selected) {
                $("#start_date").datepicker("option","maxDate", selected)
            } // start date should not be greater than end date
        });
    });
});
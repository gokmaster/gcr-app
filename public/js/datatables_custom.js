
 $(document).ready(function(e) {

    $(".customsorting").DataTable({ 
        "paging": false, 
        "info": false, 
        "searching": false, 
      columnDefs: [
      { targets: 'no-sort', orderable: false }
    ]});
  
    $(".customsearchPerPageShow").DataTable({ 
        "paging": true, 
        "info": true, 
        "ordering": false, 
        "searching": false,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
  
    });
  
    $(".search_paginate_sort_selectperpage").DataTable({ 
        "searching": true,
        "paging": true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "order": [],
        columnDefs: [
            { targets: 'no-sort', orderable: false }
        ]
    });


    $(".search_paginate_selectperpage").DataTable({ 
        "searching": true,
        "paging": true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ordering": false,
    });


    $(".search_paginate_sort").DataTable({ 
        "searching": true,
        "paging": false,
        columnDefs: [
            { targets: 'no-sort', orderable: false }
        ]
    });

    });
<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page_box">
    <h3>Email Settings</h3>

    <?php if(session()->has('success_message')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php elseif(session()->has('fail_message')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('fail_message')); ?>

        </div>
    <?php endif; ?>
    <br>
    <form class="emailsettings_form" method="post" action="<?php echo e(route('emailsettings.generalreceiver.add')); ?>">
        <?php echo csrf_field(); ?>
        <a href="#" id="btnAddEmail" class="btn-email-add btn btn-primary">Add General Receiver Email</a>
        <div class="row div-add-email" style="display:none;"> 
            <div class="col-sm-4" >   
                <input type="email" id="add_email_input" class="form-control email" name="email" required>    
            </div>
            <div class="col-sm-4" >                  
                <button type="submit" class="btn btn-primary" >Add</button>
                <a href="#"  class="btn-addEmail-cancel btn btn-default" >Cancel</a>  
            </div>
        </div>
    </form>

    <br>

    <?php if(isset($emailsettings) && $emailsettings !== null): ?>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Email Type</th>
                    <th>Email Address</th>
                </tr>
            </thead>
            <tbody>
            
            <?php $__currentLoopData = $emailsettings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emailsetting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                    <?php echo e($emailsetting['email_type']); ?>

                </td>
                <td>
                    <div class="row">
                            <form class="emailsettings_form form-no-margin" method="post" action="<?php echo e(url('/admin/system/emailsettings/update')); ?>">
                                <div class="col-sm-4" >
                                    <input type="email" id="email_<?php echo e($emailsetting['email_settings_id']); ?>" class="form-control email" name="email" value="<?php echo e($emailsetting['email']); ?>" data-original-email="<?php echo e($emailsetting['email']); ?>" disabled required>
                                </div>   
                                <div class="col-sm-4" >        
                                    <a href="#" id="btnEdit_<?php echo e($emailsetting['email_settings_id']); ?>" class="btn-email-edit btn btn-primary" data-spnsave-id="spnSave_<?php echo e($emailsetting['email_settings_id']); ?>" data-email-id="email_<?php echo e($emailsetting['email_settings_id']); ?>" data-original-email="<?php echo e($emailsetting['email']); ?>" >Edit</a>
                                    <span id="spnSave_<?php echo e($emailsetting['email_settings_id']); ?>" class="spnSave" style="display:none;">
                                            <?php echo csrf_field(); ?>
                                            <input type="hidden" name="emailsettings_id" value="<?php echo e($emailsetting['email_settings_id']); ?>">
                                            <button type="submit" class="btn btn-primary" >Save</button>
                                        <a href="#"  class="btn-email-cancel btn btn-default"  data-btnEdit-id="btnEdit_<?php echo e($emailsetting['email_settings_id']); ?>" data-spnsave-id="spnSave_<?php echo e($emailsetting['email_settings_id']); ?>" data-email-id="email_<?php echo e($emailsetting['email_settings_id']); ?>" data-original-email="<?php echo e($emailsetting['email']); ?>" >Cancel</a>  
                                    </span>
                            </form>
                            <?php if( $emailsetting['is_default'] === 0): ?>
                                <form class="emailsettings_form form-no-margin" method="post" action="<?php echo e(route('emailsettings.delete')); ?>">
                                    <?php echo csrf_field(); ?>
                                    <input type="hidden" name="emailsettings_id" value="<?php echo e($emailsetting['email_settings_id']); ?>">
                                    <button type="submit" class="btn btn-primary" >Delete</button>
                                </form>
                            <?php endif; ?>
                        </div>
                    </div>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>


    <?php else: ?>
    <div class="alert alert-warning">No records found</div> 
    <?php endif; ?>   
    
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $(".emailsettings_form").validate();

        $( document ).ready(function() {

            $( "#btnAddEmail" ).on( "click", function(e) {
                // hide all save buttons
                $('#btnAddEmail').hide();
                $('.div-add-email').show();

                $('.email').attr("disabled", "disabled"); // disable all edit textboxes

                var current_button = e.target;
                
                $(current_button).hide();
                $("#add_email_input").removeAttr("disabled");  // enable current textbox

                 // hide all save buttons
                $('.spnSave').hide();
                $('.btn-email-edit').show();

                // revert all email textbox back to original value
                $('.email').each(function(i, obj) {
                    var originalEmail = $(obj).attr('data-original-email');
                    $(obj).val(originalEmail);
                });

            });

            $( ".btn-addEmail-cancel" ).on( "click", function(e) {
                var current_button = e.target;

                $("#btnAddEmail").show();
                $(".div-add-email").hide();
                $("#add_email_input").val(""); // clear value
            });


            $( ".btn-email-edit" ).on( "click", function(e) {
                // hide all save buttons
                $('.spnSave').hide();
                $('.btn-email-edit').show();

                // revert all email textbox back to original value
                $('.email').each(function(i, obj) {
                    var originalEmail = $(obj).attr('data-original-email');
                    $(obj).val(originalEmail);
                });

                $('.email').attr("disabled", "disabled"); 

                var current_button = e.target;

                var txtEmailId = $(current_button).attr('data-email-id');
                var spnSaveId = $(current_button).attr('data-spnsave-id');
                
                $(current_button).hide();
                $("#"+ spnSaveId ).show();
                $("#"+ txtEmailId ).removeAttr("disabled");  

                $('#btnAddEmail').show();
                $("#add_email_input").val(""); // clear value
                $('.div-add-email').hide();
            });


            $( ".btn-email-cancel" ).on( "click", function(e) {
                var current_button = e.target;

                var txtEmailId = $(current_button).attr('data-email-id');
                var spnSaveId = $(current_button).attr('data-spnsave-id');
                var btnEditId = $(current_button).attr('data-btnEdit-id');
                var originalEmail = $(current_button).attr('data-original-email');
                
                $("#"+ btnEditId ).show();
                $("#"+ spnSaveId ).hide();
                $("#"+ txtEmailId ).val(originalEmail); // revert back to original value
                $("#"+ txtEmailId ).attr("disabled", "disabled"); 
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
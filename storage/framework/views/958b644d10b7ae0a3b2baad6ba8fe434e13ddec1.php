<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php

$auth = new \SimpleSAML\Auth\Simple('example-sql');
$login_data = $auth->getAttributes();
$logged_in_admin_id = $login_data['id'];

?>

<?php $__env->startSection('content'); ?>
<div class="page_box">
    <h3>Users</h3>

    <h4>Users with Roles</h4>

    <?php if(isset($user_roles) && $user_roles !== null): ?>
        <table id="users_table" class="table table-hover search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Username</th>
                <th>Role</th>

                <?php if( in_array("edit user's role", $spData['permittedtasks']) ): ?>
                    <th>Action</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $user_roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($user_role['username']); ?></td>
                <td><?php echo e($user_role['role_name']); ?></td>
                <?php if( in_array("edit user's role", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( strcmp( $logged_in_admin_id , $user_role['user_id'] ) <> 0 ): ?>
                            <a href="<?php echo e(route('user_role.edit', [ 'user_id' =>  $user_role['user_id'],  'role_id' =>  $user_role['role_id'] ] )); ?>">Edit</a>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
  
    <?php else: ?>
    <div class="alert alert-warning">No records found</div> 
    <?php endif; ?>

    <br><br>

    <h4>Users without Roles</h4>

    <?php if(isset($unasigned_users) && $unasigned_users !== null): ?>
        <table id="users_table" class="table table-hover search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Username</th>
                <?php if( in_array("assign role to user", $spData['permittedtasks']) ): ?>
                    <th>Action</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $unasigned_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($user['username']); ?></td>
                <?php if( in_array("assign role to user", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( strcmp( $logged_in_admin_id , $user['id'] ) <> 0 ): ?>
                            <a href="<?php echo e(route('user_role.assign', [ 'user_id' =>  $user['id'] ] )); ?>">Assign Role</a>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>

        <br>
        
    <?php else: ?>
    <div class="alert alert-warning">No records found</div> 
    <?php endif; ?>

    
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
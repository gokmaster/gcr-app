<?php $__env->startSection('header'); ?>
<h3>Create Campaign</h3>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <?php if(session()->has('success_message')): ?>
                <div class="alert alert-success">
                    <?php echo e(session()->get('success_message')); ?>

                </div>
            <?php elseif(session()->has('fail_message')): ?>
                <div class="alert alert-danger">
                    <?php echo e(session()->get('fail_message')); ?>

                </div>
            <?php endif; ?>

            
            <div class="card-body">
            
                <form id="campaign_form" method="post" action="<?php echo e(url('/campaign/insert')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <label for="campaign_name">Campaign Title: 
                        </label>
                        <input type="text" class="form-control" id="campaign_title" name="campaign_title" required value="<?php echo e(old('campaign_title')); ?>" >
                    </div>
                    <div class="form-group">
                        <label for="campaign_name">Campaign Name for URL: 
                        </label>
                        <input type="text" class="form-control" id="campaign_name" name="campaign_name" required value="<?php echo e(old('campaign_name')); ?>" >
                        <span style="color:red;">(*Campaign name should not have spaces nor special characters)<span>
                    </div>
                                            
                    <div class="form-group">
                        <label for="campaign_provider">Bank: </label>
                        <select class="form-control" id="campaign_provider" name="campaign_provider">
                            <?php $__currentLoopData = $banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($bank['bank_id']); ?>"
                                <?php if( strcmp($bank['bank_id'], old('campaign_provider') ) === 0 ): ?>
                                    selected="selected"
                                <?php endif; ?>
                            ><?php echo e($bank['name']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div> 

                    <div class="form-group shadow-textarea">
                        <label for="description">Description</label>
                        <textarea class="form-control z-depth-1" id="description" name="description" rows="3" placeholder="Enter description here"><?php echo e(old('description')); ?></textarea>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                        <label>Start date:</label>
                        <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" value="<?php echo e(old('start_date')); ?>" required>
                        </div>
                        <div class="col-md-5">
                        <label>End date:</label>
                        <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" value="<?php echo e(old('end_date')); ?>" required>
                        </div>
                    </div>

                    <br>

                    <b>What method of code generation do you wish to use for this Campaign ?</b><br>
                    <label class="radio-inline"><input type="radio" name="verification_type" value="1" required 
                        <?php if( strcmp(old('verification_type'), 1) === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        Unique Code
                    </label>
                   <!--  <label class="radio-inline"><input type="radio" name="verification_type" value="2"
                        <?php if(strcmp(old('verification_type'), 2) === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        Unique code & last 6-digit (GCR)
                    </label> -->
                    <label class="radio-inline" style=""><input type="radio" name="verification_type" value="3"
                        <?php if(strcmp(old('verification_type'), 3) === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        Unique code & last 6-digit (Bank)
                    </label>
                    <br>
                    <div id="unique_code_qty_div" class="form-group" <?php if(!old('unique_code_qty') ): ?> style="display:none;" <?php endif; ?>>
                        <br>
                        <label for="unique_code_qty">How many unique-codes to generate?: </label>
                        <input type="number" class="form-control" id="unique_code_qty" name="unique_code_qty" value="<?php echo e(old('unique_code_qty')); ?>" min="1" max="100000" required>
                    </div>
                    <br>
                    <b>Unique Code Type:</b><br>
                    <label class="radio-inline" style=""><input type="radio" name="unique-code-type" value="alnumcap" required
                        <?php if(strcmp(old('unique-code-type'), "alnum") === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        12345abcdeABCDE
                    </label>
                    <label class="radio-inline" style=""><input type="radio" name="unique-code-type" value="alnum"
                        <?php if(strcmp(old('unique-code-type'), "alpha") === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        12345abcde
                    </label>
                    <br><br>

                    <b>How many characters should unique code have?</b><br>
                    <label class="radio-inline" style=""><input type="radio" name="unique-code-length" value="4" required
                        <?php if(strcmp(old('unique-code-length'), 4) === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        4
                    </label>
                    &nbsp &nbsp
                    <label class="radio-inline" style=""><input type="radio" name="unique-code-length" value="6" required
                        <?php if(strcmp(old('unique-code-length'), 6) === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        6
                    </label>
                    &nbsp &nbsp
                    <label class="radio-inline" style=""><input type="radio" name="unique-code-length" value="8"
                        <?php if(strcmp(old('unique-code-length'), 8) === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        8
                    </label>
                    &nbsp &nbsp
                    <label class="radio-inline" style=""><input type="radio" name="unique-code-length" value="10"
                        <?php if(strcmp(old('unique-code-length'), 10) === 0 ): ?>
                            checked
                        <?php endif; ?> >
                        10
                    </label>
                    <br><br>
                    
                    <button id="btnCreateCampaign" type="submit" class="btn btn-primary">Create campaign</button>    
                    &nbsp
                    <a href="<?php echo e(url('/campaign/all')); ?>" class="btn btn-default" >Cancel</a>
                </form>
                
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $("#campaign_form").validate({
            rules: {
                unique_code_qty: {
                    min: 1,
                    digits: true,
                }
            }
        });

        $(document).ready(function () {
            $('input:radio[name=verification_type]').change(function () {
                if ($("input[name='verification_type']:checked").val() == 1
                    || $("input[name='verification_type']:checked").val() == 3 ) {
                    $("#unique_code_qty_div").show();
                } else {
                    $("#unique_code_qty_div").hide();
                }
            });
        });
    </script>

    <script src="<?php echo e(asset('js/datepicker.js')); ?>" ></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
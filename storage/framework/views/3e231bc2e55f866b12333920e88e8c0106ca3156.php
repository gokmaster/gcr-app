<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page_box">
    <h3>Excel Uploads History</h3>

        <?php if(session()->has('success_message')): ?>
            <div class="alert alert-success">
                <?php echo e(session()->get('success_message')); ?>

            </div>
        <?php elseif(session()->has('fail_message')): ?>
            <div class="alert alert-danger">
                <?php echo e(session()->get('fail_message')); ?>

            </div>
        <?php endif; ?>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>File Uploaded</th>
                <th>Unique Code Quantity</th>
                <th>Uploaded by</th>
                <th>Uploaded at</th>

            </tr>
            </thead>
            <tbody>            
            <?php $__currentLoopData = $uploads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                   <?php echo e($upload['bank_name']); ?>

                </td>
                <td>
                   <?php echo e($upload['campaign_title']); ?>

                </td>
                <td>
                   <a href="<?php echo e(route('campaign.uploaded-excel_download' , [ 'filename' => $upload['filename'] ] )); ?>"><?php echo e($upload['filename']); ?></a>
                </td>
                <td>
                   <?php echo e($upload['unique_code_qty']); ?>

                </td>
                <td>
                   <?php echo e($upload['username']); ?>

                </td>
                <td>
                   <?php echo e($upload['created_on']); ?>

                </td>       
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>  
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
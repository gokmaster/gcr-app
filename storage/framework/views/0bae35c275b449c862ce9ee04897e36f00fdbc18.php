<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page_box" >
    <h3>Successful Redemptions</h3>

     <?php if(session()->has('success_message')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php elseif(session()->has('fail_message')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('fail_message')); ?>

        </div>
    <?php endif; ?>

    <!-- <form id="searchform" method="get" action="<?php echo e(url('/admin/report/successful_redemptions_search')); ?>">
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <select class="form-control" id="sel1" name="search_option">
                        <option value="campaign">Campaign</option>
                        <option value="bank">Bank</option>
                        <option value="transaction_id">Transaction ID</option>
                        <option value="name">Name</option>
                        <option value="postcode">Postcode</option>
                        <option value="city">City</option>
                        <option value="state">State</option>
                        <option value="mobile_no">Mobile no</option>
                        <option value="last_6_digit">Last 6-digit Credit Card/IC</option>
                        <option value="unique_code">Unique Code</option>
                        <option value="address1">Address 1</option>
                        <option value="address2">Address 2</option>
                    </select>
                </div>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
                </div>
                <div class="col-md-2">
                    <input id="btnSearch" type="submit" value="Search" class="btn btn-primary" >
                </div>	
            </div>
        </div>
    </form> -->
<?php if(count($redemptions) > 0): ?>
    <?php if(!empty($_GET['keyword']) && !empty($_GET['search_option'])): ?>
        <?php ($table = 'customsearchPerPageShow'); ?>
    <?php else: ?>
        <?php ($table = ''); ?>
<?php endif; ?>
 
        <table class="table table-hover <?php echo e($table); ?>">
            <thead>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Address line 1</th>
                <th>Address line 2</th>
                <th>Address line 3</th>
                <th>Address line 4</th>
                <th>Postcode</th>
                <th>City</th>
                <th>State</th>
                <th>Home no</th>
                <th>Office no</th>
                <th>Mobile no</th>
                <th>Last 6-digit Credit Card/IC</th>
                <th>Unique Code</th>
                <th>Product SKU</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $redemptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $redemption): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e(date('d-m-Y h:i:s', strtotime($redemption['date_redeemed']))); ?></td>
                <td><?php echo e($redemption['bank_name']); ?></td>
                <td><?php echo e($redemption['campaign_name']); ?></td>
                <td><?php echo e($redemption['order_no']); ?></td>
                <td><?php echo e($redemption['name']); ?></td>
                <td><?php echo e($redemption['contact_no']); ?></td>
                <td><?php echo e($redemption['address1']); ?></td>
                <td><?php echo e($redemption['address2']); ?></td>
                <td><?php echo e($redemption['address3']); ?></td>
                <td><?php echo e($redemption['address4']); ?></td>
                <td><?php echo e($redemption['zip_code']); ?></td>
                <td><?php echo e($redemption['city']); ?></td>
                <td><?php echo e($redemption['state']); ?></td>
                <td><?php echo e($redemption['home_no']); ?></td>
                <td><?php echo e($redemption['office_no']); ?></td>
                <td><?php echo e($redemption['mobile']); ?></td>
                <td><?php echo e($redemption['ic_creditcard_no']); ?></td>
                <td><?php echo e($redemption['unique_code']); ?></td>
                <td><?php echo e($redemption['sku']); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>

            <tfoot>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Address line 1</th>
                <th>Address line 2</th>
                <th>Address line 3</th>
                <th>Address line 4</th>
                <th>Postcode</th>
                <th>City</th>
                <th>State</th>
                <th>Home no</th>
                <th>Office no</th>
                <th>Mobile no</th>
                <th>Last 6-digit Credit Card/IC</th>
                <th>Unique Code</th>
                <th>Product SKU</th>
            </tr>
            </tfoot>
        </table>
  
        <!--  <form id="exportExcelform" method="post" action="<?php echo e(url('/admin/successful/redemptions/export')); ?>">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-5">                    
                    <label>Start date:</label>
                        <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" required>
                    </div>
                    <div class="col-md-5">
                    <label>End date:</label>
                        <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" required>
                    </div>
                    <div class="col-md-2" style="top: 25px;">
                        <button type="submit" class="btn btn-primary">Export to Excel</button>
                    </div>
                </div>
            </div>
        </form> -->

        
<?php else: ?>
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
<?php endif; ?>

 </div>


<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Successful Redemptions'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Successful Redemptions'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            });
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });

            // floating horizontal scrollbar
            $(".page_box").floatingScroll();
        } );
    </script>

    <script src="<?php echo e(asset('js/datepicker_maxdate.js')); ?>" ></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!DOCTYPE html>

<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

   <!-- Floating Scrollbar -->
  <script src="<?php echo e(asset('vendor/floatingscroll/jquery.floatingscroll.js')); ?>" type="text/javascript"></script>
   

  <link rel="stylesheet" href="<?php echo e(asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte/dist/css/AdminLTE.min.css')); ?>">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte/dist/css/skins/skin-blue.min.css')); ?>">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!--Datatables-->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <!--Floating Scrollbar-->
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/floatingscroll/jquery.floatingscroll.less')); ?>">

  <!-- My custom css -->
  <link href="<?php echo e(asset('css/custom_css.css')); ?>?3" rel="stylesheet">
  <link href="<?php echo e(asset('css/admin_dboard.css')); ?>?2" rel="stylesheet">

     <?php echo $__env->yieldContent('assets'); ?>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
   
    <!-- Logo -->
    <a href="<?php echo e(route('admin')); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GCR Backoffice</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php 
            $auth = new \SimpleSAML\Auth\Simple('example-sql');
            //$logoutUrl = url('/admin/logout');
            $logoutUrl = $auth->getLogoutURL();

              // check if logged in
             
          ?>
            <?php if( $auth->isAuthenticated() ): ?> 
                  <!-- User Account Menu -->
                  <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="hidden-xs"><?php echo e($spData['adminUser']['first_name']); ?></span>
                    </a>
                  </li>
                  <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="hidden-xs">Profile</span>
                    </a>
                    <ul class="dropdown-menu" style="width:140px;">
                      <!-- Menu Footer-->
                      <li class="user-footer" style="background-color:#f4fffd;border:solid 1px #e5e5e5;">
                          <a href="<?php echo e(route('admin.profile')); ?>">
                            <div><?php echo e($spData['adminUser']['first_name']); ?></div>
                          </a>
                      </li>
                      <li class="user-footer" style="background-color:#f4fffd;border:solid 1px #e5e5e5;">
                          <a href="<?php echo e($logoutUrl); ?>">
                            <div>Sign out</div>
                          </a>
                      </li>
                    </ul>
                  </li><!--user account menu-->
            <?php else: ?> 
                <li><a>Login</a></li>
            <?php endif; ?>
           
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">

        <?php if( isset($spData['menu'])  && $spData['menu'] !== null ): ?>
          <?php $__currentLoopData = $spData['menu']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$menuItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if(strcmp( $menuItem['has_submenu'], 0) === 0 ): ?>
                <!-- Has no sub-menu -->
                <li><a href="<?php echo e(route( $menuItem['route_name'] )); ?>">
                  <i><img width="15px" height="14px" src="<?php echo e(URL::to('/')); ?>/images/icons/menu/<?php echo e($menuItem['menu_icon']); ?>" alt="menu-icon"></i> &nbsp<span><?php echo e($menuItem['menu_label']); ?></span></a>
                </li>
              <?php else: ?>
                <!-- Has sub-menu -->
                <li class="treeview">
                    <a href="#">
                      <i><img width="15px" height="14px" src="<?php echo e(URL::to('/')); ?>/images/icons/menu/<?php echo e($menuItem['submenu'][0]['menu_icon']); ?>" alt="menu-icon"></i> &nbsp <span><?php echo e($key); ?></span>
                      <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      <?php $__currentLoopData = $menuItem['submenu']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $submenu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="<?php echo e(route($submenu['route_name'])); ?>"><?php echo e($submenu['menu_label']); ?></a></li>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </li>
              <?php endif; ?>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <?php echo $__env->yieldContent('header'); ?>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <?php echo $__env->yieldContent('content'); ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('adminlte/dist/js/adminlte.min.js')); ?>"></script>

<!--jQuery Validation Plugin -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!--Scripts need for Datatables -->
<script src="<?php echo e(asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script> 

<script src="<?php echo e(asset('js/datatables_custom.js')); ?>"></script>

<?php echo $__env->yieldContent('scripts'); ?>
</body>
</html>
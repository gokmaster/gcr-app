<?php $__env->startSection('assets'); ?>
 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
  <h3>Profile</h3>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="panel panel-default">
  <div class="panel-heading"><b>Name</b></div>
  <div class="panel-body"><?php echo e($user['first_name']); ?></div>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><b>Email</b></div>
  <div class="panel-body"><?php echo e($user['email']); ?></div>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><b>Username</b></div>
  <div class="panel-body"><?php echo e($user['username']); ?></div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page_box">
    <h3>Excel Files for upload into Tri-e portal</h3>
   
<?php if(count($excelfiles) > 0): ?>
   
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Date Generated</th>
                <th>Redemption Date</th>
                <th>Batch No</th>
                <th>Filename</th>
                <th>Download Count</th>
                <th>Email Status</th>
                <th>Email Time</th>
      <!--           <th>Excel Checked</th> -->
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $excelfiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $excelfile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e(date('d-m-Y', strtotime( $excelfile['upload_date']))); ?></td>
                <td><?php echo e(date('d-m-Y', strtotime( $excelfile['redemption_date']))); ?></td>
                <td><?php echo e($excelfile['batch_no']); ?></td>
                <td><a href="<?php echo e(route('redemption.excel_download', ['uploaded_file_id' => $excelfile['uploaded_file_id'], 'filename' => $excelfile['filename'] ])); ?>"><?php echo e($excelfile['filename']); ?></a></td>
                <td>
                    <?php if( $excelfile['download_count'] !== null ): ?>
                        <a class="view-download-history" data-uploadfile-id="<?php echo e($excelfile['uploaded_file_id']); ?>"  style="cursor:pointer;"><?php echo e($excelfile['download_count']); ?></a>
                    <?php else: ?> 
                        0
                    <?php endif; ?>
                </td>
                <td><?php echo e($excelfile['email_status']); ?></td>
                <td><?php echo e(date('H:i', strtotime( $excelfile['email_time']))); ?></td>
                <!-- <td>
                    <?php if( strcmp($excelfile['checked'] , 0) == 0 ): ?>
                        <span style="color:red;">failed</span>
                    <?php elseif( strcmp($excelfile['checked'] , 1) == 0 ): ?>
                        <span style="color:green;">success</span>
                    <?php else: ?>
                        <span style="color:grey;">check pending<span>
                    <?php endif; ?>
                </td> -->
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>

        <?php echo e($excelfiles->links()); ?>

    
<?php else: ?>
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
<?php endif; ?>

</div>


<!-- Modal -->
<div id="downloadHistoryModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="max-height:500px; overflow:auto;">
      <div class="modal-body" id="downloadHistoryModalBody" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $( ".view-download-history" ).on( "click", function(e) {

            var current_button = e.target;

            var uploadedfileID = $(current_button).attr('data-uploadfile-id');

            $.post("<?php echo e(route('report.excel-download-history')); ?>", { _token: "<?php echo e(csrf_token()); ?>", uploadedfileID: uploadedfileID}, function(result){
                
                var history = result.history;

                var tableHtml = "<table class='table' >"+
                                    "<tr>"+
                                        "<th>Downloaded by</th>"+
                                        "<th>Downloaded at</th>"+
                                    "</tr>";

                $.each( history, function( key, value ) {
                    tableHtml += "<tr>"+
                                        "<td>"+ value.username +"</td>"+
                                        "<td>"+ value.created_at +"</td>"+
                                    "</tr>";
                });

                tableHtml += "</table>"

                $('#downloadHistoryModalBody').html(tableHtml);
                $('#downloadHistoryModal').modal('show');
            });

        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


<div class="page_box">
    <h3>Unutilised Unique Codes</h3>
    <?php if(session()->has('success_message')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php elseif(session()->has('fail_message')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('fail_message')); ?>

        </div>
    <?php endif; ?>
    
<?php if(count($unused_codes) > 0): ?>
    
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Number of Unutilised Unique Codes</th>
                <th>Details</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $unused_codes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($s['bank_name']); ?></td>
                <td><?php echo e($s['campaign_name']); ?></td>
                <td>
                    <?php echo e($s['unused_qty']); ?>

                </td>
                <td><a href="<?php echo e(route('report.unuseduniquecodeforcampaign' , ['campaign_id'=> $s['campaign_id'] ] )); ?>">View details</a>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>

            <tfoot>
                <tr>
                    <th><input type="text" placeholder="Search Bank" /></th>
                    <th><input type="text" placeholder="Search Campaign" /></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>

       <!--  <form id="exportExcelform" method="post" action="<?php echo e(url('/admin/report/unused_unique_code/export')); ?>">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-2" style="top: 25px;">
                        <button type="submit" class="btn btn-primary">Export to Excel</button>
                    </div>
                </div>
            </div>
        </form> -->

       
<?php else: ?>
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
<?php endif; ?>

 </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Unutilised Unique Codes'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Unutilised Unique Codes'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
          /*   $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } ); */
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        } );
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page_box">

    <h3>Audit Logs: Campaign</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign Title</th>
                <th>Description</th>
                <th>Excel Uploaded</th>
                <th>Deleted</th>
                <th>Has Products</th>
                <th>Action Type</th>
                <th>Actioned By</th>
                <th>Log Updated At</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($log['title']); ?></td>
            <td><?php echo e($log['description']); ?></td>
            <td>
                <?php if( strcmp( $log['excel_uploaded'] , 0) == 0 ): ?> 
                    No
                <?php else: ?>
                    Yes
                <?php endif; ?>
            </td>
            <td>
                <?php if( strcmp( $log['deleted'] , 0) == 0 ): ?> 
                    No
                <?php else: ?>
                    Yes
                <?php endif; ?>
            </td>
            <td>
                <?php if( strcmp( $log['has_products'] , 0) == 0 ): ?> 
                    No
                <?php else: ?>
                    Yes
                <?php endif; ?>
            </td>
            <td><?php echo e($log['action_type']); ?></td>
            <td><?php echo e($log['username']); ?></td>
            <td><?php echo e($log['log_timestamp']); ?></td>
            <td><?php echo e($log['created_on']); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
            
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
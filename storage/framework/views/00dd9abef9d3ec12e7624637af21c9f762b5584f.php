<?php $__env->startSection('header'); ?>
<h3>Role: <?php echo e($selected[0]['role_name']); ?></h3>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <?php if(session()->has('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session()->get('success_message')); ?>

                    </div>
                <?php elseif(session()->has('fail_message')): ?>
                    <div class="alert alert-danger">
                        <?php echo e(session()->get('fail_message')); ?>

                    </div>
                <?php endif; ?>

                <div class="card-body">
                
                    <form id="roles_form" >
                        <?php echo csrf_field(); ?>
                                               
                         <div class="box" style="padding:5px;">
                            <h4>BANK</h4>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if( $task['label'] == "bank" ): ?>
                                    <label class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="<?php echo e($task['task_id']); ?>" disabled>&nbsp <?php echo e($task['task_name']); ?>

                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                         <div class="box" style="padding:5px;">
                            <h4>CAMPAIGN</h4>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if( $task['label'] == "campaign" ): ?>
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="<?php echo e($task['task_id']); ?>" disabled>&nbsp <?php echo e($task['task_name']); ?>

                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>PRODUCT</h4>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if( $task['label'] == "product" ): ?>
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="<?php echo e($task['task_id']); ?>" disabled>&nbsp <?php echo e($task['task_name']); ?>

                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>REPORT</h4>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if( $task['label'] == "report" ): ?>
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="<?php echo e($task['task_id']); ?>" disabled>&nbsp <?php echo e($task['task_name']); ?>

                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>ROLE</h4>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if( $task['label'] == "role" ): ?>
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="<?php echo e($task['task_id']); ?>" disabled>&nbsp <?php echo e($task['task_name']); ?>

                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>SYSTEM</h4>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if( $task['label'] == "system" ): ?>
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="<?php echo e($task['task_id']); ?>" disabled>&nbsp <?php echo e($task['task_name']); ?>

                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>USERS</h4>
                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if( $task['label'] == "users" ): ?>
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="<?php echo e($task['task_id']); ?>" disabled>&nbsp <?php echo e($task['task_name']); ?>

                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <input id="role_id" name="role_id" type="hidden" value="<?php echo e($role_id); ?>" >
                        <br>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>

<input id="selected_tasks" type="hidden" value="<?php echo e(json_encode($selected)); ?>" >


<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $("#roles_form").validate();

        $( document ).ready(function() {

            var selected_tasks = $("#selected_tasks").val();
            selected_tasks_object = JSON.parse(selected_tasks);
                        
            $('.task_checkbox').each(function(i, obj) {

                $.each( selected_tasks_object, function( key, task ) {
                    var task_id =  $(obj).val();

                    if ( task['task_id'] == task_id) {
                        $(obj).prop('checked', true);
                    }
                });
               
            });
           
        });
    </script>

    <script src="<?php echo e(asset('js/role_create.js')); ?>" ></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
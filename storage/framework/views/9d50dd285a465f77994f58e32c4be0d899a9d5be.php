<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page_box" >
    <h3>Email Status Report</h3>

     <?php if(session()->has('success_message')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php elseif(session()->has('fail_message')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('fail_message')); ?>

        </div>
    <?php endif; ?>

 

 
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Email</th>
                <th>Timestamp</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $emailstatus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e(date('d-m-Y h:i:s', strtotime($d['date_redeemed']))); ?></td>
                <td><?php echo e($d['bank_name']); ?></td>
                <td><?php echo e($d['campaign']); ?></td>
                <td><?php echo e($d['order_no']); ?></td>
                <td><?php echo e($d['customer_name']); ?></td>
                <td><?php echo e($d['contact_no']); ?></td>
                <td><?php echo e($d['email']); ?></td>
                <td><?php echo e($d['created_on']); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>

            <tfoot>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Email</th>
                <th>Timestamp</th>
            </tr>
            </tfoot>
        </table>
        
 </div>


<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Email Status Report'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Email Status Report'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            });
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });

            // floating horizontal scrollbar
            //$(".page_box").floatingScroll();
        } );
    </script>

  
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
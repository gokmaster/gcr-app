<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container">
    <h3>Edit Campaign</h3>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <?php if(session()->has('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session()->get('success_message')); ?>

                    </div>
                <?php elseif(session()->has('fail_message')): ?>
                    <div class="alert alert-danger">
                        <?php echo e(session()->get('fail_message')); ?>

                    </div>
                <?php endif; ?>
           
                <div class="card-body">
                    <b>Created by:</b> <?php echo e($campaign[0]['createdbyusername']); ?> 
                    &nbsp &nbsp &nbsp &nbsp
                    <b>Created on:</b> <?php echo e(date('d M, Y', strtotime($campaign[0]['created_on']))); ?>

                    <br><br>
                
                    <form id="campaign_form" method="post" action="<?php echo e(url('/campaign/update')); ?>">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" id="campaign_id" name="campaign_id" value="<?php echo e($campaign[0]['campaign_id']); ?>">
                        <div class="form-group">
                            <label for="campaign_name">Campaign Title: 
                            </label>
                            <input type="text" class="form-control" id="campaign_title" name="title" required  
                                <?php if( old('title') ): ?> 
                                    value="<?php echo e(old('title')); ?>" 
                                <?php else: ?> 
                                    value="<?php echo e($campaign[0]['title']); ?>" 
                                <?php endif; ?>
                                >
                        </div>
                        <div class="form-group">
                            <label for="campaign_name">Campaign Name for url: </label>
                            <input type="text" class="form-control" id="campaign_name" name="campaign_name" required disabled
                                value="<?php echo e($campaign[0]['name']); ?>"
                            >
                        </div>
                                              
                        <div class="form-group">
                            <label for="campaign_provider">Bank: </label>
                            <select class="form-control" id="campaign_provider" name="campaign_provider"  disabled>
                                <?php $__currentLoopData = $banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($bank['bank_id']); ?>" 
                                    <?php if($campaign[0]['bank_id'] === $bank['bank_id']): ?>
                                        selected="selected"                             
                                    <?php endif; ?>>
                                    <?php echo e($bank['name']); ?>

                                </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div> 

                        <div class="form-group shadow-textarea">
                            <label for="description">Description</label>
                            <textarea class="form-control z-depth-1" id="description" name="description" rows="3" placeholder="Enter description here"><?php if(old('description')): ?> <?php echo e(old('description')); ?> <?php else: ?> <?php echo e($campaign[0]['description']); ?> <?php endif; ?></textarea>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                            <label>Start date:</label>
                            <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" value="<?php echo e(date('d-m-Y', strtotime($campaign[0]['start_date']))); ?>"  required>
                            </div>
                            <div class="col-md-5">
                            <label>End date:</label>
                            <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" value="<?php echo e(date('d-m-Y', strtotime($campaign[0]['end_date']))); ?>" required>
                            </div>
                        </div>

                        <br>

                        <div class="form-group">
                            <label for="campaign_name">Reason for edit: </label>
                            <input type="text" class="form-control" id="reason" name="reason" required
                                value="<?php echo e(old('reason')); ?>"
                            >
                        </div>

                        <br>

                        <button id="btnSaveEdits" type="submit" class="btn btn-primary">Save Edits</button> 
                        &nbsp
                        <a href="<?php echo e(url('/campaign/all')); ?>" class="btn btn-default" >Cancel</a>  
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $("#campaign_form").validate();
    </script>

    <script src="<?php echo e(asset('js/datepicker.js')); ?>" ></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
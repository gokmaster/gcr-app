<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <?php if(session()->has('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session()->get('success_message')); ?>

                    </div>
                <?php elseif(session()->has('fail_message')): ?>
                    <div class="alert alert-danger">
                        <?php echo e(session()->get('fail_message')); ?>

                    </div>
                <?php endif; ?>

                 <?php if(count($errors) > 0): ?>
                    <div class="alert alert-danger">
                        Image upload failed.
                        <ul>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <div class="card-header"><h2>Add Product for Campaign <?php echo e($campaign[0]['name']); ?></h2></div>

                <div class="card-body">
                
                    <form id="product_form" method="post" action="<?php echo e(url('/product/insert')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" id="campaign_id" name="campaign_id" value="<?php echo e($campaign[0]['campaign_id']); ?>">
                        <div class="form-group">
                            <label for="product_name">Product Name: </label>
                            <input type="text" class="form-control" id="product_name" name="product_name" value="<?php echo e(old('product_name')); ?>" required>
                        </div>

                        <div class="form-group shadow-textarea">
                            <label for="description">Product Description</label>
                            <textarea class="form-control z-depth-1" id="description" name="description" rows="3" placeholder="Write product description here"><?php echo e(old('description')); ?></textarea>
                        </div>
                                              
                        <div class="form-group">
                            <label for="sku">SKU: </label>
                            <input type="text" class="form-control" id="sku" name="sku"  value="<?php echo e(old('sku')); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="category">Category: </label>
                            <input type="text" class="form-control" id="category" name="category"  value="<?php echo e(old('category')); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="price">Price: </label>
                            <input type="number" class="form-control" id="price" name="price" min="0" step="0.01"  value="<?php echo e(old('price')); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="price">Ordered Quantity: </label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="0" step="1"  value="<?php echo e(old('quantity')); ?>" required>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01">Upload Picture</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="imageupload" id="imageupload" required>
                            </div>
                        </div>
                        <br>

                        <?php if(session()->has('success_message')): ?>
                            <a href="<?php echo e(route('product.add', ['campaign_id' => $campaign[0]['campaign_id'] ])); ?>" class="btn btn-primary" >Add another product</a> 
                            &nbsp
                            <a href="<?php echo e(route('campaign.all')); ?>" class="btn btn-primary" >Back to Campaigns</a> 
                            &nbsp
                            <a href="<?php echo e(route('product.all')); ?>" class="btn btn-primary" >Back to Products</a> 
                            &nbsp
                        <?php else: ?>
                            <button id="btnAddProduct" type="submit" class="btn btn-primary">Add Product</button>
                            &nbsp
                            <a href="<?php echo e(url('/campaign/all')); ?>" class="btn btn-default" >Cancel</a> 
                        <?php endif; ?>  

                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $('#product_form').validate({
            rules: {
                description: {
                    maxlength: 255,                 
                },
                
            },
            messages: {},
            errorElement : 'div',
            errorLabelContainer: '.errorTxt'
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page_box">
    <h3>Download unique codes for Campaign: <?php echo e($batches[0]['campaign_title']); ?> </h3>

        <?php if(session()->has('success_message')): ?>
            <div class="alert alert-success">
                <?php echo e(session()->get('success_message')); ?>

            </div>
        <?php elseif(session()->has('fail_message')): ?>
            <div class="alert alert-danger">
                <?php echo e(session()->get('fail_message')); ?>

            </div>
        <?php endif; ?>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Batch</th>
                <th>Created on</th>
                <th>Unique Code Quantity</th>
                <th>Download</th>
            </tr>
            </thead>
            <tbody>            
            <?php $__currentLoopData = $batches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $batch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                   <?php echo e($batch['bank_name']); ?>

                </td>
                <td>
                   <?php echo e($batch['campaign_title']); ?>

                </td>
                <td>
                   <?php echo e($batch['batch']); ?>

                </td>
                <td>
                   <?php echo e($batch['created_on']); ?>

                </td>
                <td>
                   <?php echo e($batch['unique_code_qty']); ?>

                </td>
                <td>
                    <a href="<?php echo e(route('unique_code.excel_download.onebatch' , [ 'campaign_id' => $batch['campaign_id'], 'batch' => $batch['batch'] ])); ?>">
                       Download
                    </a>
                </td>         
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>  
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
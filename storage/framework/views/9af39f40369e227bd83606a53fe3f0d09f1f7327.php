<?php $__env->startSection('header'); ?>
<h3>Edit Unique Code Expiry</h3>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <?php if(session()->has('success_message')): ?>
                <div class="alert alert-success">
                    <?php echo e(session()->get('success_message')); ?>

                </div>
            <?php elseif(session()->has('fail_message')): ?>
                <div class="alert alert-danger">
                    <?php echo e(session()->get('fail_message')); ?>

                </div>
            <?php endif; ?>

            <b>Campaign:</b> <?php echo e($details['campaign']); ?>  <br>
            <b>Batch:</b> <?php echo e($details['batch']); ?>


            
            
            <div class="card-body">
            
                <form id="campaign_form" method="post" action="<?php echo e(route('campaign.edit_unique_code_expiry_post')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>

                    <input type="hidden" id="campaign_id" name="campaign_id" value="<?php echo e($details['campaign_id']); ?>">

                    <input type="hidden" id="campaign" name="campaign" value="<?php echo e($details['campaign']); ?>">

                    <input type="hidden" id="batch" name="batch" value="<?php echo e($details['batch']); ?>">
                    
                    <b>Expiry date: &nbsp; </b>
                    <label class="radio-inline" style=""><input type="radio" name="expiry_enabled" value="1" required
                        <?php if( $details['expiry_enabled'] === 1 ): ?>
                            checked
                        <?php endif; ?> >
                        Enable
                    </label>
                    <label class="radio-inline" style=""><input type="radio" name="expiry_enabled" value="0"
                        <?php if( $details['expiry_enabled'] === 0 ): ?>
                            checked
                        <?php endif; ?> > 
                        Disable
                    </label>
                    <br><br>

                    <div id="expiry-div" <?php if($details['expiry_enabled'] === 0): ?> style="display:none;" <?php endif; ?> >
                        <br>
                        <label>Valid From:</label>
                        <input type="text" class="form-control" id="expiry_startdate" name="expiry_startdate" placeholder="Select Date" autocomplete="off"  value="<?php echo e(date( 'd-m-y', strtotime($details['expiry_startdate']))); ?>" required >

                        <br>
                        <div class="form-group">
                            <label for="days_from_expiry_startdate">Valid for how many days: </label>
                            <input type="number" class="form-control" id="days_from_expiry_startdate" name="days_from_expiry_startdate" value="<?php echo e($details['days_from_expiry_startdate']); ?>" min="0" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="remark">Remark: </label>
                        <input type="text" class="form-control" id="remark" name="remark" value="<?php echo e($details['remark']); ?>"  required>
                    </div>
                  
                    <br>
                    
                    <button id="btnAddUniqueCode" type="submit" class="btn btn-primary">Save</button>  
                    &nbsp
                    <a href="<?php echo e(url('/campaign/all')); ?>" class="btn btn-default" >Cancel</a>  
                </form>
                
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $("#campaign_form").validate();

        $( function() {
           
           $( "#expiry_startdate" ).datepicker({
               dateFormat: "dd-mm-yy",
           });
       });

        $(document).ready(function () {
            $("input:radio[name='expiry_enabled']").change(function () {
                if ($("input[name='expiry_enabled']:checked").val() == 1 ) {
                    $("#expiry-div").show();
                } else {
                    $("#expiry-div").hide();
                }
            });
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
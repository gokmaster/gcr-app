<?php $__env->startSection('header'); ?>
  <h1>Manually Generate Excel file for upload to Tri-e portal</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php if(session()->has('success_message')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php elseif(session()->has('fail_message')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('fail_message')); ?>

        </div>
    <?php endif; ?>

<form id="exportExcelform" method="post" action="<?php echo e(url('/export/manual_excel_generate_for_upload_post')); ?>">
    <?php echo csrf_field(); ?>
    <div class="form-group">
        <div class="row">
            <div class="col-md-5">
            <label>Select date to generate excel for:</label>
                <input type="text" class="form-control" id="end_date" name="end_date" placeholder="Select Date" autocomplete="off" required>
            </div>
            <div class="col-md-2" style="top: 25px;">
                <button type="submit" class="btn btn-primary">Export to Excel</button>
            </div>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $('#exportExcelform').validate();

        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var today = (day<10 ? '0' : '') + day + '-' + (month<10 ? '0' : '') + month + '-' + d.getFullYear();

         $( function() {
           
            $( "#end_date" ).datepicker({
                dateFormat: "dd-mm-yy",
                maxDate: today
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


<div class="page_box">
    <h3>SMS Notification Status</h3>
    <?php if(session()->has('success_message')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php elseif(session()->has('fail_message')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('fail_message')); ?>

        </div>
    <?php endif; ?>
    
   <!--  <form id="searchform" method="get" action="<?php echo e(url('/admin/report/sms_status_search')); ?>">
        <?php echo csrf_field(); ?>
        <div class="form-group">
        <div class="row">
            <div class="col-md-3">
            <select class="form-control" id="sel1" name="search_option">
                <option value="campaign">Campaign</option>
                <option value="bank">Bank</option>
                <option value="sms_transact_id">SMS Transaction ID</option>
                <option value="name">Name</option>
                <option value="mobile_no">Contact no</option>
                <option value="ic_creditcard_no">Last 6-digit Credit Card/IC</option>
                <option value="unique_code">Unique Code</option>
                <option value="sms_status">SMS status</option>
                <option value="gift_code">Error Description</option>
                <option value="sms_content">SMS content</option>
                <option value="redemption_id">Redemption Transaction ID</option>
            </select>
            </div>
            <div class="col-md-7">
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
            </div>
            <div class="col-md-2">
            <input id="btnSearch" type="submit" value="Search" class="btn btn-primary" >
            </div>
        </div>
        </div>
    </form> -->
    
    <?php if(count($sms_status) > 0): ?>
        <?php if(!empty($_GET['keyword']) && !empty($_GET['search_option'])): ?>
            <?php ($table = 'customsearchPerPageShow'); ?>
        <?php else: ?>
            <?php ($table = ''); ?>
    <?php endif; ?>

        <table class="table table-hover <?php echo e($table); ?>">
            <thead>
            <tr>
                <th>SMS Sent Date</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>SMS Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Last 6-digit of credit card/ IC</th>
                <th>Unique Code Input</th>
                <th>SMS status</th>
                <th>Gift Code</th>
                <th>SMS content</th>
                <th>Redemption Transaction ID</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $sms_status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e(date('d-m-Y', strtotime( $s['sent_date']))); ?></td>
                <td><?php echo e($s['bank_name']); ?></td>
                <td><?php echo e($s['campaign_name']); ?></td>
                <td><?php echo e($s['sms_transact_id']); ?></td>
                <td><?php echo e($s['customer_name']); ?></td>
                <td><?php echo e($s['mobile_no']); ?></td>
                <td><?php echo e($s['ic_creditcard_no']); ?></td>
                <td><?php echo e($s['unique_code']); ?></td>
                <td><?php echo e($s['sms_status']); ?></td>
                <td><?php echo e($s['gift_code']); ?></td>
                <td><?php echo e($s['sms_content']); ?></td>
                <td><?php echo e($s['redemption_id']); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
            <tfoot>
            <tr>
                <th>SMS Sent Date</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>SMS Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Last 6-digit of credit card/ IC</th>
                <th>Unique Code Input</th>
                <th>SMS status</th>
                <th>Gift Code</th>
                <th>SMS content</th>
                <th>Redemption Transaction ID</th>
            </tr>
            </tfoot>
        </table>

       <!--  <form id="exportExcelform" method="post" action="<?php echo e(url('/admin/report/sms/status/export')); ?>">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-5">                    
                    <label>Start date:</label>
                        <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" required>
                    </div>
                    <div class="col-md-5">
                    <label>End date:</label>
                        <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" required>
                    </div>
                    <div class="col-md-2" style="top: 25px;">
                        <button type="submit" class="btn btn-primary">Export to Excel</button>
                    </div>
                </div>
            </div>
        </form> -->

        <?php echo e($sms_status->links()); ?>

<?php else: ?>
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
<?php endif; ?>

 </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'SMS Notification Status'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'SMS Notification Status'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        } );
    </script>

    <script src="<?php echo e(asset('js/datepicker_maxdate.js')); ?>" ></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page_box">
    <h3>Roles</h3>

    <?php if(isset($roles) && $roles !== null): ?>
        <table class="table table-hover search_paginate_sort_selectperpage">
            <thead>
            <tr>
                <th>Role</th>
                <th>Users</th>
                <?php if( in_array("edit role", $spData['permittedtasks']) || in_array("delete role", $spData['permittedtasks'])): ?>
                    <th>Action</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            
            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                    <?php if( in_array("view role tasks", $spData['permittedtasks']) ): ?>
                        <a href="<?php echo e(route('role.view_role_tasks', [ 'role_id' =>  $role['role_id'] ] )); ?>"><?php echo e($role['role_name']); ?></a> 
                    <?php else: ?>
                        <?php echo e($role['role_name']); ?>

                    <?php endif; ?>
                </td>
                <td>
                    <?php if($role['user_count'] > 0): ?>
                        <a href="<?php echo e(route('role.view_users_of_role', [ 'role_id' =>  $role['role_id'] , 'role_name' =>  $role['role_name'] ] )); ?>">View users</a>
                    <?php endif; ?>
                </td>
                <?php if( in_array("edit role", $spData['permittedtasks']) || in_array("delete role", $spData['permittedtasks'])): ?>
                    <td>
                        <?php if( in_array("edit role", $spData['permittedtasks']) ): ?>
                            <a href="<?php echo e(route('role.edit_form', [ 'role_id' =>  $role['role_id'] ] )); ?>">Edit</a> 
                        <?php endif; ?>    
                        <?php if( in_array("edit role", $spData['permittedtasks']) && in_array("delete role", $spData['permittedtasks']) ): ?>
                            |
                        <?php endif; ?>
                        <?php if( in_array("delete role", $spData['permittedtasks']) ): ?>    
                            <a href="<?php echo e(route('role.delete_form', [ 'role_name' =>  $role['role_name'] , 'role_id' =>  $role['role_id'] ] )); ?>">Delete</a>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">No records found</div> 
    <?php endif; ?>

    <br>

    <div class="page_bottom_div"> 
        <?php if( in_array("create role", $spData['permittedtasks']) ): ?>
            <a class="btn btn-primary" href="<?php echo e(url('/admin/role/create')); ?>">Create Role</a>
        <?php endif; ?>
    </div>

   
    
</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
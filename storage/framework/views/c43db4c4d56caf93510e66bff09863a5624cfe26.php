

<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
  
    <title>HSBC Get-A-Gift of Your Choice* Redemption</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="<?php echo e(asset('css/app.css')); ?>?2" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom_css.css')); ?>?2" rel="stylesheet">

     <!-- Scripts -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    
    <?php echo $__env->yieldContent('assets'); ?>
</head>
<body>
    <section class="header-container">
        <div id="topbar">Powered by 3ex</div>
        <div id="header-main">
        <div class="container" style="width:auto;">
            <div class="d-flex flex-row"> <img style="margin-top:8px;height:70% !important;width:auto;" class="hsbc-logo" src="<?php echo e(URL::to('/')); ?>/images/site_img/hsbc.png"> 
        </div>
        </div>
    </section>
   
    <?php echo $__env->yieldContent('content'); ?>
   

    <?php echo $__env->yieldContent('scripts'); ?>
</body>

 
       
</html>

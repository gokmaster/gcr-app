<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page_box">
    <h3>Inventory</h3>
  <!--   <form id="searchform" method="get" action="<?php echo e(url('/admin/report/inventory_search')); ?>">
        <?php echo csrf_field(); ?>
        <div class="form-group">
        <div class="row">
            <div class="col-md-3">
            <select class="form-control" id="sel1" name="search_option">
                <option value="product_name">Product Name</option>
                <option value="campaign">Campaign</option>
                <option value="bank">Bank</option>
            </select>
            </div>
            <div class="col-md-7">
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
            </div>
            <div class="col-md-2">
            <input id="btnSearch" type="submit" value="Search" class="btn btn-primary" >
            </div>
        </div>
        </div>
    </form> -->


<?php if(count($inventory) > 0): ?>
    <?php if(!empty($_GET['keyword']) && !empty($_GET['search_option'])): ?>
    <?php ($table = 'customsearchPerPageShow'); ?>
    <?php else: ?>
    <?php ($table = ''); ?>
<?php endif; ?> 

        <table class="table table-hover <?php echo e($table); ?>">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Item</th>
                <th>Ordered</th>
                <th>Total Redeemed</th>
                <th>Balance</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $inventory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $inv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($inv['bank_name']); ?></td>
                <td><?php echo e($inv['campaign_name']); ?></td>
                <td><?php echo e($inv['product_name']); ?></td>
                <td><?php echo e($inv['ordered_qty']); ?></td>
                <td><?php echo e($inv['total_redeemed']); ?></td>
                <td><?php echo $inv['ordered_qty'] - $inv['total_redeemed']; ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>

            <tfoot>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Item</th>
                <th>Ordered</th>
                <th>Total Redeemed</th>
                <th>Balance</th>
            </tr>
            </tfoot>
        </table>

    <!--      <form id="exportExcelform" method="get" action="<?php echo e(url('/admin/report/inventory/export')); ?>">
            <?php echo csrf_field(); ?>
            <div class="form-group">
            <div class="row">
                <div class="col-md-2" style="top: 25px;">
                <input type="hidden" name="inventory_export" value="1" />
                <button type="submit" class="btn btn-primary">Export to Excel</button>
                </div>
            </div>
            </div>
        </form> -->

<?php else: ?>
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
<?php endif; ?>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Inventory'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Inventory'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        } );
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
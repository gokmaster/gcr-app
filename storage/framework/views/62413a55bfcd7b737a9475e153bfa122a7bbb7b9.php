<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page_box">

    <h3>Audit Logs: Unique Code</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Batch</th>
                <th>Unique Code Quantity</th>
                <th>Status</th>
                <th>Action Type</th>
                <th>Approved/Rejected By</th>
                <th>Log Updated At</th>
                <th>Created on</th>
                <th>Created By</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($log['campaign_name']); ?></td>
                <td><?php echo e($log['bank_name']); ?></td>
                <td><?php echo e($log['batch']); ?></td>
                <td><?php echo e($log['unique_code_qty']); ?></td>
                <td><?php echo e($log['status']); ?></td>
                <td><?php echo e($log['action_type']); ?></td>
                <td><?php echo e($log['approved_by']); ?></td>
                <td><?php echo e($log['log_timestamp']); ?></td>
                <td><?php echo e($log['created_on']); ?></td>
                <td><?php echo e($log['username']); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>

    <hr>

    <h3>Audit Logs: Unique Code Downloads</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Batch</th>
                <th>Unique Code Quantity</th>
                <th>Action Type</th>
                <th>Log Updated At</th>
                <th>Downloaded By</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $downloadlogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($log['campaign_name']); ?></td>
                <td><?php echo e($log['bank_name']); ?></td>
                <td><?php echo e($log['batch']); ?></td>
                <td><?php echo e($log['unique_code_qty']); ?></td>
                <td><?php echo e($log['action_type']); ?></td>
                <td><?php echo e($log['log_timestamp']); ?></td>
                <td><?php echo e($log['username']); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>

    <hr>

    <h3>Audit Logs: Unique Code Uploads</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>File Uploaded</th>
                <th>Unique Code Quantity</th>
                <th>Action Type</th>
                <th>Log Updated At</th>
                <th>Uploaded By</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $uploadlogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($log['campaign_title']); ?></td>
                <td><?php echo e($log['bank_name']); ?></td>
                <td><?php echo e($log['filename']); ?></td>
                <td><?php echo e($log['unique_code_qty']); ?></td>
                <td><?php echo e($log['action_type']); ?></td>
                <td><?php echo e($log['log_timestamp']); ?></td>
                <td><?php echo e($log['username']); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
        
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
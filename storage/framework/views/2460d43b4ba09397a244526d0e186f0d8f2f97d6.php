<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <?php if(session()->has('success_message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session()->get('success_message')); ?>

                    </div>
                <?php elseif(session()->has('fail_message')): ?>
                    <div class="alert alert-danger">
                        <?php echo e(session()->get('fail_message')); ?>

                    </div>
                <?php endif; ?>

                 <?php if(count($errors) > 0): ?>
                    <div class="alert alert-danger">
                        Image upload failed.
                        <ul>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <div class="card-header"><h2>Edit Product</h2></div>

                <div class="card-body">
                
                    <form id="product_form" method="post" action="<?php echo e(url('/product/update')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" id="product_id" name="product_id" value="<?php echo e($product[0]['product_id']); ?>">
                        <div class="form-group">
                            <label for="product_name">Product Name: </label>
                            <input type="text" class="form-control" id="product_name" name="product_name" required 
                                <?php if(session()->has('success_message') || session()->get('fail_message')  ): ?>
                                    value="<?php echo e(old('product_name')); ?>"
                                <?php else: ?>
                                    value="<?php echo e($product[0]['product_name']); ?>"
                                <?php endif; ?>
                            >
                        </div>

                        <div class="form-group shadow-textarea">
                            <label for="exampleFormControlTextarea6">Product Description</label>
                            <textarea class="form-control z-depth-1" id="description" name="description" rows="3" placeholder="Write product description here"><?php if(session()->has('success_message') || session()->get('fail_message')  ): ?><?php echo e(old('description')); ?> <?php else: ?> <?php echo e($product[0]['description']); ?> <?php endif; ?></textarea>
                        </div>
                                              
                        <div class="form-group">
                            <label for="sku">SKU: </label>
                            <input type="text" class="form-control" id="sku" name="sku" required 
                                <?php if(session()->has('success_message') || session()->get('fail_message')  ): ?>
                                    value="<?php echo e(old('sku')); ?>"
                                <?php else: ?>
                                    value="<?php echo e($product[0]['sku']); ?>"
                                <?php endif; ?>
                            >
                        </div>

                        <div class="form-group">
                            <label for="category">Category: </label>
                            <input type="text" class="form-control" id="category" name="category" required 
                                <?php if(session()->has('success_message') || session()->get('fail_message')  ): ?>
                                    value="<?php echo e(old('category')); ?>"
                                <?php else: ?>
                                    value="<?php echo e($product[0]['categories']); ?>"
                                <?php endif; ?>
                            >
                        </div>

                        <div class="form-group">
                            <label for="price">Price: </label>
                            <input type="number" class="form-control" id="price" name="price" min="0" step="0.01" required 
                                <?php if(session()->has('success_message') || session()->get('fail_message')  ): ?>
                                    value="<?php echo e(old('price')); ?>"
                                <?php else: ?>
                                    value="<?php echo e($product[0]['price']); ?>"
                                <?php endif; ?>
                            >
                        </div>

                        <div class="form-group">
                            <label for="price">Quantity remaining: </label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="0" step="1" required disabled 
                                    value="<?php echo e($product[0]['quantity']); ?>"
                            >
                        </div>

                        <div class="form-group">
                            <label for="price">Additional Quantity Ordered: </label>
                            <input type="number" class="form-control" id="quantity_ordered" name="quantity_ordered" step="1" 
                            >
                        </div>

                        <?php if( count($productsOrdered) > 0 ): ?> 
                            <h4>History of Product Inventory Orders Added</h4>
                            <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Date Added</th>
                                    <th>Quantity</th>
                                    <th>Added by</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $productsOrdered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ft): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e(date('d-m-Y h:i:s', strtotime( $ft['created_at']))); ?></td>
                                    <td><?php echo e($ft['quantity']); ?></td>
                                    <td><?php echo e($ft['username']); ?></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        <?php endif; ?>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01">Upload Picture</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="imageupload" id="imageupload">
                            </div>
                        </div>
                        <br>

                        <button id="btnAddProduct" type="submit" class="btn btn-primary">Save Edits</button>  
                        &nbsp
                        <a href="<?php echo e(url('/product/all')); ?>" class="btn btn-default" >Cancel</a>  
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
         $('#product_form').validate({
            rules: {
                description: {
                    maxlength: 255,                 
                },
                
            },
            messages: {},
            errorElement : 'div',
            errorLabelContainer: '.errorTxt'
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
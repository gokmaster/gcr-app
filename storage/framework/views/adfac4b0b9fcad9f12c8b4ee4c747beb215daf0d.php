<?php $__env->startSection('header'); ?>
<h3>Products for Campaign <?php echo e($products[0]['campaign_name']); ?> </h3>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="box-body table-responsive no-padding">
        <table class="table table-hover customsorting">
            <thead>
            <tr>
                    <th>Product Name</th>
                    <th>Campaign Name</th>
                    <th>Bank Name</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>SKU</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Quantity</th>
                <?php if( in_array("edit product", $spData['permittedtasks']) || in_array("delete product", $spData['permittedtasks'])): ?>
                    <th>Action</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($product['product_name']); ?></a></td>
                <td><?php echo e($product['campaign_name']); ?></td>
                <td><?php echo e($product['name']); ?></td>
                <td><img width="auto" height="90px" src="<?php echo e(URL::to('/')); ?>/<?php echo e($product['filepath']); ?>"></td>
                <td><?php echo e($product['description']); ?></td>
                <td><?php echo e($product['sku']); ?></td>
                <td><?php echo e($product['categories']); ?></td>
                <td><?php echo e(number_format((float)$product['price'], 2, '.', '')); ?></td>
                <td><?php echo e($product['quantity']); ?></td>
                <?php if( in_array("edit product", $spData['permittedtasks']) || in_array("delete product", $spData['permittedtasks'])): ?>
                <td> 
                    <form method="post" class="form-no-margin" action="<?php echo e(action('ProductController@delete_post')); ?>">
                        <?php echo csrf_field(); ?>  
                        <?php if( in_array("edit product", $spData['permittedtasks']) ): ?>
                            <a href="<?php echo e(route('product.edit', ['product_id' => $product['product_id']])); ?>" class="btn btn-link inline-button" >Edit</a>
                        <?php endif; ?>

                        <?php if( in_array("edit product", $spData['permittedtasks']) && in_array("delete product", $spData['permittedtasks'])): ?>
                            |
                        <?php endif; ?>

                        <?php if( in_array("delete product", $spData['permittedtasks']) ): ?>        
                            <input type="hidden" id="product_id" name="product_id" value="<?php echo e($product['product_id']); ?>">                                   
                            <button id="btnDeleteProduct" type="submit" class="btn btn-link inline-button">Disable</button>
                        <?php endif; ?>
                    </form>
                </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <?php echo e($products->links()); ?>


    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
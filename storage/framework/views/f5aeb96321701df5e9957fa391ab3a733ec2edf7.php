<?php $__env->startSection('assets'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page_box">

    <h3>Campaigns</h3>
    <?php if(session()->has('success_message')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success_message')); ?>

        </div>
    <?php elseif(session()->has('fail_message')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('fail_message')); ?>

        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-8">
            Filter campaigns: &nbsp &nbsp
            <label  class="chkbox_container" >
                <input id="chkbox-active" type="checkbox" class="filter-checkbox" name="filter[]" value="1" checked>Active
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp

            <label  class="chkbox_container" >
                <input id="chkbox-expired" type="checkbox" class="filter-checkbox" name="filter[]" value="2" checked>Expired
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp

            <label  class="chkbox_container" >
                <input id="chkbox-disabled" type="checkbox" class="filter-checkbox" name="filter[]" value="3" checked>Disabled
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp
        </div>

        <div class="col-sm-4">
            <div class="page_bottom_div">
                <?php if( in_array("add campaign", $spData['permittedtasks']) ): ?>
                    <a class="btn btn-primary" href="<?php echo e(url('/campaign/create')); ?>">Create Campaign</a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div id="active-div">
        <h4>Active Campaigns</h4>

        <table class="table search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Description</th>
                <th>Created on</th>
                <th>Start Date</th>
                <th>End Date</th>
                <?php if( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks'])): ?>
                    <th>Action</th>
                <?php endif; ?> 
                <?php if( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) ): ?>     
                    <th>Products</th>
                <?php endif; ?>
                <?php if( in_array("upload unique code masterlist", $spData['permittedtasks']) ): ?>
                    <th>Upload Excel</th>
                <?php endif; ?>
                <th>Unique-code type</th>
                <?php if( in_array("download unique codes", $spData['permittedtasks']) ): ?>
                    <th>Unique-codes</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $campaigns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $campaign): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                    <a href="<?php echo e(route('campaign.view', ['campaign_name' => $campaign['name']])); ?>" target="_blank">
                        <?php echo e($campaign['title']); ?>

                    </a>
                </td>
                <td><?php echo e($campaign['bank_name']); ?></td>
                <td><?php echo e($campaign['description']); ?></td>
                <td><?php echo e($campaign['created_on']); ?></td>
                <td><?php echo e($campaign['start_date']); ?></td>
                <td><?php echo e($campaign['end_date']); ?></td>

                <?php if( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks'])): ?>
                    <td>
                        <?php if( in_array("edit campaign", $spData['permittedtasks']) ): ?>
                            <a href="<?php echo e(route('campaign.edit', ['campaign_id' => $campaign['campaign_id']])); ?>">
                                Edit
                            </a>
                        <?php endif; ?>
                        <?php if( in_array("edit campaign", $spData['permittedtasks']) && in_array("delete campaign", $spData['permittedtasks']) ): ?>
                            |
                        <?php endif; ?>
                        <?php if( in_array("delete campaign", $spData['permittedtasks']) ): ?>
                            <form class="form-no-margin" method="post" action="<?php echo e(url('/campaign/delete')); ?>">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" name="campaign_id" value="<?php echo e($campaign['campaign_id']); ?>">                                           
                                    <button type="submit" class="inline-button btn btn-link">
                                        Disable
                                    </button>
                            </form>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>

                <?php if( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( in_array("add product", $spData['permittedtasks']) ): ?>
                            <a href="<?php echo e(route('product.add', ['campaign_id' => $campaign['campaign_id']])); ?>">
                                Add Product
                            </a>
                        <?php endif; ?>
                        <?php if( in_array("add product", $spData['permittedtasks']) &&  in_array("view all products", $spData['permittedtasks']) &&  $campaign['has_products'] == 1): ?>
                            |
                        <?php endif; ?>
                        <?php if( in_array("view all products", $spData['permittedtasks']) ): ?>
                            <?php if( $campaign['has_products'] == 1 ): ?>
                                <a href="<?php echo e(route('product.show_campaign_products', ['campaign_id' => $campaign['campaign_id']])); ?>">
                                        View
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
                
                <?php if( in_array("upload unique code masterlist", $spData['permittedtasks']) ): ?>
                    <?php if( strcmp($campaign['verification_type'], 2) ===0 ||  strcmp($campaign['verification_type'], 3) ===0 ): ?>
                        <td>
                            
                                <?php if( $campaign['excel_uploaded'] == 0 ): ?>
                                    <a href="<?php echo e(route('campaign.excel_uploadform_bank_masterlist', ['campaign_name' => $campaign['name'], 'campaign_id' => $campaign['campaign_id'], 'verificationType' => $campaign['verification_type'] ])); ?>">
                                        Upload
                                    </a>
                                <?php else: ?>
                                    <a href="<?php echo e(route('campaign.excel_uploadform_bank_masterlist', ['campaign_name' => $campaign['name'], 'campaign_id' => $campaign['campaign_id'], 'verificationType' => $campaign['verification_type'] ])); ?>">
                                        Upload
                                    </a> | 
                                    <a href="<?php echo e(route('campaign.exceluploadhistory', ['campaign_id' => $campaign['campaign_id'] ])); ?>">View History</a>
                                <?php endif; ?>
                            
                        </td>
                    <?php else: ?>
                        <td>N/A</td>
                    <?php endif; ?>
                <?php endif; ?>

                <td>
                    <?php if( in_array("add unique codes", $spData['permittedtasks']) ): ?>
                        <?php if( $campaign['verification_type'] == 2 ): ?>
                            N/A
                        <?php else: ?>
                            <a href="<?php echo e(route('campaign.add_unique_code', ['campaign_id' => $campaign['campaign_id'] , 'campaign_name' => $campaign['name']])); ?>">
                                Add
                            </a>
                        <?php endif; ?>
                        |
                    <?php endif; ?>

                    <?php if( in_array("approve unique codes", $spData['permittedtasks']) ): ?>
                        <a href="<?php echo e(route('campaign.approve_unique_code', ['campaign_name' => $campaign['name'] , 'campaign_id' => $campaign['campaign_id']])); ?>">
                            View Approval
                        </a>
                        |
                    <?php endif; ?>

                    <?php if( strcmp($campaign['verification_type'], 1) ===0 ): ?>
                        Unique-code only
                    <?php elseif( strcmp($campaign['verification_type'], 2) ===0 ): ?>
                        Unique-code & last 6-digit (GCR)
                    <?php elseif( strcmp($campaign['verification_type'], 3) ===0 ): ?>
                        Unique-code & last 6-digit (Bank)
                    <?php endif; ?>
                </td>

                <?php if( in_array("download unique codes", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( $campaign['batch_count'] === 0 ): ?>
                            N/A
                        <?php elseif( $campaign['batch_count'] === 1 ): ?>
                            <a href="<?php echo e(route('unique_code.excel_download.onebatch' , [ 'campaign_id' => $campaign['campaign_id'], 'batch' => $campaign['batch']] )); ?>">
                                Download
                            </a>
                        <?php elseif( 
                            $campaign['verification_type'] == 1 || $campaign['verification_type'] == 3 
                            || ($campaign['verification_type'] == 2 && $campaign['excel_uploaded'] == 1) 
                            && $campaign['batch_count']  > 0 
                        ): ?>
                            <a href="<?php echo e(route('unique_code.download.batches', ['campaign_id' => $campaign['campaign_id']])); ?>">
                                Download
                            </a>
                        <?php elseif( $campaign['verification_type'] == 2 ): ?>
                            <?php if( $campaign['excel_uploaded'] <> 1 ): ?>
                                N/A
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <hr>
    </div>

    <div id="expired-div">
        <h4>Expired Campaigns</h4>
        
        <table id="expired-camapigns-table" class="table search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Description</th>
                <th>Created on</th>
                <th>Start Date</th>
                <th>End Date</th>
                <?php if( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks'])): ?>
                    <th>Action</th>
                <?php endif; ?> 
                <?php if( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) ): ?>     
                    <th>Products</th>
                <?php endif; ?>
                <?php if( in_array("upload unique code masterlist", $spData['permittedtasks']) ): ?>
                    <th>Upload Excel</th>
                <?php endif; ?>
                <th>Unique-code type</th>
                <?php if( in_array("download unique codes", $spData['permittedtasks']) ): ?>
                    <th>Unique-codes</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $expiredCampaigns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $campaign): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="expired_campaign" >
                <td>
                    <?php echo e($campaign['title']); ?>

                </td>
                <td><?php echo e($campaign['bank_name']); ?></td>
                <td><?php echo e($campaign['description']); ?></td>
                <td><?php echo e($campaign['created_on']); ?></td>
                <td><?php echo e($campaign['start_date']); ?></td>
                <td><?php echo e($campaign['end_date']); ?></td>

                <?php if( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks'])): ?>
                    <td>
                        <?php if( in_array("edit campaign", $spData['permittedtasks']) ): ?>
                            <a href="<?php echo e(route('campaign.edit', ['campaign_id' => $campaign['campaign_id']])); ?>">
                                Edit
                            </a>
                        <?php endif; ?>
                        <?php if( in_array("edit campaign", $spData['permittedtasks']) && in_array("delete campaign", $spData['permittedtasks']) ): ?>
                            |
                        <?php endif; ?>
                        <?php if( in_array("delete campaign", $spData['permittedtasks']) ): ?>
                            <form class="form-no-margin" method="post" action="<?php echo e(url('/campaign/delete')); ?>">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" name="campaign_id" value="<?php echo e($campaign['campaign_id']); ?>">                                           
                                    <button type="submit" class="inline-button btn btn-link">
                                        Disable
                                    </button>
                            </form>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>

                <?php if( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( in_array("add product", $spData['permittedtasks']) ): ?>
                                Add Product
                        <?php endif; ?>
                        <?php if( in_array("add product", $spData['permittedtasks']) &&  in_array("view all products", $spData['permittedtasks']) &&  $campaign['has_products'] == 1): ?>
                            |
                        <?php endif; ?>
                        <?php if( in_array("view all products", $spData['permittedtasks']) ): ?>
                            <?php if( $campaign['has_products'] == 1 ): ?>
                                <a href="<?php echo e(route('product.show_campaign_products', ['campaign_id' => $campaign['campaign_id']])); ?>">
                                        View
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
                
                <?php if( in_array("upload unique code masterlist", $spData['permittedtasks']) ): ?>
                    <?php if( strcmp($campaign['verification_type'], 2) ===0 ||  strcmp($campaign['verification_type'], 3) ===0 ): ?>
                        <td>
                            <?php if( $campaign['excel_uploaded'] == 0 ): ?>
                                Upload
                            <?php else: ?>
                                Uploaded
                            <?php endif; ?>  
                        </td>
                    <?php else: ?>
                        <td>N/A</td>
                    <?php endif; ?>
                <?php endif; ?>

                <td>
                    <?php if( in_array("add unique codes", $spData['permittedtasks']) ): ?>
                        <?php if( $campaign['verification_type'] == 2 ): ?>
                            N/A
                        <?php else: ?>
                            Add   
                        <?php endif; ?>
                        |
                    <?php endif; ?>

                    <?php if( in_array("approve unique codes", $spData['permittedtasks']) ): ?>
                            View Approval
                        |
                    <?php endif; ?>

                    <?php if( strcmp($campaign['verification_type'], 1) ===0 ): ?>
                        Unique-code only
                    <?php elseif( strcmp($campaign['verification_type'], 2) ===0 ): ?>
                        Unique-code & last 6-digit (GCR)
                    <?php elseif( strcmp($campaign['verification_type'], 3) ===0 ): ?>
                        Unique-code & last 6-digit (Bank)
                    <?php endif; ?>
                </td>

                <?php if( in_array("download unique codes", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( $campaign['verification_type'] == 1 || $campaign['verification_type'] == 3 
                            || ($campaign['verification_type'] == 2 && $campaign['excel_uploaded'] == 1 )
                        ): ?>
                            Download
                        <?php elseif( $campaign['verification_type'] == 2 ): ?>
                            <?php if( $campaign['excel_uploaded'] <> 1 ): ?>
                                N/A
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <hr>
    </div>

    <div id="disabled-div">
        <h4>Disabled Campaigns</h4>
        <table id="disabled-camapigns-table" class="table search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Description</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Created on</th>
                <?php if( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks'])): ?>
                    <th>Action</th>
                <?php endif; ?> 
                <?php if( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) ): ?>     
                    <th>Products</th>
                <?php endif; ?>
                <?php if( in_array("upload unique code masterlist", $spData['permittedtasks']) ): ?>
                    <th>Upload Excel</th>
                <?php endif; ?>
                <th>Unique-code type</th>
                <?php if( in_array("download unique codes", $spData['permittedtasks']) ): ?>
                    <th>Unique-codes</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $disabledCampaigns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $campaign): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class='disabled_row' >
                <td>
                    <?php echo e($campaign['title']); ?>

                </td>
                <td><?php echo e($campaign['bank_name']); ?></td>
                <td><?php echo e($campaign['description']); ?></td>
                <td><?php echo e($campaign['created_on']); ?></td>
                <td><?php echo e($campaign['start_date']); ?></td>
                <td><?php echo e($campaign['end_date']); ?></td>

                <?php if( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks'])): ?>
                    <td>
                        <?php if( in_array("edit campaign", $spData['permittedtasks']) ): ?>
                            Edit
                        <?php endif; ?>
                    </td>
                <?php endif; ?>

                <?php if( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( in_array("add product", $spData['permittedtasks']) ): ?>
                            Add Product
                        <?php endif; ?>
                        <?php if( in_array("add product", $spData['permittedtasks']) &&  in_array("view all products", $spData['permittedtasks']) &&  $campaign['has_products'] == 1): ?>
                            |
                        <?php endif; ?>
                        <?php if( in_array("view all products", $spData['permittedtasks']) ): ?>
                            <?php if( $campaign['has_products'] == 1 ): ?>
                                View
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
                
                <?php if( in_array("upload unique code masterlist", $spData['permittedtasks']) ): ?>
                    <?php if( strcmp($campaign['verification_type'], 2) ===0 ||  strcmp($campaign['verification_type'], 3) ===0 ): ?>
                        <td>
                            <?php if( $campaign['excel_uploaded'] == 0 ): ?>
                                Upload
                            <?php else: ?>
                                Uploaded
                            <?php endif; ?>
                        </td>
                    <?php else: ?>
                        <td>N/A</td>
                    <?php endif; ?>
                <?php endif; ?>

                <td>
                    <?php if( in_array("add unique codes", $spData['permittedtasks']) ): ?>
                        <?php if( $campaign['verification_type'] == 2 ): ?>
                            N/A
                        <?php else: ?>
                            Add
                        <?php endif; ?>
                        |
                    <?php endif; ?>

                    <?php if( in_array("approve unique codes", $spData['permittedtasks']) ): ?>
                            View Approval
                        |
                    <?php endif; ?>

                    <?php if( strcmp($campaign['verification_type'], 1) ===0 ): ?>
                        Unique-code only
                    <?php elseif( strcmp($campaign['verification_type'], 2) ===0 ): ?>
                        Unique-code & last 6-digit (GCR)
                    <?php elseif( strcmp($campaign['verification_type'], 3) ===0 ): ?>
                        Unique-code & last 6-digit (Bank)
                    <?php endif; ?>
                </td>

                <?php if( in_array("download unique codes", $spData['permittedtasks']) ): ?>
                    <td>
                        <?php if( $campaign['verification_type'] == 1 || $campaign['verification_type'] == 3 
                            || ($campaign['verification_type'] == 2 && $campaign['excel_uploaded'] == 1 )
                        ): ?>
                            Download
                        <?php elseif( $campaign['verification_type'] == 2 ): ?>
                            <?php if( $campaign['excel_uploaded'] <> 1 ): ?>
                                N/A
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
         $(document).ready(function() {
             // filter camapigns by active, expired, disabled
            $('.filter-checkbox').change(function() {
                if($('#chkbox-active').is(":checked")) {
                    $('#active-div').show();
                } else {
                    $('#active-div').hide();
                }   

                if($('#chkbox-expired').is(":checked")) {
                    $('#expired-div').show();
                } else {
                    $('#expired-div').hide();
                }    

                if($('#chkbox-disabled').is(":checked")) {
                    $('#disabled-div').show();
                } else {
                    $('#disabled-div').hide();
                }  
            });
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin/layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
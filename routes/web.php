<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin' , 'AdminController@index')->name('admin');
Route::get('/admin/profile' , 'AdminController@profile')->name('admin.profile');

Route::get('/admin/permission_denied' , 'AdminController@permission_denied')->name('admin.permission_denied');

Route::get('/admin/report/successful_redemptions', 'ReportController@successful_redemptions')->name('report.successful_redemptions')->middleware('checkPermission');
Route::get('/admin/report/successful_redemptions_search', 'ReportController@search_successful_redemptions');
Route::post('/admin/successful/redemptions/export', 'ReportController@successful_redemptions_export_excel');

Route::get('/admin/report/newsletter', 'ReportController@newsletter')->name('report.newsletter')->middleware('checkPermission');

Route::get('/admin/report/failed_redemptions', 'ReportController@failed_redemptions')->name('report.failed_redemptions')->middleware('checkPermission');
Route::get('/admin/report/failed_redemptions_search', 'ReportController@search_failed_redemptions');
Route::post('/admin/report/failed/redemptions/export', 'ReportController@failed_redemptions_export_excel');

Route::get('/admin/report/sms_status', 'ReportController@sms_status')->name('report.sms_status')->middleware('checkPermission');
Route::get('/admin/report/sms_status_search', 'ReportController@search_sms_status');
Route::post('/admin/report/sms/status/export', 'ReportController@sms_status_export_excel');

Route::get('/admin/report/emailstatus', 'ReportController@redemptionEmailStatus')->name('report.emailstatus')->middleware('checkPermission');

Route::get('/admin/report/excel_files', 'ReportController@excel_files')->name('report.excelfiles')->middleware('checkPermission');
Route::post('/admin/report/excel_files', 'ReportController@search_excel_files');

Route::post('/admin/report/excel_files/downloadhistory', 'ReportController@excelDownloadHistory')->name('report.excel-download-history');

Route::get('/admin/report/inventory', 'ReportController@inventory')->name('report.inventory')->middleware('checkPermission');
Route::get('/admin/report/inventory_search', 'ReportController@search_inventory');
Route::get('/admin/report/inventory/export', 'ReportController@inventory_export_excel');

Route::get('/admin/report/unused_unique_code', 'ReportController@unused_unique_codes')->name('report.unused_unique_codes')->middleware('checkPermission');
Route::get('/admin/report/unused_unique_code_search', 'ReportController@search_unused_codes');
Route::post('/admin/report/unused_unique_code/export', 'ReportController@unused_unique_codes_export_excel');
Route::get('/admin/report/unuseduniquecodeforcampaign/{campaign_id}', 'ReportController@unusedUniqueCodesForCampaign')->name('report.unuseduniquecodeforcampaign')->middleware('checkPermission');

Route::get('admin/auditlog/campaign_log','AuditLogController@campaign_logs')->name('auditlog.campaign')->middleware('checkPermission');
Route::get('admin/auditlog/product_log','AuditLogController@product_logs')->name('auditlog.product')->middleware('checkPermission');
Route::get('admin/auditlog/bank_log','AuditLogController@bank_logs')->name('auditlog.bank')->middleware('checkPermission');
Route::get('admin/auditlog/role_log','AuditLogController@role_logs')->name('auditlog.role')->middleware('checkPermission');
Route::get('admin/auditlog/manual_excel_generate_log','AuditLogController@manual_excel_generate_logs')->name('auditlog.manualexcel')->middleware('checkPermission');
Route::get('admin/auditlog/emailsettingslog','AuditLogController@emailSettingsLog')->name('auditlog.emailsettingslog')->middleware('checkPermission');
Route::get('admin/auditlog/uniquecodelog','AuditLogController@giftcodeLog')->name('auditlog.giftcodeLog')->middleware('checkPermission');
Route::get('admin/auditlog/campaignproductlog','AuditLogController@campaignProductLog')->name('auditlog.campaignproduct')->middleware('checkPermission');


Route::get('/admin/bank/all', 'BankController@fetch_all')->name('bank.all')->middleware('checkPermission');
Route::get('/admin/bank/add', 'BankController@add_bank_form')->middleware('checkPermission');
Route::get('/admin/bank/edit/{bank_name}/{bank_id}', 'BankController@edit_bank_form')->name('bank.edit_form')->middleware('checkPermission');
Route::get('/admin/bank/delete/{bank_name}/{bank_id}', 'BankController@delete_bank_form')->name('bank.delete_form')->middleware('checkPermission');
Route::post('/admin/bank/insert', 'BankController@insert');
Route::post('/admin/bank/update', 'BankController@update');
Route::post('/admin/bank/delete', 'BankController@delete');

Route::get('/admin/role/create', 'RolesController@create_role_form')->middleware('checkPermission');
Route::post('/admin/role/insert', 'RolesController@insert');
Route::get('/admin/role/delete/{role_name}/{role_id}', 'RolesController@delete_role_form')->name('role.delete_form')->middleware('checkPermission');
Route::post('/admin/role/delete', 'RolesController@delete_role_post');
Route::get('/admin/role/view_role_tasks/{role_id}', 'RolesController@view_role_tasks')->name('role.view_role_tasks')->middleware('checkPermission');
Route::get('/admin/role/edit/{role_id}', 'RolesController@edit_role_form')->name('role.edit_form')->middleware('checkPermission');
Route::post('/admin/role/editpost', 'RolesController@edit_role_post');
Route::get('/admin/role/all_users_with_roles', 'RolesController@view_all_user_roles')->name('users.all')->middleware('checkPermission');
Route::get('/admin/role/list', 'RolesController@view_all_roles')->name('role.all')->middleware('checkPermission');
Route::get('/admin/role/assign/{user_id}', 'RolesController@user_role_assign_form')->name('user_role.assign')->middleware('checkPermission');
Route::get('/admin/role/edit_user_role/{user_id}/{role_id}', 'RolesController@user_role_edit_form')->name('user_role.edit')->middleware('checkPermission');
Route::post('/admin/role/assign_user_role_post', 'RolesController@user_role_assign_post');
Route::get('/admin/role/users_of_role/{role_name}/{role_id}', 'RolesController@fetch_users_of_role')->name('role.view_users_of_role');
Route::post('/admin/role/unassign-user-role', 'RolesController@unassignUserRolePost')->name('user-role.unassign');

Route::get('/admin/system/emailsettings', 'EmailSettingsController@emailSettingsEditForm')->name('emailsettings')->middleware('checkPermission');
Route::post('/admin/system/emailsettings/update', 'EmailSettingsController@updateEmailSettings');
Route::post('/admin/system/emailsettings/delete', 'EmailSettingsController@delete')->name('emailsettings.delete');
Route::post('/admin/system/emailsettings/generalreceiver/add', 'EmailSettingsController@addGeneralReceiverEmail')->name('emailsettings.generalreceiver.add');

Route::get('/campaign/create', 'CampaignController@create')->middleware('checkPermission');
Route::get('/campaign/all', 'CampaignController@all')->name('campaign.all')->middleware('checkPermission');
Route::post('/campaign/insert', 'CampaignController@insert');
Route::post('/campaign/update', 'CampaignController@edit_post');
Route::get('/campaign/edit/{campaign_id}', 'CampaignController@edit')->name('campaign.edit')->middleware('checkPermission');
Route::get('/campaign/delete_confirm/{campaign_id}', 'CampaignController@delete_confirm')->name('campaign.delete_confirm')->middleware('checkPermission');
Route::post('/campaign/delete', 'CampaignController@delete_post');
Route::post('/campaign/upload_excel', 'CampaignController@upload_excel');
Route::get('/campaign/upload_excel_bank_masterlist/{campaign_name}/{campaign_id}/{verificationType}', 'CampaignController@upload_excel_form_bank_masterlist')->name('campaign.excel_uploadform_bank_masterlist')->middleware('checkPermission');
Route::post('/campaign/upload_excel_bank_masterlist', 'CampaignController@upload_excel_bank_masterlist');
Route::get('/campaign/add_unique_code/{campaign_id}/{campaign_name}', 'CampaignController@add_unique_code_form')->name('campaign.add_unique_code')->middleware('checkPermission');
Route::post('/campaign/add_unique_code/post', 'CampaignController@add_unique_code_post');
Route::get('/campaign/add_unique_code/{campaign_id}/{campaign_name}', 'CampaignController@add_unique_code_form')->name('campaign.add_unique_code')->middleware('checkPermission');

Route::get('/campaign/edit_unique_code_expiry/{campaign_id}/{batch}', 'CampaignController@editUniqueCodeExpiry')->name('campaign.edit_unique_code_expiry');
Route::post('/campaign/edit_unique_code_expiry/post', 'CampaignController@editUniqueCodeExpiryPost')->name('campaign.edit_unique_code_expiry_post');
Route::get('/{campaign_name}', 'CampaignFrontendController@fetch')->name('campaign.view');
Route::get('/campaign/approve_unique_code/{campaign_name}/{campaign_id}', 'CampaignController@unique_code_approve_form')->name('campaign.approve_unique_code')->middleware('checkPermission');
Route::post('/campaign/unique_code/approve', 'CampaignController@unique_code_approve_post');
Route::post('/campaign/unique_code/reject', 'CampaignController@uniqueCodeRejectPost')->name('uniqueCode.reject');
Route::get('/campaign/uploaded-excel/download/{filename}', 'ExcelFrontendController@uploadedBankMasterlistExcelDownload')->name('campaign.uploaded-excel_download');


Route::get('/product/all', 'ProductController@fetch_all')->name('product.all')->middleware('checkPermission');
Route::get('/product/add/{campaign_id}', 'ProductController@add')->name('product.add')->middleware('checkPermission');
Route::get('/product/edit/{product_id}', 'ProductController@edit_form')->name('product.edit')->middleware('checkPermission');
Route::get('/product/campaign_products/{campaign_id}', 'ProductController@show_campaign_products')->name('product.show_campaign_products');
Route::post('/product/update', 'ProductController@edit_post');
Route::post('/product/insert', 'ProductController@add_post');
Route::get('/product/imageupload/{product_id}', 'ProductController@img_upload_form');
Route::get('/product/delete_confirm/{product_id}', 'ProductController@delete_confirm')->name('product.delete_confirm')->middleware('checkPermission');
Route::post('/product/delete', 'ProductController@delete_post');


Route::get('/product/image/upload/{product_id}','ImageUploadController@imageUpload');
Route::post('/product/image/upload/post', 'ImageUploadController@imageUploadPost');

Route::get('/redeem/user_form/{campaign_id}/{product_id}/','RedeemController@user_fill_form')->name('redeem.userform');
Route::get('/redeem/user_form/{campaign_id}/{product_id}/{ic_code}/{unique_code}','RedeemController@user_fill_form_2')->name('redeem.userform2');
Route::post('/redeem/post','RedeemController@redeem_post');
Route::post('/redeem/validate_unique_code','RedeemController@validate_unique_code');

Route::get('/export/manual_excel_generate_for_upload_form', 'ExcelFrontendController@manual_excel_generate_redemption_groupby_product')->name('manualexcel')->middleware('checkPermission');
Route::post('/export/manual_excel_generate_for_upload_post', 'ExcelFrontendController@manual_excel_generate_redemption_groupby_product_post');

Route::get('/redemption/download/{uploaded_file_id}/{filename}', 'ExcelFrontendController@excel_download')->name('redemption.excel_download');
Route::view('/redemption/toc', 'toc')->name('toc');

Route::get('/login', 'LoginTestController@index');
Route::get('/admin/logout', 'LoginTestController@logout');

Route::get('/contact/message_form', 'ContactController@contact_form')->name('contact.form');
Route::post('/contactsend_message', 'ContactController@send_message');

// test mail
//Route::get('mail/sendbasic','MailController@basic_email');
//Route::get('mail/sendhtml','MailController@html_email');
//Route::get('mail/sendattachment','MailController@attachment_email');

Route::get('/unique_code/downloads/{campaign_id}', 'UniqueCodeController@uniqueCodeDownloadPage')->name('unique_code.download.batches');
Route::get('/unique_code/download_excel/{campaign_id}', 'UniqueCodeController@generate_excel_of_campaign_unique_codes')->name('unique_code.excel_download')->middleware('checkPermission');
Route::get('/unique_code/download_excel/one-batch/{campaign_id}/{batch}', 'UniqueCodeController@generateExcelOfCampaignUniqueCodesforOneBatch')->name('unique_code.excel_download.onebatch');

Route::get('campaign/excelupload/history/{campaign_id}', 'UniqueCodeController@excelUploadHistoryforCampaignPage')->name('campaign.exceluploadhistory');

// cron-jobs
Route::post('/export/redemption/groupbyproduct', 'ExcelController@redemption_groupby_product')->name('excel_generate.redemption_groupby_product');
Route::get('/excel_generated/check', 'FileCheckController@check_excel');









<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Message from Customer</title>
</head>
<body>
    <p>Customer Name: {{ $emaildata['name'] }}</p>
    <p>Customer Email: {{ $emaildata['email'] }}</p>

    <p>{{ $emaildata['message'] }}</p>

    <p style="font-size:0.7rem;">
    (Note: This email has been auto-generated when a customer sends a message through the "GCR - Contact Us" form. )
    </p>
</body>
</html>

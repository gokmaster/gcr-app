@extends('layouts.app')

@section('header')
<h3>Contact Us</h3>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

              
                <div class="card-body">
                    <h2>Contact Us</h2>
                    <form id="message_form" method="post" action="{{ url('/contactsend_message')}}">
                        @csrf
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name:" value="{{old('name')}}" required>
                        </div>

                        <div class="form-group">
                            <label>Email:</label>
                            <input type="email" class="form-control" id="email" name="email"  placeholder="Email:" value="{{old('email')}}" required>
                        </div>

                        <div class="form-group">
                            <label>Message:</label>
                            <textarea class="form-control" id="message" name="message" rows="3" placeholder="Write message here" required>{{old('message')}}</textarea>
                        </div>

                        <div class="styled-input" id="google-recaptcha">
                            <div class="g-recaptcha" data-sitekey="6Ld1Vn4UAAAAAN2StOoICOO4FRxeVtF6ue2zqaxI"></div>
                        </div>
                        <br>

                        <button id="btnSendMessage" type="submit" class="btn btn-primary">Send Message</button>    
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
     <!-- Google Recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        $("#message_form").validate();
    </script>
@endsection

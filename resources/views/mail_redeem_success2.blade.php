<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Redeem Success</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom_css.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> <!-- Font-Awesome-Icons-CSS -->
<style>
html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,dl,dt,dd,ol,nav ul,nav li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video {
	margin:0;
	padding:0;
	border:0;
	font-size:100%;
	font:inherit;
	vertical-align:baseline;
}
article, aside, details, figcaption, figure,footer, header, hgroup, menu, nav, section {
	display: block;
}
ol,ul {
	list-style:none;
	margin:0px;
	padding:0px;
}


/* start editing from here */
img {
	max-width:100%;
}
/*--- end reset code ---*/
body {
	padding: 0;
	margin: 0;
	background: #FFF;
	margin: 0;
	font-family: 'Poppins', sans-serif;
}
body a {
	transition: 0.5s all;
	-webkit-transition: 0.5s all;
	-moz-transition: 0.5s all;
	-o-transition: 0.5s all;
	-ms-transition: 0.5s all;
	text-decoration: none;
}
body a:hover {
	text-decoration: none;
}
body a:focus, a:hover {
	text-decoration: none;
}
input[type="button"], input[type="submit"] {
	transition: 0.5s all;
	-webkit-transition: 0.5s all;
	-moz-transition: 0.5s all;
	-o-transition: 0.5s all;
	-ms-transition: 0.5s all;
}
label {
	margin: 0;
    letter-spacing: 1px;
    color: #fff;
}
a:focus, a:hover {
	text-decoration: none;
	outline: none;
}

p{
	font-family: 'Open Sans', sans-serif;
}
.register-full {
    width: 90%;
    margin: 2em auto 0;
}
.register-left {

	
}
.register-right {
    background: #FFFFFF;
    background: rgba(0, 0, 0, 0);
    -webkit-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    -moz-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    box-shadow: 2px 2px 2px 1px rgba(253, 249, 249, 0.75);
}
.register-in {
    padding: 1em;
}
.register-left{
	width: 40%;
	height:350px;
	float: left;
    border: 1px solid #d3d3d3;
    border-radius: 10px;
	margin-right:5px;
	margin-left:120px;
	background: rgba(0, 0, 0, 0);
    -webkit-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    -moz-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    box-shadow: 1px 1px 9px 2px rgba(114, 111, 111, 0.75)
}

.register-right {
    width: 40%;
    float: left;
}
.register-left p {
    margin: 2em 0;
    line-height: 28px;
    font-size: 15px;
    font-weight: 100;
	padding:0 4em;
    letter-spacing: 1px;
    color: #000000;
    text-align: center;
 
}
.register-left h1 {
    font-size: 3em;
    text-transform: uppercase;
    margin-top: .5em;
    margin-bottom: .5em;
    color: #000000;
    text-align: center;
}
.register-right h2 {
    text-transform: uppercase;
    font-size: 2em;
    font-weight: 300;
    letter-spacing: 1px;
    word-spacing: 5px;
	color: #000;
	font-size:1.0em;
}
/*-- //input-effect --*/
.logo{
	text-align: center;
	margin-top: 2em;
	
}
.logo span.fa {
    color: #ffffff;
    font-size: 6em;
}
/*-- //copyright --*/

/* responsive design */
@media (max-width: 1440px) {
	.register-left h1 {
        font-size: 2.9em;
        color:#000000;
	}
}

@media (max-width: 1080px) {
	.register-full {
		width: 90%;
	}
	.register-left p {
        line-height: 1.6em;
        color: #000000;
	}
	.register-in {
		padding: 2em;
	}
}
@media (max-width: 1024px) {
	.register-left h1 {
        font-size: 2.3em;
        color:#000000;
	}
	.register-right h2 {
		font-size: 1.0em;
	}
	label {
		letter-spacing: 0px;
	}
	.register-left p {
		padding: 0 2em;
	}
	.logo span.fa {
        font-size: 5em;
        color:#ffffff;
	}
	
}
@media (max-width: 991px) {
	.register-right h2 {
		font-size: 1.0em;
	}
}

@media (max-width: 900px) {
	.register-full {
		width: 95%;
	}
	.register-left h1 {
		font-size: 2.1em;
	}
	.logo span.fa {
		font-size: 4em;
	}
	.register-left p {
		padding: 0 1em;
	}
}

@media (max-width: 800px) {
	.register-in {
		padding: 2em;
	}
	.register-left h1 {
        font-size: 2.1em;
        color:#000000;
	}
}
@media (max-width: 768px) {
	.register-left h1 {
		font-size: 2em;
	}
	.register-left p {
		padding: 0 4em;
	}
	.register-left {
		width: 100%;
        float: none;
		background-color:#ffffff;
	}
	.register-right {
		width: 70%;
		float: none;
		margin: 4em auto 0;
	}
	.logo {
		text-align: center;
		margin-top: 2em;
	}
}

@media (max-width: 736px) {
	.styled-input {
		margin: 0 0 1.2em;
	}
	.register-left h1 {
        font-size: 2em;
        color:#000000;
	}
	.register-right h2 {
		font-size: 1.0em;
	}
}
@media (max-width: 667px) {
	.register-in {
		padding: 2em;
	}
	.register-full {
		margin: 0em auto 0;
	}
	.logo {
		text-align: center;
		margin-top: 1em;
	}
}
@media (max-width: 640px) {
	.agile-copyright {
		letter-spacing: 1px;
	}
}
@media (max-width: 600px) {
	.register-left p {
		padding: 0 1em;
	}
}
@media (max-width: 568px) {
	.register-in {
		padding: 1.5em;
	}
	.register-left h1 {
        font-size: 1.7em;
        color:#000000;
	}
}
@media (max-width: 480px) {
	.register-left, .register-right {
		width: 100%;
		height: inherit;
	}
	.register-full {
		width: 90%;
	}
}
@media (max-width: 414px) {
	.register-left{
		width: 100%;
        height: inherit;
		background-color:#ffffff;
		margin-left: 0;
	}
	.register-right{
		width:100%;
	}
	input[type="checkbox"] {
		margin-right: 5px;
	}
	.logo span.fa {
		font-size: 3.5em;
	}
	.register input[type="text"], .register input[type="email"], .register input[type="password"], .register input[type="tel"] {
		width: 87%;
	}
	.logo {
		margin-top: 0em;
	}
}
@media (max-width: 384px) {
	.register-left h1 {
        font-size: 1.6em;
        color:#000000;
	}
	.register-left p {
		padding: 0 0em;
		font-size: 14px;
	}
}
@media (max-width: 375px) {
	.logo span.fa {
        font-size: 3em;
        color:#ffffff;
	}
}
@media (max-width: 320px) {
	body {
		padding: 2em 0;
	}
	.register-left p {
		font-size: 14px;
		letter-spacing: .4px;
	}
	.register-left h1 {
        font-size: 1.35em;
        color:#000000;
	}
	.register-right h2 {
		font-size: 1.0em;
	}
	.register-left p {
		margin: 1em 0 2em;
	}
	.register-right {
		margin: 3em auto 0;
	}
}	

/* //responsive design */

/**header*/
#topbar {
    background-color: #fff;
    text-align: center;
    font-size: 12px;
    padding: 5px 0;
}

#header-main {
    background-color: #f0f0f0;
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}

.d-flex.flex-row {
    border: 5px blue;
    width: 100%;
    color: white;
    height: 50px;
}

/**end header*/

/**stepper*/
.wrapper-progressBar {
    width: 100%
}

.progressBar {
}

.progressBar li {
    list-style-type: none;
    float: left;
    width: 33%;
    position: relative;
    text-align: center;
}

.progressBar li:before {
    /*content: " ";*/
    content: "\f25a";
    font-family: FontAwesome;
    line-height: 30px;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    border: 1px solid #ef241c;
    display: block;
    text-align: center;
    margin: 0 auto 10px;
    color: #ffffff;
    background-color: #ef241c;
}

.progressBar li:after {
    content: "";
    position: absolute;
    width: 100%;
    height: 4px;
    background-color: #ef241c;
    top: 15px;
    left: -50%;
    z-index: -1;
}

.progressBar li:first-child:after {
    content: none;
}

.progressBar li.active {
    color: #000;
}

.progressBar li.active:before {
    border-color: #ef241c;
    background-color: #ef241c;
}

.progressBar .active:after {
    background-color: #ef241c;
}

.progressBar li#step2:before {
    content:"\f040";
    font-family: FontAwesome;
    color: #ffffff;
    background-color: #ef241c;
}

.progressBar li#step3:before {
    content:"\f06b";
    font-family: FontAwesome;
    color: #ffffff;
    background-color: #ef241c;
}
/**stepper*/

.modal-dialog{
 max-width:70%;
 max-height: 80%;
}

.toc_box {
overflow: auto;
height: 400px;
}

.btn-circle.btn-xl {
    width: 70px;
    height: 70px;
    padding: 10px 16px;
    border-radius: 35px;
    font-size: 24px;
    line-height: 1.33;
}

.btn-circle {
    width: 30px;
    height: 30px;
    padding: 6px 0px;
    border-radius: 15px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
}


.btn-super-danger {
	color:red;
	background-color:#fff;
	width:35px;
	height:35px;
	padding:0px;
}

.btn-super-danger2 {
	color:white;
	background-color:red;
	width:100px;
}
</style>
</head>
<body>
<div class="container">

<section class="successful">
<div style="text-align:center; margin-top: 1px; margin-bottom: 50px;">
	<h5  style="font-weight: bold;font-size: 1.5em;"> 
		<img src="{{ URL::to('/') }}/images/icons/tick.png" width="30px" height="30px" alt="success-icon"> 
		REDEMPTION SUCCESSFUL 
	</h5><br>
	<h3  style="font-size: 1.5em; color: #787878;"> Your Gift Redemption is Successful! </h3><br>
	<h5  style="font-weight: bold;font-size: 1.0em;">YOUR ORDER NUMBER IS: {{ $order_no }}</h5><br>
</div>
</section>
 
<section class="register">
	<div class="register-full">
		<div class="register-left" style="height:auto;">
			<div class="register">
				<div class="logo">                 
                    <div class="box row" style="height: auto;">
                        <div class="col-sm-4 col-xs-12" style="text-align:left;padding-left:35px;">
                            <h3 style="font-size:1.0em; font-weight: bold;"><b> NAME: </b></h3>
                                <h5>{{ $user_data['name'] }}</h5> <br> 
                            <h3 style="font-size:1.0em; font-weight: bold"><b> MOBILE NO:</b></h3>
                                <h5>{{ $user_data['mobile'] }}</h5> <br> 
                            <h3 style="font-size:1.0em; font-weight: bold"><b> ADDRESS:</b></h3>
                                <h5>  {{ $user_data['address1'] }}<br>
                                    {{ $user_data['address2'] }}<br> 
                                    {{ $user_data['city'] }}<br>   
                                    {{ $user_data['state'] }}</h5><br>
                        </div>
                        <div class="col-sm-7 col-xs-12" style="text-align:center;">
                            <img src="{{ URL::to('/') }}/{{ $product[0]['filepath'] }}" alt="product" class="img-thumbnail img-check" width="70%">
							<br>{{$product[0]['product_name']}}
                        </div>
                    </div> 
				</div>
			</div>
        </div>

        <div class="register-right">
			<div class="register-in">
                <div class="box" style="height: auto; text-align: left; font-size:1.0em; font-weight: bold; margin-top: 1em;">
                        <h3><b>Thank you for your submission.</b></h3><br><br>
                        <h3><b>Your gifts will be processed for delivery to your <br>shipping address.</b></h3><br><br>
                        <h3><b>Please do allow 2-4 weeks time for delivery.</b></h3><br><br>
                        <h3><b>For more enquiries, please contact Tri-e Marketing<br>at 03-8076 1313 or info@tri-e.com.my</b></h3><br><br>
                        <h3><b>For more details please refer to HSBC terms and<br>conditions at www.hsbc.com.my</b></h3></span>
                </div>   
			</div>
		</div>
		<div style="clear:both;"> </div>
    </div> 
</div>
</body>
</html>

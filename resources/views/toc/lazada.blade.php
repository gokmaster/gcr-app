

<ul style="list-style-type: square;">
<li>Discount is applicable till [XXDAY, XXMONTH, XXYEAR] on the Lazada mobile app.</li>
<li>No minimum purchase is required to redeem this Discount.</li>
<li>The discount is applicable for one (1) time redemption only.</li>
<li>Create or Log on to your Lazada account to redeem discount.</li>
<li>No cash alternatives or refund will be offered in lieu of Promotion entitlement. Discount cannot be combined and accumulated.</li>
</ul>

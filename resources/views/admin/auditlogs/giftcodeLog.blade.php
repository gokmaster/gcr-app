@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Audit Logs: Unique Code</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Batch</th>
                <th>Unique Code Quantity</th>
                <th>Status</th>
                <th>Action Type</th>
                <th>Approved/Rejected By</th>
                <th>Log Updated At</th>
                <th>Created on</th>
                <th>Created By</th>
            </tr>
        </thead>
        <tbody>
            @foreach($logs as $log)
            <tr>
                <td>{{ $log['campaign_name'] }}</td>
                <td>{{ $log['bank_name'] }}</td>
                <td>{{ $log['batch'] }}</td>
                <td>{{ $log['unique_code_qty'] }}</td>
                <td>{{ $log['status'] }}</td>
                <td>{{ $log['action_type'] }}</td>
                <td>{{ $log['approved_by'] }}</td>
                <td>{{ $log['log_timestamp'] }}</td>
                <td>{{ $log['created_on'] }}</td>
                <td>{{ $log['username'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <hr>

    <h3>Audit Logs: Unique Code Downloads</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Batch</th>
                <th>Unique Code Quantity</th>
                <th>Action Type</th>
                <th>Log Updated At</th>
                <th>Downloaded By</th>
            </tr>
        </thead>
        <tbody>
            @foreach($downloadlogs as $log)
            <tr>
                <td>{{ $log['campaign_name'] }}</td>
                <td>{{ $log['bank_name'] }}</td>
                <td>{{ $log['batch'] }}</td>
                <td>{{ $log['unique_code_qty'] }}</td>
                <td>{{ $log['action_type'] }}</td>
                <td>{{ $log['log_timestamp'] }}</td>
                <td>{{ $log['username'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <hr>

    <h3>Audit Logs: Unique Code Uploads</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>File Uploaded</th>
                <th>Unique Code Quantity</th>
                <th>Action Type</th>
                <th>Log Updated At</th>
                <th>Uploaded By</th>
            </tr>
        </thead>
        <tbody>
            @foreach($uploadlogs as $log)
            <tr>
                <td>{{ $log['campaign_title'] }}</td>
                <td>{{ $log['bank_name'] }}</td>
                <td>{{ $log['filename'] }}</td>
                <td>{{ $log['unique_code_qty'] }}</td>
                <td>{{ $log['action_type'] }}</td>
                <td>{{ $log['log_timestamp'] }}</td>
                <td>{{ $log['username'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
        
</div>


@endsection
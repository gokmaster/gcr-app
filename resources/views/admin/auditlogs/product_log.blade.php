@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Audit Logs: Product</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Description</th>
                <th>SKU</th>
                <th>Category</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Deleted</th>
                <th>Action Type</th>
                <th>Actioned By</th>
                <th>Log Updated At</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
        <tr>
            <td>{{ $log['product_name'] }}</td>
            <td>{{ $log['description'] }}</td>
            <td>{{ $log['sku'] }}</td>
            <td>{{ $log['categories'] }}</td>
            <td>{{ $log['price'] }}</td>
            <td>{{ $log['quantity'] }}</td>
            <td>
                @if ( strcmp( $log['deleted'] , 0) == 0 ) 
                    No
                @else
                    Yes
                @endif
            </td>
            <td>{{ $log['action_type'] }}</td>
            <td>{{ $log['username'] }}</td>
            <td>{{ $log['log_timestamp'] }}</td>
            <td>{{ $log['created_on'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
        
</div>

@endsection
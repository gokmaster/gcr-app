@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Audit Logs: Bank</h3>
    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Bank Name</th>
                <th>Deleted</th>
                <th>Action Type</th>
                <th>Action By</th>
                <th>Log Updated at</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
        <tr>
            <td>{{ $log['name'] }}</td>
            <td>
                @if ( strcmp( $log['deleted'] , 0) == 0 ) 
                    No
                @else
                    Yes
                @endif
            </td>
            <td>{{ $log['action_type'] }}</td>
            <td>{{ $log['username'] }}</td>
            <td>{{ $log['log_timestamp'] }}</td>
            <td>{{ $log['created_on'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
        
</div>


@endsection
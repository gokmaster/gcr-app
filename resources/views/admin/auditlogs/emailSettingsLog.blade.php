@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Audit Logs: Email Settings</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Email Type</th>
                <th>Email</th>
                <th>Action Type</th>
                <th>Actioned By</th>
                <th>Log Updated At</th>
            </tr>
        </thead>
        <tbody>
            @foreach($logs as $log)
            <tr>
                <td>{{ $log['email_type'] }}</td>
                <td>{{ $log['email'] }}</td>
                <td>{{ $log['action_type'] }}</td>
                <td>{{ $log['username'] }}</td>
                <td>{{ $log['log_timestamp'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
        
</div>


@endsection
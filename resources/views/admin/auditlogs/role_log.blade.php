@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Audit Logs: Role</h3>
 
    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Role Name</th>
                <th>Permission List</th>
                <th>Action Type</th>
                <th>Actioned By</th>
                <th>Log Updated At</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
        <tr>
            <td>{{ $log['role_name'] }}</td>
            <td>{{ $log['task_list'] }}</td>
            <td>{{ $log['action_type'] }}</td>
            <td>{{ $log['username'] }}</td>
            <td>{{ $log['log_timestamp'] }}</td>
            <td>{{ $log['created_on'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <br><br>
    <h3>Audit Logs: Role Assign</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Role Name</th>
                <th>Username</th>
                <th>Action Type</th>
                <th>Actioned By</th>
                <th>Log Updated At</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
        @foreach($userRolesLog as $log)
        <tr>
            <td>{{ $log['role_name'] }}</td>
            <td>{{ $log['username'] }}</td>
            <td>{{ $log['action_type'] }}</td>
            <td>{{ $log['admin'] }}</td>
            <td>{{ $log['log_timestamp'] }}</td>
            <td>{{ $log['created_on'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
              
</div>

@endsection
@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Audit Logs: Campaign</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Campaign Title</th>
                <th>Description</th>
                <th>Excel Uploaded</th>
                <th>Deleted</th>
                <th>Has Products</th>
                <th>Action Type</th>
                <th>Actioned By</th>
                <th>Log Updated At</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
        <tr>
            <td>{{ $log['title'] }}</td>
            <td>{{ $log['description'] }}</td>
            <td>
                @if ( strcmp( $log['excel_uploaded'] , 0) == 0 ) 
                    No
                @else
                    Yes
                @endif
            </td>
            <td>
                @if ( strcmp( $log['deleted'] , 0) == 0 ) 
                    No
                @else
                    Yes
                @endif
            </td>
            <td>
                @if ( strcmp( $log['has_products'] , 0) == 0 ) 
                    No
                @else
                    Yes
                @endif
            </td>
            <td>{{ $log['action_type'] }}</td>
            <td>{{ $log['username'] }}</td>
            <td>{{ $log['log_timestamp'] }}</td>
            <td>{{ $log['created_on'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
            
</div>


@endsection
@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Audit Logs: Manual Excel Generate</h3>

    <table class="table search_paginate_sort_selectperpage">
        <thead>
            <tr>
                <th>Date Generated</th>
                <th>Redemption Date</th>
                <th>Batch No</th>
                <th>Filename</th>
                <th>Action Type</th>
                <th>Actioned By</th>
                <th>Log Updated At</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
        <tr>
            <td>{{ date('d-m-Y', strtotime( $log['upload_date'])) }}</td>
            <td>{{ date('d-m-Y', strtotime( $log['redemption_date'])) }}</td>
            <td>{{ $log['batch_no'] }}</td>
            <td>{{ $log['filename'] }}</td>
            <td>{{ $log['action_type'] }}</td>
            <td>{{ $log['username'] }}</td>
            <td>{{ $log['log_timestamp'] }}</td>
            <td>{{ $log['created_on'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
               

</div>

@endsection
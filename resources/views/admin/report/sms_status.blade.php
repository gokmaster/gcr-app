@extends('admin/layout')

@section('header')

@endsection

@section('content')


<div class="page_box">
    <h3>SMS Notification Status</h3>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    
   <!--  <form id="searchform" method="get" action="{{ url('/admin/report/sms_status_search') }}">
        @csrf
        <div class="form-group">
        <div class="row">
            <div class="col-md-3">
            <select class="form-control" id="sel1" name="search_option">
                <option value="campaign">Campaign</option>
                <option value="bank">Bank</option>
                <option value="sms_transact_id">SMS Transaction ID</option>
                <option value="name">Name</option>
                <option value="mobile_no">Contact no</option>
                <option value="ic_creditcard_no">Last 6-digit Credit Card/IC</option>
                <option value="unique_code">Unique Code</option>
                <option value="sms_status">SMS status</option>
                <option value="gift_code">Error Description</option>
                <option value="sms_content">SMS content</option>
                <option value="redemption_id">Redemption Transaction ID</option>
            </select>
            </div>
            <div class="col-md-7">
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
            </div>
            <div class="col-md-2">
            <input id="btnSearch" type="submit" value="Search" class="btn btn-primary" >
            </div>
        </div>
        </div>
    </form> -->
    
    @if (count($sms_status) > 0)
        @if(!empty($_GET['keyword']) && !empty($_GET['search_option']))
            @php($table = 'customsearchPerPageShow')
        @else
            @php($table = '')
    @endif

        <table class="table table-hover {{ $table }}">
            <thead>
            <tr>
                <th>SMS Sent Date</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>SMS Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Last 6-digit of credit card/ IC</th>
                <th>Unique Code Input</th>
                <th>SMS status</th>
                <th>Gift Code</th>
                <th>SMS content</th>
                <th>Redemption Transaction ID</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sms_status as $s)
            <tr>
                <td>{{ date('d-m-Y', strtotime( $s['sent_date'])) }}</td>
                <td>{{ $s['bank_name'] }}</td>
                <td>{{ $s['campaign_name'] }}</td>
                <td>{{ $s['sms_transact_id'] }}</td>
                <td>{{ $s['customer_name'] }}</td>
                <td>{{ $s['mobile_no'] }}</td>
                <td>{{ $s['ic_creditcard_no'] }}</td>
                <td>{{ $s['unique_code'] }}</td>
                <td>{{ $s['sms_status'] }}</td>
                <td>{{ $s['gift_code'] }}</td>
                <td>{{ $s['sms_content'] }}</td>
                <td>{{ $s['redemption_id'] }}</td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>SMS Sent Date</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>SMS Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Last 6-digit of credit card/ IC</th>
                <th>Unique Code Input</th>
                <th>SMS status</th>
                <th>Gift Code</th>
                <th>SMS content</th>
                <th>Redemption Transaction ID</th>
            </tr>
            </tfoot>
        </table>

       <!--  <form id="exportExcelform" method="post" action="{{ url('/admin/report/sms/status/export') }}">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-md-5">                    
                    <label>Start date:</label>
                        <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" required>
                    </div>
                    <div class="col-md-5">
                    <label>End date:</label>
                        <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" required>
                    </div>
                    <div class="col-md-2" style="top: 25px;">
                        <button type="submit" class="btn btn-primary">Export to Excel</button>
                    </div>
                </div>
            </div>
        </form> -->

        {{ $sms_status->links() }}
@else
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
@endif

 </div>

@endsection


@section('scripts')
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'SMS Notification Status'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'SMS Notification Status'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        } );
    </script>

    <script src="{{ asset('js/datepicker_maxdate.js') }}" ></script>
@endsection
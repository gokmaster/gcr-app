@extends('admin/layout')

@section('header')

@endsection

@section('content')


<div class="page_box">
    <h3>Failed Redemptions</h3>

     @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

  <!--  <form id="searchform" method="get" action="{{ url('/admin/report/failed_redemptions_search') }}">
    @csrf
    <div class="form-group">
      <div class="row">
        <div class="col-md-3">
          <select class="form-control" id="sel1" name="search_option">
            <option value="date">Date</option>
            <option value="campaign">Campaign</option>
            <option value="bank">Bank</option>
            <option value="ticket_number">Ticket NO</option>
            <option value="name">Name</option>
            <option value="ic_creditcard_no">Last 6-digit Credit Card/IC</option>
            <option value="mobile">Contact no</option>
            <option value="unique_code">Unique Code</option>
            <option value="error_code">Error Code</option>
            <option value="error_description">Error Description</option>
          </select>
        </div>
        <div class="col-md-7">
          <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
        </div>
        <div class="col-md-2">
          <input id="btnSearch" type="submit" value="Search" class="btn btn-primary" >
        </div>
      </div>
    </div>
  </form> -->
  @if (count($failed_transactions) > 0)
    @if(!empty($_GET['keyword']) && !empty($_GET['search_option']))
        @php($table = 'customsearchPerPageShow')
    @else
        @php($table = '')
  @endif

        <table class="table table-hover {{$table}}">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Bank</th>
                    <th>Campaign</th>
                    <th>Ticket ID</th>
                    <th>Name</th>
                    <th>Last 6-digit Credit Card/IC</th>
                    <th>Contact Number</th>
                    <th>Unique Code Input</th>
                    <th>Error Code</th>
                    <th>Error Description</th>
                </tr>
            </thead>
            <tbody>
                @foreach($failed_transactions as $ft)
                <tr>
                    <td>{{ date('d-m-Y h:i:s', strtotime( $ft['created_on'])) }}</td>
                    <td>{{ $ft['bank_name'] }}</td>
                    <td>{{ $ft['campaign_name'] }}</td>
                    <td>{{ $ft['ticket_number'] }}</td>
                    <td>{{ $ft['name'] }}</td>
                    <td>{{ $ft['ic_creditcard_no'] }}</td>
                    <td>{{ $ft['mobile'] }}</td>
                    <td>{{ $ft['unique_code_entered'] }}</td>
                    <td>{{ $ft['error_code'] }}</td>
                    <td>{{ $ft['error_description'] }}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Bank</th>
                    <th>Campaign</th>
                    <th>Ticket ID</th>
                    <th>Name</th>
                    <th>Last 6-digit Credit Card/IC</th>
                    <th>Contact Number</th>
                    <th>Unique Code Input</th>
                    <th>Error Code</th>
                    <th>Error Description</th>
                </tr>
            </tfoot>
        </table>

      <!--   <form id="exportExcelform" method="post" action="{{ url('/admin/report/failed/redemptions/export') }}">
        @csrf
        <div class="form-group">
        <div class="row">
            <div class="col-md-5">
            <label>Start date:</label>
            <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" required>
            </div>
            <div class="col-md-5">
            <label>End date:</label>
            <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" required>
            </div>
            <div class="col-md-2" style="top: 25px;">
            <button type="submit" class="btn btn-primary">Export to Excel</button>
            </div>
        </div>
        </div>
    </form> -->

@else
    <div class="alert alert-warning" role="alert">
        No records found
    </div>

@endif

 

</div>

@endsection


@section('scripts')
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Failed Redemptions'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Failed Redemptions'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        } );
    </script>

    <script src="{{ asset('js/datepicker_maxdate.js') }}" ></script>
@endsection
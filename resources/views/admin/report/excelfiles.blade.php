@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">
    <h3>Excel Files for upload into Tri-e portal</h3>
   
@if (count($excelfiles) > 0)
   
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Date Generated</th>
                <th>Redemption Date</th>
                <th>Batch No</th>
                <th>Filename</th>
                <th>Download Count</th>
                <th>Email Status</th>
                <th>Email Time</th>
      <!--           <th>Excel Checked</th> -->
            </tr>
            </thead>
            <tbody>
            @foreach($excelfiles as $excelfile)
            <tr>
                <td>{{ date('d-m-Y', strtotime( $excelfile['upload_date'])) }}</td>
                <td>{{ date('d-m-Y', strtotime( $excelfile['redemption_date'])) }}</td>
                <td>{{ $excelfile['batch_no'] }}</td>
                <td><a href="{{route('redemption.excel_download', ['uploaded_file_id' => $excelfile['uploaded_file_id'], 'filename' => $excelfile['filename'] ]) }}">{{ $excelfile['filename'] }}</a></td>
                <td>
                    @if ( $excelfile['download_count'] !== null )
                        <a class="view-download-history" data-uploadfile-id="{{ $excelfile['uploaded_file_id'] }}"  style="cursor:pointer;">{{ $excelfile['download_count'] }}</a>
                    @else 
                        0
                    @endif
                </td>
                <td>{{ $excelfile['email_status'] }}</td>
                <td>{{ date('H:i', strtotime( $excelfile['email_time'])) }}</td>
                <!-- <td>
                    @if ( strcmp($excelfile['checked'] , 0) == 0 )
                        <span style="color:red;">failed</span>
                    @elseif ( strcmp($excelfile['checked'] , 1) == 0 )
                        <span style="color:green;">success</span>
                    @else ( strcmp($excelfile['checked'] , null) == 0 )
                        <span style="color:grey;">check pending<span>
                    @endif
                </td> -->
            </tr>
            @endforeach
            </tbody>
        </table>

        {{ $excelfiles->links() }}
    
@else
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
@endif

</div>


<!-- Modal -->
<div id="downloadHistoryModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="max-height:500px; overflow:auto;">
      <div class="modal-body" id="downloadHistoryModalBody" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection


@section('scripts')
    <script>
        $( ".view-download-history" ).on( "click", function(e) {

            var current_button = e.target;

            var uploadedfileID = $(current_button).attr('data-uploadfile-id');

            $.post("{{ route('report.excel-download-history') }}", { _token: "{{ csrf_token() }}", uploadedfileID: uploadedfileID}, function(result){
                
                var history = result.history;

                var tableHtml = "<table class='table' >"+
                                    "<tr>"+
                                        "<th>Downloaded by</th>"+
                                        "<th>Downloaded at</th>"+
                                    "</tr>";

                $.each( history, function( key, value ) {
                    tableHtml += "<tr>"+
                                        "<td>"+ value.username +"</td>"+
                                        "<td>"+ value.created_at +"</td>"+
                                    "</tr>";
                });

                tableHtml += "</table>"

                $('#downloadHistoryModalBody').html(tableHtml);
                $('#downloadHistoryModal').modal('show');
            });

        });
    </script>

@endsection
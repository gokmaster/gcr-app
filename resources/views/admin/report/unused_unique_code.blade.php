@extends('admin/layout')

@section('header')

@endsection

@section('content')


<div class="page_box">
    <h3>Unutilised Unique Codes</h3>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    
@if (count($unused_codes) > 0)
    
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Number of Unutilised Unique Codes</th>
                <th>Details</th>
            </tr>
            </thead>
            <tbody>
            @foreach($unused_codes as $s)
            <tr>
                <td>{{ $s['bank_name'] }}</td>
                <td>{{ $s['campaign_name'] }}</td>
                <td>
                    {{ $s['unused_qty'] }}
                </td>
                <td><a href="{{ route('report.unuseduniquecodeforcampaign' , ['campaign_id'=> $s['campaign_id'] ] ) }}">View details</a>
            </tr>
            @endforeach
            </tbody>

            <tfoot>
                <tr>
                    <th><input type="text" placeholder="Search Bank" /></th>
                    <th><input type="text" placeholder="Search Campaign" /></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>

       <!--  <form id="exportExcelform" method="post" action="{{ url('/admin/report/unused_unique_code/export') }}">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-md-2" style="top: 25px;">
                        <button type="submit" class="btn btn-primary">Export to Excel</button>
                    </div>
                </div>
            </div>
        </form> -->

       
@else
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
@endif

 </div>

@endsection


@section('scripts')
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Unutilised Unique Codes'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Unutilised Unique Codes'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
          /*   $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } ); */
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        } );
    </script>
@endsection
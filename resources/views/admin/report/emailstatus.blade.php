@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box" >
    <h3>Email Status Report</h3>

     @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
 
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Email</th>
                <th>Timestamp</th>
            </tr>
            </thead>
            <tbody>
            @foreach($emailstatus as $d)
            <tr>
                <td>{{ date('d-m-Y h:i:s', strtotime($d['date_redeemed'])) }}</td>
                <td>{{ $d['bank_name'] }}</td>
                <td>{{ $d['campaign'] }}</td>
                <td>{{ $d['order_no'] }}</td>
                <td>{{ $d['customer_name'] }}</td>
                <td>{{ $d['contact_no'] }}</td>
                <td>{{ $d['email'] }}</td>
                <td>{{ $d['created_on'] }}</td>
            </tr>
            @endforeach
            </tbody>

            <tfoot>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Email</th>
                <th>Timestamp</th>
            </tr>
            </tfoot>
        </table>
 </div>


@endsection


@section('scripts')
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Email Status Report'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Email Status Report'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            });
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });

            // floating horizontal scrollbar
            //$(".page_box").floatingScroll();
        } );
    </script>

  
@endsection


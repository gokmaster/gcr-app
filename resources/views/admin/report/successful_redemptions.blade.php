@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box" >
    <h3>Successful Redemptions</h3>

     @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

    <!-- <form id="searchform" method="get" action="{{ url('/admin/report/successful_redemptions_search') }}">
        @csrf
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <select class="form-control" id="sel1" name="search_option">
                        <option value="campaign">Campaign</option>
                        <option value="bank">Bank</option>
                        <option value="transaction_id">Transaction ID</option>
                        <option value="name">Name</option>
                        <option value="postcode">Postcode</option>
                        <option value="city">City</option>
                        <option value="state">State</option>
                        <option value="mobile_no">Mobile no</option>
                        <option value="last_6_digit">Last 6-digit Credit Card/IC</option>
                        <option value="unique_code">Unique Code</option>
                        <option value="address1">Address 1</option>
                        <option value="address2">Address 2</option>
                    </select>
                </div>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
                </div>
                <div class="col-md-2">
                    <input id="btnSearch" type="submit" value="Search" class="btn btn-primary" >
                </div>	
            </div>
        </div>
    </form> -->
@if (count($redemptions) > 0)
    @if(!empty($_GET['keyword']) && !empty($_GET['search_option']))
        @php($table = 'customsearchPerPageShow')
    @else
        @php($table = '')
@endif
 
        <table class="table table-hover {{$table}}">
            <thead>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Address line 1</th>
                <th>Address line 2</th>
                <th>Address line 3</th>
                <th>Address line 4</th>
                <th>Postcode</th>
                <th>City</th>
                <th>State</th>
                <th>Home no</th>
                <th>Office no</th>
                <th>Mobile no</th>
                <th>Last 6-digit Credit Card/IC</th>
                <th>Unique Code</th>
                <th>Product SKU</th>
            </tr>
            </thead>
            <tbody>
            @foreach($redemptions as $redemption)
            <tr>
                <td>{{ date('d-m-Y h:i:s', strtotime($redemption['date_redeemed'])) }}</td>
                <td>{{ $redemption['bank_name'] }}</td>
                <td>{{ $redemption['campaign_name'] }}</td>
                <td>{{ $redemption['order_no'] }}</td>
                <td>{{ $redemption['name'] }}</td>
                <td>{{ $redemption['contact_no'] }}</td>
                <td>{{ $redemption['address1'] }}</td>
                <td>{{ $redemption['address2'] }}</td>
                <td>{{ $redemption['address3'] }}</td>
                <td>{{ $redemption['address4'] }}</td>
                <td>{{ $redemption['zip_code'] }}</td>
                <td>{{ $redemption['city'] }}</td>
                <td>{{ $redemption['state'] }}</td>
                <td>{{ $redemption['home_no'] }}</td>
                <td>{{ $redemption['office_no'] }}</td>
                <td>{{ $redemption['mobile'] }}</td>
                <td>{{ $redemption['ic_creditcard_no'] }}</td>
                <td>{{ $redemption['unique_code'] }}</td>
                <td>{{ $redemption['sku'] }}</td>
            </tr>
            @endforeach
            </tbody>

            <tfoot>
            <tr>
                <th>Date of transaction</th>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Transaction ID</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Address line 1</th>
                <th>Address line 2</th>
                <th>Address line 3</th>
                <th>Address line 4</th>
                <th>Postcode</th>
                <th>City</th>
                <th>State</th>
                <th>Home no</th>
                <th>Office no</th>
                <th>Mobile no</th>
                <th>Last 6-digit Credit Card/IC</th>
                <th>Unique Code</th>
                <th>Product SKU</th>
            </tr>
            </tfoot>
        </table>
  
        <!--  <form id="exportExcelform" method="post" action="{{ url('/admin/successful/redemptions/export') }}">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-md-5">                    
                    <label>Start date:</label>
                        <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" required>
                    </div>
                    <div class="col-md-5">
                    <label>End date:</label>
                        <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" required>
                    </div>
                    <div class="col-md-2" style="top: 25px;">
                        <button type="submit" class="btn btn-primary">Export to Excel</button>
                    </div>
                </div>
            </div>
        </form> -->

        
@else
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
@endif

 </div>


@endsection


@section('scripts')
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Successful Redemptions'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Successful Redemptions'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            });
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
            });

            // floating horizontal scrollbar
            $(".page_box").floatingScroll();
        } );
    </script>

    <script src="{{ asset('js/datepicker_maxdate.js') }}" ></script>
@endsection


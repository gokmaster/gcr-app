@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">
    <h3>Inventory</h3>
  <!--   <form id="searchform" method="get" action="{{ url('/admin/report/inventory_search') }}">
        @csrf
        <div class="form-group">
        <div class="row">
            <div class="col-md-3">
            <select class="form-control" id="sel1" name="search_option">
                <option value="product_name">Product Name</option>
                <option value="campaign">Campaign</option>
                <option value="bank">Bank</option>
            </select>
            </div>
            <div class="col-md-7">
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
            </div>
            <div class="col-md-2">
            <input id="btnSearch" type="submit" value="Search" class="btn btn-primary" >
            </div>
        </div>
        </div>
    </form> -->


@if (count($inventory) > 0)
    @if(!empty($_GET['keyword']) && !empty($_GET['search_option']))
    @php($table = 'customsearchPerPageShow')
    @else
    @php($table = '')
@endif 

        <table class="table table-hover {{$table}}">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Item</th>
                <th>Ordered</th>
                <th>Total Redeemed</th>
                <th>Balance</th>
            </tr>
            </thead>
            <tbody>
            @foreach($inventory as $inv)
            <tr>
                <td>{{ $inv['bank_name'] }}</td>
                <td>{{ $inv['campaign_name'] }}</td>
                <td>{{ $inv['product_name'] }}</td>
                <td>{{ $inv['ordered_qty'] }}</td>
                <td>{{ $inv['total_redeemed'] }}</td>
                <td><?php echo $inv['ordered_qty'] - $inv['total_redeemed']; ?></td>
            </tr>
            @endforeach
            </tbody>

            <tfoot>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Item</th>
                <th>Ordered</th>
                <th>Total Redeemed</th>
                <th>Balance</th>
            </tr>
            </tfoot>
        </table>

    <!--      <form id="exportExcelform" method="get" action="{{ url('/admin/report/inventory/export') }}">
            @csrf
            <div class="form-group">
            <div class="row">
                <div class="col-md-2" style="top: 25px;">
                <input type="hidden" name="inventory_export" value="1" />
                <button type="submit" class="btn btn-primary">Export to Excel</button>
                </div>
            </div>
            </div>
        </form> -->

@else
    <div class="alert alert-warning" role="alert">
        No records found
    </div>
@endif

</div>

@endsection

@section('scripts')
    <script>
        $('#exportExcelform').validate();

        $(document).ready(function() {
            $('.table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                    extend: 'excelHtml5',
                    title: 'Inventory'
                    },
                    {
                    extend: 'pdfHtml5',
                    title: 'Inventory'
                    }
                ]
            });

             // Setup - add a text input to each footer cell
            $('.table tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
        
            // DataTable
            var table = $('.table').DataTable();
        
            // Apply the search
            table.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        } );
    </script>
@endsection
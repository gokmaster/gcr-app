@extends('admin/layout')

@section('header')

@endsection

@section('content')
<div class="page_box">
    <h3>Email Settings</h3>

    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <br>
    <form class="emailsettings_form" method="post" action="{{ route('emailsettings.generalreceiver.add')}}">
        @csrf
        <a href="#" id="btnAddEmail" class="btn-email-add btn btn-primary">Add General Receiver Email</a>
        <div class="row div-add-email" style="display:none;"> 
            <div class="col-sm-4" >   
                <input type="email" id="add_email_input" class="form-control email" name="email" required>    
            </div>
            <div class="col-sm-4" >                  
                <button type="submit" class="btn btn-primary" >Add</button>
                <a href="#"  class="btn-addEmail-cancel btn btn-default" >Cancel</a>  
            </div>
        </div>
    </form>

    <br>

    @if(isset($emailsettings) && $emailsettings !== null)
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Email Type</th>
                    <th>Email Address</th>
                </tr>
            </thead>
            <tbody>
            
            @foreach($emailsettings as $emailsetting)
            <tr>
                <td>
                    {{ $emailsetting['email_type'] }}
                </td>
                <td>
                    <div class="row">
                            <form class="emailsettings_form form-no-margin" method="post" action="{{ url('/admin/system/emailsettings/update')}}">
                                <div class="col-sm-4" >
                                    <input type="email" id="email_{{ $emailsetting['email_settings_id'] }}" class="form-control email" name="email" value="{{ $emailsetting['email'] }}" data-original-email="{{ $emailsetting['email'] }}" disabled required>
                                </div>   
                                <div class="col-sm-4" >        
                                    <a href="#" id="btnEdit_{{ $emailsetting['email_settings_id'] }}" class="btn-email-edit btn btn-primary" data-spnsave-id="spnSave_{{ $emailsetting['email_settings_id'] }}" data-email-id="email_{{ $emailsetting['email_settings_id'] }}" data-original-email="{{ $emailsetting['email'] }}" >Edit</a>
                                    <span id="spnSave_{{ $emailsetting['email_settings_id'] }}" class="spnSave" style="display:none;">
                                            @csrf
                                            <input type="hidden" name="emailsettings_id" value="{{ $emailsetting['email_settings_id'] }}">
                                            <button type="submit" class="btn btn-primary" >Save</button>
                                        <a href="#"  class="btn-email-cancel btn btn-default"  data-btnEdit-id="btnEdit_{{ $emailsetting['email_settings_id'] }}" data-spnsave-id="spnSave_{{ $emailsetting['email_settings_id'] }}" data-email-id="email_{{ $emailsetting['email_settings_id'] }}" data-original-email="{{ $emailsetting['email'] }}" >Cancel</a>  
                                    </span>
                            </form>
                            @if ( $emailsetting['is_default'] === 0)
                                <form class="emailsettings_form form-no-margin" method="post" action="{{ route('emailsettings.delete') }}">
                                    @csrf
                                    <input type="hidden" name="emailsettings_id" value="{{ $emailsetting['email_settings_id'] }}">
                                    <button type="submit" class="btn btn-primary" >Delete</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>


    @else
    <div class="alert alert-warning">No records found</div> 
    @endif   
    
</div>

@endsection


@section('scripts')
    <script>
        $(".emailsettings_form").validate();

        $( document ).ready(function() {

            $( "#btnAddEmail" ).on( "click", function(e) {
                // hide all save buttons
                $('#btnAddEmail').hide();
                $('.div-add-email').show();

                $('.email').attr("disabled", "disabled"); // disable all edit textboxes

                var current_button = e.target;
                
                $(current_button).hide();
                $("#add_email_input").removeAttr("disabled");  // enable current textbox

                 // hide all save buttons
                $('.spnSave').hide();
                $('.btn-email-edit').show();

                // revert all email textbox back to original value
                $('.email').each(function(i, obj) {
                    var originalEmail = $(obj).attr('data-original-email');
                    $(obj).val(originalEmail);
                });

            });

            $( ".btn-addEmail-cancel" ).on( "click", function(e) {
                var current_button = e.target;

                $("#btnAddEmail").show();
                $(".div-add-email").hide();
                $("#add_email_input").val(""); // clear value
            });


            $( ".btn-email-edit" ).on( "click", function(e) {
                // hide all save buttons
                $('.spnSave').hide();
                $('.btn-email-edit').show();

                // revert all email textbox back to original value
                $('.email').each(function(i, obj) {
                    var originalEmail = $(obj).attr('data-original-email');
                    $(obj).val(originalEmail);
                });

                $('.email').attr("disabled", "disabled"); 

                var current_button = e.target;

                var txtEmailId = $(current_button).attr('data-email-id');
                var spnSaveId = $(current_button).attr('data-spnsave-id');
                
                $(current_button).hide();
                $("#"+ spnSaveId ).show();
                $("#"+ txtEmailId ).removeAttr("disabled");  

                $('#btnAddEmail').show();
                $("#add_email_input").val(""); // clear value
                $('.div-add-email').hide();
            });


            $( ".btn-email-cancel" ).on( "click", function(e) {
                var current_button = e.target;

                var txtEmailId = $(current_button).attr('data-email-id');
                var spnSaveId = $(current_button).attr('data-spnsave-id');
                var btnEditId = $(current_button).attr('data-btnEdit-id');
                var originalEmail = $(current_button).attr('data-original-email');
                
                $("#"+ btnEditId ).show();
                $("#"+ spnSaveId ).hide();
                $("#"+ txtEmailId ).val(originalEmail); // revert back to original value
                $("#"+ txtEmailId ).attr("disabled", "disabled"); 
            });
        });
    </script>
@endsection
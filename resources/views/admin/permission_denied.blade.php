
@extends('admin.layout')

@section('header')

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="alert alert-danger" style="width:70%" >Sorry, you do not have permission to view this page.</div>
    </div>
</div>

@endsection
@extends('admin.layout')

@section('header')
  <h1>Manually Generate Excel file for upload to Tri-e portal</h1>
@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

<form id="exportExcelform" method="post" action="{{ url('/export/manual_excel_generate_for_upload_post') }}">
    @csrf
    <div class="form-group">
        <div class="row">
            <div class="col-md-5">
            <label>Select date to generate excel for:</label>
                <input type="text" class="form-control" id="end_date" name="end_date" placeholder="Select Date" autocomplete="off" required>
            </div>
            <div class="col-md-2" style="top: 25px;">
                <button type="submit" class="btn btn-primary">Export to Excel</button>
            </div>
        </div>
    </div>
</form>
@endsection


@section('scripts')
    <script>
        $('#exportExcelform').validate();

        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var today = (day<10 ? '0' : '') + day + '-' + (month<10 ? '0' : '') + month + '-' + d.getFullYear();

         $( function() {
           
            $( "#end_date" ).datepicker({
                dateFormat: "dd-mm-yy",
                maxDate: today
            });
        });
    </script>
@endsection
@extends('admin/layout')

@section('header')

@endsection

<?php

$auth = new \SimpleSAML\Auth\Simple('example-sql');
$login_data = $auth->getAttributes();
$logged_in_admin_id = $login_data['id'];

?>

@section('content')
<div class="page_box">
    <h3>Users</h3>

    <h4>Users with Roles</h4>

    @if(isset($user_roles) && $user_roles !== null)
        <table id="users_table" class="table table-hover search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Username</th>
                <th>Role</th>

                @if ( in_array("edit user's role", $spData['permittedtasks']) )
                    <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($user_roles as $user_role)
            <tr>
                <td>{{  $user_role['username'] }}</td>
                <td>{{  $user_role['role_name'] }}</td>
                @if ( in_array("edit user's role", $spData['permittedtasks']) )
                    <td>
                        @if ( strcmp( $logged_in_admin_id , $user_role['user_id'] ) <> 0 )
                            <a href="{{ route('user_role.edit', [ 'user_id' =>  $user_role['user_id'],  'role_id' =>  $user_role['role_id'] ] ) }}">Edit</a>
                        @endif
                    </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
  
    @else
    <div class="alert alert-warning">No records found</div> 
    @endif

    <br><br>

    <h4>Users without Roles</h4>

    @if(isset($unasigned_users) && $unasigned_users !== null)
        <table id="users_table" class="table table-hover search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Username</th>
                @if ( in_array("assign role to user", $spData['permittedtasks']) )
                    <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($unasigned_users as $user)
            <tr>
                <td>{{  $user['username'] }}</td>
                @if ( in_array("assign role to user", $spData['permittedtasks']) )
                    <td>
                        @if ( strcmp( $logged_in_admin_id , $user['id'] ) <> 0 )
                            <a href="{{ route('user_role.assign', [ 'user_id' =>  $user['id'] ] ) }}">Assign Role</a>
                        @endif
                    </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>

        <br>
        
    @else
    <div class="alert alert-warning">No records found</div> 
    @endif

    
</div>
@endsection


@section('scripts')
    
@endsection
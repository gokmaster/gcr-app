@extends('admin/layout')

@section('header')

@endsection

<?php

$auth = new \SimpleSAML\Auth\Simple('example-sql');
$login_data = $auth->getAttributes();
$logged_in_admin_id = $login_data['id'];

?>

@section('content')
<div class="page_box">
    <h3>Users having Role: {{ $role_name }}</h3>

    @if(isset($role_users) && $role_users !== null)
        <table id="users_table" class="table table-hover search_paginate_sort_selectperpage">
            <thead>
            <tr>
            <th>Username</th>
            </tr>
            </thead>
            <tbody>
            @foreach($role_users as $role_user)
                <tr>
                    <td>{{  $role_user['username'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <br>

    @else
    <div class="alert alert-warning">No records found</div> 
    @endif
    
</div>
@endsection


@section('scripts')
    
@endsection
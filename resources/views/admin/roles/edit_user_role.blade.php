@extends('admin.layout')

@section('header')
<h3>Edit User Role</h3>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

              
                <div class="card-body">
                
                    <form id="roles_form" method="post" action="{{ url('/admin/role/assign_user_role_post')}}">
                        @csrf
                        <div class="form-group">
                            <label for="username">Username: </label>
                            <select class="form-control" disabled >
                                <?php
                                    $user_array;
                                    $user_json2;
                                ?>
                                @foreach($users as $user)
                                <?php 
                                    $user_arr = [
                                        'user_id' => $user['id'],
                                        'username' => $user['username'],
                                    ];
                                    $user_json = json_encode($user_arr);
                                ?>

                                <option value="{{ $user_json }}"                                 
                                    @if ($user_id == $user['id'])
                                        selected="selected" 
                                        <?php
                                            $user_array = [
                                                'user_id' => $user['id'],
                                                'username' => $user['username'],
                                            ];

                                            $user_json2 = json_encode($user_array);
                                        ?>
                                    @endif                           
                                >{{ $user['username'] }}</option>
                                @endforeach
                            </select>
                            <?php
                                $user_arr = [
                                    'user_id' => $user_id,
                                    'username' => $user['username'],
                                ];
                                $user_json = json_encode($user_arr);
                            ?>
                             <input type="hidden" name="username" value="{{ $user_json2 }}">
                        </div>

                        <div class="form-group">
                            <label for="role">Role: </label>
                            <select class="form-control" id="role" name="role" required>
                                @foreach($roles as $role)
                                    <option value="{{ $role['role_id'] }}"
                                        @if(session()->has('success_message') || session()->get('fail_message')  )
                                                @if (strcmp( old('role'), $role['role_id']) === 0)
                                                    selected="selected" 
                                                @endif                             
                                        @elseif ($role['role_id'] == $role_id)
                                            selected="selected" 
                                        @endif 
                                    >{{ $role['role_name'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        @if(session()->has('assign_success'))
                            <a href="{{ url('/admin/role/all_users_with_roles') }}" class="btn btn-primary" >Back to Users</a>
                        @else
                            <button id="btnAssignRole" type="submit" class="btn btn-primary">Save Edits</button> 
                            &nbsp
                            <a href="{{ url('/admin/role/all_users_with_roles') }}" class="btn btn-default" >Cancel</a>      
                        @endif
                        
                          
                    </form>
                    
                    
                    @if(session()->has('assign_success'))
                        
                    @else
                        <br>
                        <form id="roles_form" method="post" action="{{ route('user-role.unassign') }}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $user_id }}">
                            <button id="btnUnassignRole" type="submit" class="btn btn-primary">Unassign role for user</button> 
                        </form>      
                    @endif
                                        
                    @if (count($roleChanges) > 0)
                        <br>
                        <h4>Role Change History</h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Role Name</th>
                                    <th>Username</th>
                                    <th>Action Type</th>
                                    <th>Actioned By</th>
                                    <th>Log Updated At</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($roleChanges as $log)
                            <tr>
                                <td>{{ $log['role_name'] }}</td>
                                <td>{{ $log['username'] }}</td>
                                <td>{{ $log['action_type'] }}</td>
                                <td>{{ $log['admin'] }}</td>
                                <td>{{ $log['log_timestamp'] }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $("#roles_form").validate();
    </script>
@endsection

@extends('admin.layout')

@section('header')
<h3>Create Role</h3>
@endsection

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

              
                <div class="card-body">
                
                    <form id="roles_form" method="post" action="{{ url('/admin/role/insert')}}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" id="role_name" name="role_name" placeholder="Role Name e.g. IT admin" value="{{ old('role_name') }}" required>
                        </div>
                        <h3>Select the tasks for this role</h3>
                        <?php $chkbox_index = 0 ?>
                        @foreach($tasks as $label)
                            <div class="box" style="padding:5px;">
                                <h4>{{ strtoupper( $label[0]['label'] ) }}</h4>
                                @foreach($label as $task)
                                    <?php
                                        $input_id = str_replace(' ', '_', $task['task_name'] );
                                    ?>
                                    <label class="chkbox_container" >
                                        <input id="{{$input_id}}" type="checkbox" name="tasks[]" value="{{$task['task_id']}}" required
                                            @if(is_array( old('tasks') ) && in_array( $task['task_id'], old('tasks') )) 
                                                checked 
                                            @endif
                                        >
                                        &nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp

                                    <?php $chkbox_index++; ?>
                                @endforeach
                            </div>
                        @endforeach

                        <button id="btnCreateRole" type="submit" class="btn btn-primary">Create role</button>  
                        &nbsp
                        <a href="{{ url('/admin/role/list') }}" class="btn btn-default" >Cancel</a>  
                    </form>
                   
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("#roles_form").validate();
        });
    </script>

    <script src="{{ asset('js/role_create.js') }}" ></script>
@endsection

@extends('admin.layout')

@section('header')
<h3>Role: {{ $selected[0]['role_name'] }}</h3>
@endsection

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

                <div class="card-body">
                
                    <form id="roles_form" >
                        @csrf
                                               
                         <div class="box" style="padding:5px;">
                            <h4>BANK</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "bank" )
                                    <label class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" disabled>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                         <div class="box" style="padding:5px;">
                            <h4>CAMPAIGN</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "campaign" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" disabled>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>PRODUCT</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "product" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" disabled>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>REPORT</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "report" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" disabled>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>ROLE</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "role" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" disabled>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>SYSTEM</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "system" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" disabled>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>USERS</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "users" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" disabled>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <input id="role_id" name="role_id" type="hidden" value="{{ $role_id }}" >
                        <br>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>

<input id="selected_tasks" type="hidden" value="{{ json_encode($selected) }}" >


@endsection

@section('scripts')
    <script>
        $("#roles_form").validate();

        $( document ).ready(function() {

            var selected_tasks = $("#selected_tasks").val();
            selected_tasks_object = JSON.parse(selected_tasks);
                        
            $('.task_checkbox').each(function(i, obj) {

                $.each( selected_tasks_object, function( key, task ) {
                    var task_id =  $(obj).val();

                    if ( task['task_id'] == task_id) {
                        $(obj).prop('checked', true);
                    }
                });
               
            });
           
        });
    </script>

    <script src="{{ asset('js/role_create.js') }}" ></script>
@endsection

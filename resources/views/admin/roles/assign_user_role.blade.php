@extends('admin.layout')

@section('header')
<h3>Assign Role to User</h3>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

                <div class="card-body">
                
                    <form id="roles_form" method="post" action="{{ url('/admin/role/assign_user_role_post')}}">
                        @csrf
                        <div class="form-group">
                            <label for="username">Username: </label>
                            <select class="form-control" id="username" name="username" disabled>
                                @foreach($users as $user)
                                <?php 
                                    $user_array = [
                                        'user_id' => $user['id'],
                                        'username' => $user['username'],
                                    ];

                                    $user_json = json_encode($user_array);

                                    $auth = new \SimpleSAML\Auth\Simple('example-sql');
                                    $login_data = $auth->getAttributes();
                                    $logged_in_admin_id = $login_data['id'];
                                ?>

                                    @if ( strcmp( $logged_in_admin_id , $user['id'] ) <> 0 )
                                        <option value="{{ $user_json }}"
                                        @if ($user_id == $user['id'])
                                            selected="selected" 
                                            <?php
                                                $user_array = [
                                                    'user_id' => $user['id'],
                                                    'username' => $user['username'],
                                                ];

                                                $user_json2 = json_encode($user_array);
                                            ?>
                                        @endif                                        
                                        >{{ $user['username'] }}</option>
                                    @endif
                                @endforeach
                            </select>

                            <input type="hidden" name="username" value="{{ $user_json2 }}">
                        </div>

                        <div class="form-group">
                            <label for="role">Role: </label>
                            <select class="form-control" id="role" name="role" required>
                                <option value="" disabled selected hidden >Select Role</option>
                                @foreach($roles as $role)
                                    <option value="{{ $role['role_id'] }}" 
                                        @if(session()->has('success_message') || session()->get('fail_message')  )
                                            @if (strcmp( old('role'), $role['role_id']) === 0)
                                                selected="selected" 
                                            @endif                             
                                        @endif   
                                    >{{ $role['role_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <button id="btnAssignRole" type="submit" class="btn btn-primary">Assign role</button>    
                        &nbsp
                        <a href="{{ url('/admin/role/all_users_with_roles') }}" class="btn btn-default" >Cancel</a> 
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $("#roles_form").validate();
    </script>
@endsection

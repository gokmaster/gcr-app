@extends('admin.layout')

@section('header')
<h3>Delete Role</h3>
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @elseif(session()->has('fail_message'))
                <div class="alert alert-danger">
                    {{ session()->get('fail_message') }}
                </div>
            @endif

            
            <div class="card-body">

                <form id="role_form" method="post" action="{{ url('/admin/role/delete') }}">
                    @csrf
                    <input type="hidden" id="role_id" name="role_id" value="{{ $role_id }}">
                    Are you sure you want to this role: <b>{{ $role_name }}</b>?
                    <br><br>
                    <button id="btnDelete" type="submit" class="btn btn-primary">Delete</button>   
                    &nbsp  
                    <a href="{{ url('/admin/role/list') }}" class="btn btn-default" >Cancel</a> 
                </form>
                
            </div>
        </div>
    </div>
</div>


@endsection


@section('scripts')
    <script>
        $("#bank_form").validate();
    </script>
@endsection

@extends('admin/layout')

@section('header')

@endsection

@section('content')
<div class="page_box">
    <h3>Roles</h3>

    @if(isset($roles) && $roles !== null)
        <table class="table table-hover search_paginate_sort_selectperpage">
            <thead>
            <tr>
                <th>Role</th>
                <th>Users</th>
                @if ( in_array("edit role", $spData['permittedtasks']) || in_array("delete role", $spData['permittedtasks']))
                    <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            
            @foreach($roles as $role)
            <tr>
                <td>
                    @if ( in_array("view role tasks", $spData['permittedtasks']) )
                        <a href="{{ route('role.view_role_tasks', [ 'role_id' =>  $role['role_id'] ] ) }}">{{  $role['role_name'] }}</a> 
                    @else
                        {{ $role['role_name'] }}
                    @endif
                </td>
                <td>
                    @if ($role['user_count'] > 0)
                        <a href="{{ route('role.view_users_of_role', [ 'role_id' =>  $role['role_id'] , 'role_name' =>  $role['role_name'] ] ) }}">View users</a>
                    @endif
                </td>
                @if ( in_array("edit role", $spData['permittedtasks']) || in_array("delete role", $spData['permittedtasks']))
                    <td>
                        @if ( in_array("edit role", $spData['permittedtasks']) )
                            <a href="{{ route('role.edit_form', [ 'role_id' =>  $role['role_id'] ] ) }}">Edit</a> 
                        @endif    
                        @if ( in_array("edit role", $spData['permittedtasks']) && in_array("delete role", $spData['permittedtasks']) )
                            |
                        @endif
                        @if ( in_array("delete role", $spData['permittedtasks']) )    
                            <a href="{{ route('role.delete_form', [ 'role_name' =>  $role['role_name'] , 'role_id' =>  $role['role_id'] ] ) }}">Delete</a>
                        @endif
                    </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-warning">No records found</div> 
    @endif

    <br>

    <div class="page_bottom_div"> 
        @if ( in_array("create role", $spData['permittedtasks']) )
            <a class="btn btn-primary" href="{{ url('/admin/role/create')}}">Create Role</a>
        @endif
    </div>

   
    
</div>



@endsection
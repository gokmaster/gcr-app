@extends('admin.layout')

@section('header')
<h3>Edit Role</h3>
@endsection

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

                <div class="card-body">
                
                    <form id="roles_form" method="post" action="{{ url('/admin/role/editpost') }}" >
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" id="role_name" name="role_name" placeholder="Role Name e.g. IT admin" required
                                @if(session()->has('success_message') || session()->get('fail_message')  )
                                    value="{{ old('role_name') }}"
                                @else
                                    value="{{ $selected[0]['role_name'] }}"
                                @endif
                            >
                        </div>
                        <h3>Select tasks for this role</h3>

                         <div class="box" style="padding:5px;">
                            <h4>BANK</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "bank" )
                                    <label class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" required>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                         <div class="box" style="padding:5px;">
                            <h4>CAMPAIGN</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "campaign" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" required>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>PRODUCT</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "product" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" required>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>REPORT</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "report" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" required>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>ROLE</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "role" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" required>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>SYSTEM</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "system" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" required>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <div class="box" style="padding:5px;">
                            <h4>USERS</h4>
                            @foreach($tasks as $task)
                                @if ( $task['label'] == "users" )
                                    <label  class="chkbox_container" >
                                        <input type="checkbox" class="task_checkbox" name="tasks[]" value="{{$task['task_id']}}" required>&nbsp {{$task['task_name']}}
                                        <span class="checkmark"></span>
                                    </label>&nbsp &nbsp &nbsp
                                @endif
                            @endforeach
                        </div>

                        <input id="role_id" name="role_id" type="hidden" value="{{ $role_id }}" >
                        <br>
                        <button id="btnEditRole" type="submit" class="btn btn-primary">Save edits</button>    
                        &nbsp
                        <a href="{{ url('/admin/role/list') }}" class="btn btn-default" >Cancel</a>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>

<input id="selected_tasks" type="hidden" value="{{ json_encode($selected) }}" >


@endsection

@section('scripts')
    <script>
        $("#roles_form").validate();

        $( document ).ready(function() {

            var selected_tasks = $("#selected_tasks").val();
            selected_tasks_object = JSON.parse(selected_tasks);
                
            $('.task_checkbox').each(function(i, obj) {

                // Apply check to check-boxes
                $.each( selected_tasks_object, function( key, task ) {
                    var task_id =  $(obj).val();

                    if ( task['task_id'] == task_id) {
                        $(obj).prop('checked', true);
                    }
                });
               
            });
           
        });
    </script>

    <script src="{{ asset('js/role_create.js') }}" ></script>
@endsection

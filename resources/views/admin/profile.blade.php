@extends('admin.layout')

@section('assets')
 
@endsection

@section('header')
  <h3>Profile</h3>
@endsection

@section('content')

<div class="panel panel-default">
  <div class="panel-heading"><b>Name</b></div>
  <div class="panel-body">{{ $user['first_name'] }}</div>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><b>Email</b></div>
  <div class="panel-body">{{ $user['email'] }}</div>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><b>Username</b></div>
  <div class="panel-body">{{ $user['username'] }}</div>
</div>

@endsection
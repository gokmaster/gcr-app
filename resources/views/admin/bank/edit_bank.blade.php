@extends('admin.layout')

@section('header')
<h3>Edit Bank</h3>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

                
                <div class="card-body">

                    <form id="bank_form" method="post" action="{{ url('/admin/bank/update')}}">
                        @csrf
                        <input type="hidden" id="bank_id" name="bank_id" value="{{ $bank_id }}" >
                        <div class="form-group">
                            <label for="name">Bank name: </label>
                            <input type="text" class="form-control" id="name" name="name" required
                            @if(session()->has('success_message') || session()->get('fail_message')  )
                                value="{{ old('name') }}"
                            @else
                                value="{{ $bankname }}"
                            @endif>
                        </div>
                                              
                        <button id="btnSave" type="submit" class="btn btn-primary">Save</button> 
                        &nbsp   
                        <a href="{{ url('admin/bank/all') }}" class="btn btn-default" >Cancel</a>  
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
    <script>
        $("#bank_form").validate();
    </script>
@endsection

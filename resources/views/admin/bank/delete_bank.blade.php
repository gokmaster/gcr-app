@extends('admin.layout')

@section('header')
<h3>Delete Bank</h3>
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @elseif(session()->has('fail_message'))
                <div class="alert alert-danger">
                    {{ session()->get('fail_message') }}
                </div>
            @endif

            
            <div class="card-body">

                <form id="bank_form" method="post" action="{{ url('/admin/bank/delete')}}">
                    @csrf
                    <input type="hidden" id="bank_id" name="bank_id" value={{ $bank_id }}>
                    Are you sure you want to delete <b>{{ $bankname }}</b> bank?
                    <br><br>
                    <button id="btnDelete" type="submit" class="btn btn-primary">Delete</button>   
                    &nbsp  
                    <a href="{{ url('admin/bank/all') }}" class="btn btn-default" >Cancel</a>   
                </form>
                
            </div>
        </div>
    </div>
</div>


@endsection


@section('scripts')
    <script>
        $("#bank_form").validate();
    </script>
@endsection

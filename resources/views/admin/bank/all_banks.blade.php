@extends('admin/layout')

@section('assets')
<style>
    .bank_row {
        padding: 4px 2px 3px;
        border: solid 1px #94b8b8;
    }
</style>
@endsection

@section('header')

@endsection

@section('content')
<div class="page_box">
    <h3>Banks</h3>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

    <div class="row">
        <div class="col-sm-8">
            Filter campaigns: &nbsp &nbsp
            <label  class="chkbox_container" >
                <input id="chkbox-active" type="checkbox" class="filter-checkbox" name="filter[]" value="1" checked>Active
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp

            <label  class="chkbox_container" >
                <input id="chkbox-disabled" type="checkbox" class="filter-checkbox" name="filter[]" value="3" checked>Disabled
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp
        </div>

        <div class="col-sm-4">
            <div class="page_bottom_div"> 
                @if ( in_array("add bank", $spData['permittedtasks']) )
                    <a class="btn btn-primary" href="{{ url('/admin/bank/add') }}">Add New Bank</a>
                @endif
            </div>
        </div>
    </div>
    <br>

    <div class="row bank_row">
        <div class="col-sm-7">
            <b>Bank</b>
        </div>
        <div class="col-sm-5">
           <b>Action</b>
        </div>                 
    </div>
    @if(isset($banks) && $banks !== null)
       
        @foreach($banks as $bankname=>$bank)
            <div @if ( strcmp($bank[0]['bank_deleted'],1) === 0) class="row bank_row disabled_row disabled-bank" @else  class="row bank_row active-bank"  @endif>
                <div class="col-sm-7">
                    @if ( count($bank) > 1)
                        <a  class="show_campaigns" data-bank-table-id="tablebank_{{$bank[0]['bankId'] }}" href="#">+ &nbsp &nbsp</a> 
                    @else 
                        &nbsp &nbsp  &nbsp
                    @endif
                    {{ $bankname }} 
                </div>
                <div class="col-sm-5">
                    @if ( in_array("delete bank", $spData['permittedtasks']) )
                        @if ( strcmp($bank[0]['show_disable'],1) === 0 )
                        <form class="form-no-margin" id="bank_form" method="post" action="{{ url('/admin/bank/delete')}}">
                            @csrf
                            <input type="hidden" id="bank_id" name="bank_id" value="{{ $bank[0]['bankId']  }}">
                            <button id="btnDelete" type="submit" class="inline-button btn btn-link">Disable</button>   
                        </form>
                        @endif
                    @endif
                </div>                 
            </div>
            
            @if ( count($bank) > 1)
                <table id="tablebank_{{$bank[0]['bankId'] }}" class="table campaign-table" style="display:none;">
                    <col width="190">
                    <thead>
                        <tr>
                            <th>Campaign</th>
                            <th>Created on</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($bank as $campaign)
                        <tr 
                            @if ( strcmp($campaign['deleted'],1) === 0) class='disabled_row' @endif
                            @if (strtotime($campaign['end_date']) < $today && strcmp($campaign['deleted'],0) === 0)) class="expired_campaign" @endif
                        >
                            <td>
                                @if ( strcmp($campaign['deleted'],0) === 0)
                                    <a href="{{ route('campaign.edit', ['campaign_id' => $campaign['campaign_id']]) }}" >{{ ($campaign['title']) }}<a>
                                @else
                                    {{ ($campaign['title']) }}
                                @endif
                            </td>
                            <td>{{ $campaign['created_on'] }}</td>
                            <td>{{ $campaign['start_date'] }}</td>
                            <td>{{ $campaign['end_date'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                <table>
            @endif
           
        @endforeach
     
    @else
    <div style="color:grey; padding-top: 10px" >Click the 'Add New Bank' Button to start filling this list.</div>
    @endif

    <br>    
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $(".show_campaigns").on( "click", function(e) {
                var current_button = e.target;

                var banktableId = $(current_button).attr('data-bank-table-id');
                               
                $("#"+ banktableId ).toggle();
            });

             // filter camapigns by active, expired, disabled
             $('.filter-checkbox').change(function() {
                if($('#chkbox-active').is(":checked")) {
                    $('.active-bank').show();
                } else {
                    $('.active-bank').hide();
                    $('.campaign-table').hide();
                }   

                if($('#chkbox-disabled').is(":checked")) {
                    $('.disabled-bank').show();
                    $('.campaign-table').hide();
                } else {
                    $('.disabled-bank').hide();
                }  
            });
        });
    </script>
@endsection




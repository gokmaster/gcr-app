@extends('admin/layout')

@section('header')

@endsection

@section('content')
<div class="page_box">
    <h3>Download unique codes for Campaign: {{ $batches[0]['campaign_title'] }} </h3>

        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Batch</th>
                <th>Created on</th>
                <th>Unique Code Quantity</th>
                <th>Download</th>
            </tr>
            </thead>
            <tbody>            
            @foreach($batches as $batch)
            <tr>
                <td>
                   {{ $batch['bank_name'] }}
                </td>
                <td>
                   {{ $batch['campaign_title'] }}
                </td>
                <td>
                   {{ $batch['batch'] }}
                </td>
                <td>
                   {{ $batch['created_on'] }}
                </td>
                <td>
                   {{ $batch['unique_code_qty'] }}
                </td>
                <td>
                    <a href="{{route('unique_code.excel_download.onebatch' , [ 'campaign_id' => $batch['campaign_id'], 'batch' => $batch['batch'] ])}}">
                       Download
                    </a>
                </td>         
            </tr>
            @endforeach
            </tbody>
        </table>  
</div>

@endsection
@extends('admin.layout')

@section('header')
<h3>Add Unique Code for Campaign {{ $campaign_name }}</h3>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @elseif(session()->has('fail_message'))
                <div class="alert alert-danger">
                    {{ session()->get('fail_message') }}
                </div>
            @endif

            
            <div class="card-body">
            
                <form id="campaign_form" method="post" action="{{ url('/campaign/add_unique_code/post') }}" enctype="multipart/form-data">
                    @csrf
                    
                    <div id="unique_code_qty_div" class="form-group">
                        <br>
                        <label for="unique_code_qty">How many unique-codes to generate?: </label>
                        <input type="number" class="form-control" id="unique_code_qty" name="unique_code_qty" value="{{old('unique_code_qty')}}" min="1" max="100000" required>
                        <input type="hidden" id="campaign_id" name="campaign_id" value="{{ $campaign_id }}" >
                    </div>

                    <b>Expiry date: &nbsp; </b>
                    <label class="radio-inline" style=""><input type="radio" name="expiry_enabled" value="1" required
                        @if(strcmp(old('expiry_enabled'), "0") === 0 )
                            checked
                        @endif >
                        Enable
                    </label>
                    <label class="radio-inline" style=""><input type="radio" name="expiry_enabled" value="0"
                        @if(strcmp(old('expiry_enabled'), "1") === 0 )
                            checked
                        @endif >
                        Disable
                    </label>
                    <br><br>

                    <div id="expiry-div" style="display:none;">
                        <br>
                        <label>Valid From:</label>
                        <input type="text" class="form-control" id="expiry_startdate" name="expiry_startdate" placeholder="Select Date" autocomplete="off" required>

                        <br>
                        <div class="form-group">
                            <label for="days_from_expiry_startdate">Valid for how many days: </label>
                            <input type="number" class="form-control" id="days_from_expiry_startdate" name="days_from_expiry_startdate" value="{{old('days-valid')}}" min="0" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="remark">Remark: </label>
                        <input type="text" class="form-control" id="remark" name="remark" value="{{old('remark')}}"  required>
                    </div>
                  
                    <br>
                    
                    <button id="btnAddUniqueCode" type="submit" class="btn btn-primary">Add</button>  
                    &nbsp
                    <a href="{{ url('/campaign/all') }}" class="btn btn-default" >Cancel</a>  
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $("#campaign_form").validate();

        $( function() {
           
           $( "#expiry_startdate" ).datepicker({
               dateFormat: "dd-mm-yy",
           });
       });

        $(document).ready(function () {
            $("input:radio[name='expiry_enabled']").change(function () {
                if ($("input[name='expiry_enabled']:checked").val() == 1 ) {
                    $("#expiry-div").show();
                } else {
                    $("#expiry-div").hide();
                }
            });
        });

    </script>
@endsection

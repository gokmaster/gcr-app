@extends('admin.layout')

@section('header')

@endsection

@section('content')
<div class="container">
    <h3>Edit Campaign</h3>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif
           
                <div class="card-body">
                    <b>Created by:</b> {{ $campaign[0]['createdbyusername'] }} 
                    &nbsp &nbsp &nbsp &nbsp
                    <b>Created on:</b> {{ date('d M, Y', strtotime($campaign[0]['created_on'])) }}
                    <br><br>
                
                    <form id="campaign_form" method="post" action="{{ url('/campaign/update')}}">
                        @csrf
                        <input type="hidden" id="campaign_id" name="campaign_id" value="{{ $campaign[0]['campaign_id'] }}">
                        <div class="form-group">
                            <label for="campaign_name">Campaign Title: 
                            </label>
                            <input type="text" class="form-control" id="campaign_title" name="title" required  
                                @if( old('title') ) 
                                    value="{{ old('title') }}" 
                                @else 
                                    value="{{ $campaign[0]['title'] }}" 
                                @endif
                                >
                        </div>
                        <div class="form-group">
                            <label for="campaign_name">Campaign Name for url: </label>
                            <input type="text" class="form-control" id="campaign_name" name="campaign_name" required disabled
                                value="{{ $campaign[0]['name'] }}"
                            >
                        </div>
                                              
                        <div class="form-group">
                            <label for="campaign_provider">Bank: </label>
                            <select class="form-control" id="campaign_provider" name="campaign_provider"  disabled>
                                @foreach($banks as $bank)
                                <option value="{{ $bank['bank_id'] }}" 
                                    @if ($campaign[0]['bank_id'] === $bank['bank_id'])
                                        selected="selected"                             
                                    @endif>
                                    {{ $bank['name'] }}
                                </option>
                                @endforeach
                            </select>
                        </div> 

                        <div class="form-group shadow-textarea">
                            <label for="description">Description</label>
                            <textarea class="form-control z-depth-1" id="description" name="description" rows="3" placeholder="Enter description here">@if (old('description')) {{ old('description') }} @else {{ $campaign[0]['description'] }} @endif</textarea>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                            <label>Start date:</label>
                            <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" value="{{ date('d-m-Y', strtotime($campaign[0]['start_date'])) }}"  required>
                            </div>
                            <div class="col-md-5">
                            <label>End date:</label>
                            <input type="text" class="form-control" id="end_date" name="end_date" placeholder="End date" value="{{ date('d-m-Y', strtotime($campaign[0]['end_date'])) }}" required>
                            </div>
                        </div>

                        <br>

                        <div class="form-group">
                            <label for="campaign_name">Reason for edit: </label>
                            <input type="text" class="form-control" id="reason" name="reason" required
                                value="{{old('reason')}}"
                            >
                        </div>

                        <br>

                        <button id="btnSaveEdits" type="submit" class="btn btn-primary">Save Edits</button> 
                        &nbsp
                        <a href="{{ url('/campaign/all') }}" class="btn btn-default" >Cancel</a>  
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
    <script>
        $("#campaign_form").validate();
    </script>

    <script src="{{ asset('js/datepicker.js') }}" ></script>
@endsection

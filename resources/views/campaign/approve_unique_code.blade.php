@extends('admin/layout')

@section('header')

@endsection

@section('content')

<div class="page_box">
    <h3>Approve unique codes for Campaign: {{ $campaign_name }} </h3>

    <div id="approve-progress-div" style="display:none;">
        <i class="fa fa-spinner fa-spin" style="font-size:32px; color: blue;"></i>
        <i id="progress-timer"></i>
        <b>Unique codes are being generated. Please wait!</b>
    </div>
    <div id="success-response-div" class="alert alert-success" style="display:none;">
    </div>
    <div id="failed-response-div" class="alert alert-danger" style="display:none;">
    </div>
    <br>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif

        <table id="approval-table" class="table table-hover search_paginate_sort_selectperpage">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>Batch</th>
                <th>Unique Code Quantity</th>
                <th>Date Created</th>
                <th>Expiry Enabled</th>
                <th>Valid From</th>
                <th>Days Valid For</th>
                <th>Remark</th>
                <th>Created By</th>
                <th>Approved/Rejected By</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>            
            @foreach($batches as $batch)
            <tr>
                <td>
                   {{ $batch['bank_name'] }}
                </td>
                <td>
                   {{ $batch['campaign_name'] }}
                </td>
                <td>
                   {{ $batch['batch'] }}
                </td>
                <td>
                   {{ $batch['unique_code_qty'] }}
                </td>
                <td>
                   {{ $batch['created_on'] }}
                </td>
                <td>
                    @if ($batch['expiry_enabled'] === 1) 
                        Enabled
                    @else 
                        Disabled
                    @endif
                    <a href="{{ route('campaign.edit_unique_code_expiry' , [ 'campaign_id' =>  $batch['campaign_id'], 'batch' => $batch['batch']  ] )}}">Edit</a> 
                </td>
                <td>
                   {{ $batch['expiry_startdate'] }}
                </td>
                <td>
                   {{ $batch['days_from_expiry_startdate'] }}
                </td>
                <td>
                   {{ $batch['remark'] }}
                </td>
                <td>
                   {{ $batch['created_by'] }}
                </td>
                <td>
                   {{ $batch['approved_by'] }}
                </td>

                <td>
                   @if( strcmp($batch['status'], 0) === 0  ) 
                        Pending approval
                   @elseif ( strcmp($batch['status'], 1) === 0  )
                        Approved
                    @elseif ( strcmp($batch['status'], 2) === 0  )
                        Rejected
                   @endif
                </td>

                <td>
                   @if( strcmp($batch['status'], 0) === 0  ) 
                    <form method="post" action="{{ url('/campaign/unique_code/approve') }}">
                        @csrf
                        <input type="hidden" class="campaign_id" name="campaign_id" value="{{ $batch['campaign_id'] }}">
                        <input type="hidden" class="batch" name="batch" value="{{ $batch['batch'] }}">
                        <input type="hidden" class="unique_code_qty" name="unique_code_qty" value="{{ $batch['unique_code_qty'] }}">
                        <input type="hidden" class="unique_codes_pending_id" name="unique_codes_pending_id" value="{{ $batch['unique_codes_pending_id'] }}">
                        
                        <a class="btnApprove btn btn-primary">Approve</a> 
                    </form>
                    <form method="post" action="{{ route('uniqueCode.reject') }}">
                        @csrf
                        <input type="hidden" id="unique_codes_pending_id" name="unique_codes_pending_id" value="{{ $batch['unique_codes_pending_id'] }}">
                        <button type="submit" class="btnReject btn btn-default">Reject</button>  
                    </form>
                   @endif
                </td>
              
            </tr>
            @endforeach
            </tbody>
        </table>  
</div>

@endsection


@section('scripts')
    <script>
        $("#campaign_form").validate({
            rules: {
                unique_code_qty: {
                    min: 1,
                    digits: true,
                }
            }
        });

        $(document).ready(function () {
            $('input:radio[name=verification_type]').change(function () {
                if ($("input[name='verification_type']:checked").val() == 1
                    || $("input[name='verification_type']:checked").val() == 3 ) {
                    $("#unique_code_qty_div").show();
                } else {
                    $("#unique_code_qty_div").hide();
                }
            });


            $(".btnApprove").click(function (e) {
                $('#approve-progress-div').show();
                var currentbutton = e.target;

                var campaign_id = $(currentbutton).parent().find(".campaign_id").val();
                var batch = $(currentbutton).parent().find(".batch").val();
                var unique_code_qty = $(currentbutton).parent().find(".unique_code_qty").val();
                var unique_codes_pending_id = $(currentbutton).parent().find(".unique_codes_pending_id").val();

                $.post("{{ url('/campaign/unique_code/approve') }}", 
                {       _token: "{{ csrf_token() }}",
                        campaign_id: campaign_id, 
                        batch: batch,
                        unique_code_qty: unique_code_qty, 
                        unique_codes_pending_id: unique_codes_pending_id
                    },
                    function(result){
                        var obj = JSON.parse(result);
                        if (obj.status = "success") {
                            $("#success-response-div").html(obj.message);
                            $("#success-response-div").show();

                            $( "#approval-table" ).load(window.location.href + " #approval-table" );
                        } else {
                            $("#failed-response-div").html(obj.message);
                            $("#failed-response-div").show();
                        }
                        
                        $('#approve-progress-div').hide();
                });
            
            });
        });

        function get_elapsed_time_string(total_seconds) {

            function pretty_time_string(num) {
                return ( num < 10 ? "0" : "" ) + num;
            }

            var hours = Math.floor(total_seconds / 3600);
            total_seconds = total_seconds % 3600;

            var minutes = Math.floor(total_seconds / 60);
            total_seconds = total_seconds % 60;

            var seconds = Math.floor(total_seconds);

            // Pad the minutes and seconds with leading zeros, if required
            hours = pretty_time_string(hours);
            minutes = pretty_time_string(minutes);
            seconds = pretty_time_string(seconds);

            // Compose the string for display
            var currentTimeString = hours + ":" + minutes + ":" + seconds;

            return currentTimeString;
        }

        var elapsed_seconds = 0;
        
        setInterval(function() {
            elapsed_seconds = elapsed_seconds + 1;
            $('#progress-timer').text(get_elapsed_time_string(elapsed_seconds));
        }, 1000);

    </script>

    <script src="{{ asset('js/datepicker.js') }}" ></script>
@endsection




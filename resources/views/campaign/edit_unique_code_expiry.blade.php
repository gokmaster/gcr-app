@extends('admin.layout')

@section('header')
<h3>Edit Unique Code Expiry</h3>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @elseif(session()->has('fail_message'))
                <div class="alert alert-danger">
                    {{ session()->get('fail_message') }}
                </div>
            @endif

            <b>Campaign:</b> {{ $details['campaign'] }}  <br>
            <b>Batch:</b> {{ $details['batch'] }}

            
            
            <div class="card-body">
            
                <form id="campaign_form" method="post" action="{{ route('campaign.edit_unique_code_expiry_post') }}" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" id="campaign_id" name="campaign_id" value="{{ $details['campaign_id'] }}">

                    <input type="hidden" id="campaign" name="campaign" value="{{ $details['campaign'] }}">

                    <input type="hidden" id="batch" name="batch" value="{{ $details['batch'] }}">
                    
                    <b>Expiry date: &nbsp; </b>
                    <label class="radio-inline" style=""><input type="radio" name="expiry_enabled" value="1" required
                        @if( $details['expiry_enabled'] === 1 )
                            checked
                        @endif >
                        Enable
                    </label>
                    <label class="radio-inline" style=""><input type="radio" name="expiry_enabled" value="0"
                        @if( $details['expiry_enabled'] === 0 )
                            checked
                        @endif > 
                        Disable
                    </label>
                    <br><br>

                    <div id="expiry-div" @if ($details['expiry_enabled'] === 0) style="display:none;" @endif >
                        <br>
                        <label>Valid From:</label>
                        <input type="text" class="form-control" id="expiry_startdate" name="expiry_startdate" placeholder="Select Date" autocomplete="off"  value="{{ date( 'd-m-y', strtotime($details['expiry_startdate'])) }}" required >

                        <br>
                        <div class="form-group">
                            <label for="days_from_expiry_startdate">Valid for how many days: </label>
                            <input type="number" class="form-control" id="days_from_expiry_startdate" name="days_from_expiry_startdate" value="{{ $details['days_from_expiry_startdate'] }}" min="0" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="remark">Remark: </label>
                        <input type="text" class="form-control" id="remark" name="remark" value="{{ $details['remark'] }}"  required>
                    </div>
                  
                    <br>
                    
                    <button id="btnAddUniqueCode" type="submit" class="btn btn-primary">Save</button>  
                    &nbsp
                    <a href="{{ url('/campaign/all') }}" class="btn btn-default" >Cancel</a>  
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $("#campaign_form").validate();

        $( function() {
           
           $( "#expiry_startdate" ).datepicker({
               dateFormat: "dd-mm-yy",
           });
       });

        $(document).ready(function () {
            $("input:radio[name='expiry_enabled']").change(function () {
                if ($("input[name='expiry_enabled']:checked").val() == 1 ) {
                    $("#expiry-div").show();
                } else {
                    $("#expiry-div").hide();
                }
            });
        });

    </script>
@endsection

@extends('admin/layout')

@section('assets')

@endsection

@section('header')

@endsection

@section('content')

<div class="page_box">

    <h3>Campaigns</h3>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

    <div class="row">
        <div class="col-sm-8">
            Filter campaigns: &nbsp &nbsp
            <label  class="chkbox_container" >
                <input id="chkbox-active" type="checkbox" class="filter-checkbox" name="filter[]" value="1" checked>Active
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp

            <label  class="chkbox_container" >
                <input id="chkbox-expired" type="checkbox" class="filter-checkbox" name="filter[]" value="2" checked>Expired
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp

            <label  class="chkbox_container" >
                <input id="chkbox-disabled" type="checkbox" class="filter-checkbox" name="filter[]" value="3" checked>Disabled
                <span class="checkmark"></span>
            </label>&nbsp &nbsp &nbsp
        </div>

        <div class="col-sm-4">
            <div class="page_bottom_div">
                @if ( in_array("add campaign", $spData['permittedtasks']) )
                    <a class="btn btn-primary" href="{{ url('/campaign/create')}}">Create Campaign</a>
                @endif
            </div>
        </div>
    </div>

    <div id="active-div">
        <h4>Active Campaigns</h4>

        <table class="table search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Description</th>
                <th>Created on</th>
                <th>Start Date</th>
                <th>End Date</th>
                @if ( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks']))
                    <th>Action</th>
                @endif 
                @if ( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) )     
                    <th>Products</th>
                @endif
                @if ( in_array("upload unique code masterlist", $spData['permittedtasks']) )
                    <th>Upload Excel</th>
                @endif
                <th>Unique-code type</th>
                @if ( in_array("download unique codes", $spData['permittedtasks']) )
                    <th>Unique-codes</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($campaigns as $campaign)
            <tr>
                <td>
                    <a href="{{route('campaign.view', ['campaign_name' => $campaign['name']]) }}" target="_blank">
                        {{ $campaign['title'] }}
                    </a>
                </td>
                <td>{{ $campaign['bank_name'] }}</td>
                <td>{{ $campaign['description'] }}</td>
                <td>{{ $campaign['created_on'] }}</td>
                <td>{{ $campaign['start_date'] }}</td>
                <td>{{ $campaign['end_date'] }}</td>

                @if ( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks']))
                    <td>
                        @if ( in_array("edit campaign", $spData['permittedtasks']) )
                            <a href="{{route('campaign.edit', ['campaign_id' => $campaign['campaign_id']]) }}">
                                Edit
                            </a>
                        @endif
                        @if ( in_array("edit campaign", $spData['permittedtasks']) && in_array("delete campaign", $spData['permittedtasks']) )
                            |
                        @endif
                        @if ( in_array("delete campaign", $spData['permittedtasks']) )
                            <form class="form-no-margin" method="post" action="{{ url('/campaign/delete') }}">
                                @csrf
                                <input type="hidden" name="campaign_id" value="{{ $campaign['campaign_id'] }}">                                           
                                    <button type="submit" class="inline-button btn btn-link">
                                        Disable
                                    </button>
                            </form>
                        @endif
                    </td>
                @endif

                @if ( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) )
                    <td>
                        @if ( in_array("add product", $spData['permittedtasks']) )
                            <a href="{{route('product.add', ['campaign_id' => $campaign['campaign_id']]) }}">
                                Add Product
                            </a>
                        @endif
                        @if ( in_array("add product", $spData['permittedtasks']) &&  in_array("view all products", $spData['permittedtasks']) &&  $campaign['has_products'] == 1)
                            |
                        @endif
                        @if ( in_array("view all products", $spData['permittedtasks']) )
                            @if ( $campaign['has_products'] == 1 )
                                <a href="{{route('product.show_campaign_products', ['campaign_id' => $campaign['campaign_id']]) }}">
                                        View
                                </a>
                            @endif
                        @endif
                    </td>
                @endif
                
                @if ( in_array("upload unique code masterlist", $spData['permittedtasks']) )
                    @if ( strcmp($campaign['verification_type'], 2) ===0 ||  strcmp($campaign['verification_type'], 3) ===0 )
                        <td>
                            
                                @if ( $campaign['excel_uploaded'] == 0 )
                                    <a href="{{route('campaign.excel_uploadform_bank_masterlist', ['campaign_name' => $campaign['name'], 'campaign_id' => $campaign['campaign_id'], 'verificationType' => $campaign['verification_type'] ]) }}">
                                        Upload
                                    </a>
                                @else
                                    <a href="{{route('campaign.excel_uploadform_bank_masterlist', ['campaign_name' => $campaign['name'], 'campaign_id' => $campaign['campaign_id'], 'verificationType' => $campaign['verification_type'] ]) }}">
                                        Upload
                                    </a> | 
                                    <a href="{{ route('campaign.exceluploadhistory', ['campaign_id' => $campaign['campaign_id'] ]) }}">View History</a>
                                @endif
                            
                        </td>
                    @else
                        <td>N/A</td>
                    @endif
                @endif

                <td>
                    @if ( in_array("add unique codes", $spData['permittedtasks']) )
                        @if ( $campaign['verification_type'] == 2 )
                            N/A
                        @else
                            <a href="{{route('campaign.add_unique_code', ['campaign_id' => $campaign['campaign_id'] , 'campaign_name' => $campaign['name']]) }}">
                                Add
                            </a>
                        @endif
                        |
                    @endif

                    @if ( in_array("approve unique codes", $spData['permittedtasks']) )
                        <a href="{{route('campaign.approve_unique_code', ['campaign_name' => $campaign['name'] , 'campaign_id' => $campaign['campaign_id']]) }}">
                            View Approval
                        </a>
                        |
                    @endif

                    @if ( strcmp($campaign['verification_type'], 1) ===0 )
                        Unique-code only
                    @elseif ( strcmp($campaign['verification_type'], 2) ===0 )
                        Unique-code & last 6-digit (GCR)
                    @elseif ( strcmp($campaign['verification_type'], 3) ===0 )
                        Unique-code & last 6-digit (Bank)
                    @endif
                </td>

                @if ( in_array("download unique codes", $spData['permittedtasks']) )
                    <td>
                        @if  ( $campaign['batch_count'] === 0 )
                            N/A
                        @elseif ( $campaign['batch_count'] === 1 )
                            <a href="{{route('unique_code.excel_download.onebatch' , [ 'campaign_id' => $campaign['campaign_id'], 'batch' => $campaign['batch']] ) }}">
                                Download
                            </a>
                        @elseif ( 
                            $campaign['verification_type'] == 1 || $campaign['verification_type'] == 3 
                            || ($campaign['verification_type'] == 2 && $campaign['excel_uploaded'] == 1) 
                            && $campaign['batch_count']  > 0 
                        )
                            <a href="{{route('unique_code.download.batches', ['campaign_id' => $campaign['campaign_id']]) }}">
                                Download
                            </a>
                        @elseif  ( $campaign['verification_type'] == 2 )
                            @if ( $campaign['excel_uploaded'] <> 1 )
                                N/A
                            @endif
                        @endif
                    </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
        <hr>
    </div>

    <div id="expired-div">
        <h4>Expired Campaigns</h4>
        
        <table id="expired-camapigns-table" class="table search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Description</th>
                <th>Created on</th>
                <th>Start Date</th>
                <th>End Date</th>
                @if ( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks']))
                    <th>Action</th>
                @endif 
                @if ( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) )     
                    <th>Products</th>
                @endif
                @if ( in_array("upload unique code masterlist", $spData['permittedtasks']) )
                    <th>Upload Excel</th>
                @endif
                <th>Unique-code type</th>
                @if ( in_array("download unique codes", $spData['permittedtasks']) )
                    <th>Unique-codes</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($expiredCampaigns as $campaign)
            <tr class="expired_campaign" >
                <td>
                    {{ $campaign['title'] }}
                </td>
                <td>{{ $campaign['bank_name'] }}</td>
                <td>{{ $campaign['description'] }}</td>
                <td>{{ $campaign['created_on'] }}</td>
                <td>{{ $campaign['start_date'] }}</td>
                <td>{{ $campaign['end_date'] }}</td>

                @if ( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks']))
                    <td>
                        @if ( in_array("edit campaign", $spData['permittedtasks']) )
                            <a href="{{route('campaign.edit', ['campaign_id' => $campaign['campaign_id']]) }}">
                                Edit
                            </a>
                        @endif
                        @if ( in_array("edit campaign", $spData['permittedtasks']) && in_array("delete campaign", $spData['permittedtasks']) )
                            |
                        @endif
                        @if ( in_array("delete campaign", $spData['permittedtasks']) )
                            <form class="form-no-margin" method="post" action="{{ url('/campaign/delete') }}">
                                @csrf
                                <input type="hidden" name="campaign_id" value="{{ $campaign['campaign_id'] }}">                                           
                                    <button type="submit" class="inline-button btn btn-link">
                                        Disable
                                    </button>
                            </form>
                        @endif
                    </td>
                @endif

                @if ( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) )
                    <td>
                        @if ( in_array("add product", $spData['permittedtasks']) )
                                Add Product
                        @endif
                        @if ( in_array("add product", $spData['permittedtasks']) &&  in_array("view all products", $spData['permittedtasks']) &&  $campaign['has_products'] == 1)
                            |
                        @endif
                        @if ( in_array("view all products", $spData['permittedtasks']) )
                            @if ( $campaign['has_products'] == 1 )
                                <a href="{{route('product.show_campaign_products', ['campaign_id' => $campaign['campaign_id']]) }}">
                                        View
                                </a>
                            @endif
                        @endif
                    </td>
                @endif
                
                @if ( in_array("upload unique code masterlist", $spData['permittedtasks']) )
                    @if ( strcmp($campaign['verification_type'], 2) ===0 ||  strcmp($campaign['verification_type'], 3) ===0 )
                        <td>
                            @if ( $campaign['excel_uploaded'] == 0 )
                                Upload
                            @else
                                Uploaded
                            @endif  
                        </td>
                    @else
                        <td>N/A</td>
                    @endif
                @endif

                <td>
                    @if ( in_array("add unique codes", $spData['permittedtasks']) )
                        @if ( $campaign['verification_type'] == 2 )
                            N/A
                        @else
                            Add   
                        @endif
                        |
                    @endif

                    @if ( in_array("approve unique codes", $spData['permittedtasks']) )
                            View Approval
                        |
                    @endif

                    @if ( strcmp($campaign['verification_type'], 1) ===0 )
                        Unique-code only
                    @elseif ( strcmp($campaign['verification_type'], 2) ===0 )
                        Unique-code & last 6-digit (GCR)
                    @elseif ( strcmp($campaign['verification_type'], 3) ===0 )
                        Unique-code & last 6-digit (Bank)
                    @endif
                </td>

                @if ( in_array("download unique codes", $spData['permittedtasks']) )
                    <td>
                        @if ( $campaign['verification_type'] == 1 || $campaign['verification_type'] == 3 
                            || ($campaign['verification_type'] == 2 && $campaign['excel_uploaded'] == 1 )
                        )
                            Download
                        @elseif  ( $campaign['verification_type'] == 2 )
                            @if ( $campaign['excel_uploaded'] <> 1 )
                                N/A
                            @endif
                        @endif
                    </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
        <hr>
    </div>

    <div id="disabled-div">
        <h4>Disabled Campaigns</h4>
        <table id="disabled-camapigns-table" class="table search_paginate_selectperpage">
            <thead>
            <tr>
                <th>Campaign</th>
                <th>Bank</th>
                <th>Description</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Created on</th>
                @if ( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks']))
                    <th>Action</th>
                @endif 
                @if ( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) )     
                    <th>Products</th>
                @endif
                @if ( in_array("upload unique code masterlist", $spData['permittedtasks']) )
                    <th>Upload Excel</th>
                @endif
                <th>Unique-code type</th>
                @if ( in_array("download unique codes", $spData['permittedtasks']) )
                    <th>Unique-codes</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($disabledCampaigns as $campaign)
            <tr class='disabled_row' >
                <td>
                    {{ $campaign['title'] }}
                </td>
                <td>{{ $campaign['bank_name'] }}</td>
                <td>{{ $campaign['description'] }}</td>
                <td>{{ $campaign['created_on'] }}</td>
                <td>{{ $campaign['start_date'] }}</td>
                <td>{{ $campaign['end_date'] }}</td>

                @if ( in_array("edit campaign", $spData['permittedtasks']) || in_array("delete campaign", $spData['permittedtasks']))
                    <td>
                        @if ( in_array("edit campaign", $spData['permittedtasks']) )
                            Edit
                        @endif
                    </td>
                @endif

                @if ( in_array("add product", $spData['permittedtasks']) || in_array("view all products", $spData['permittedtasks']) )
                    <td>
                        @if ( in_array("add product", $spData['permittedtasks']) )
                            Add Product
                        @endif
                        @if ( in_array("add product", $spData['permittedtasks']) &&  in_array("view all products", $spData['permittedtasks']) &&  $campaign['has_products'] == 1)
                            |
                        @endif
                        @if ( in_array("view all products", $spData['permittedtasks']) )
                            @if ( $campaign['has_products'] == 1 )
                                View
                            @endif
                        @endif
                    </td>
                @endif
                
                @if ( in_array("upload unique code masterlist", $spData['permittedtasks']) )
                    @if ( strcmp($campaign['verification_type'], 2) ===0 ||  strcmp($campaign['verification_type'], 3) ===0 )
                        <td>
                            @if ( $campaign['excel_uploaded'] == 0 )
                                Upload
                            @else
                                Uploaded
                            @endif
                        </td>
                    @else
                        <td>N/A</td>
                    @endif
                @endif

                <td>
                    @if ( in_array("add unique codes", $spData['permittedtasks']) )
                        @if ( $campaign['verification_type'] == 2 )
                            N/A
                        @else
                            Add
                        @endif
                        |
                    @endif

                    @if ( in_array("approve unique codes", $spData['permittedtasks']) )
                            View Approval
                        |
                    @endif

                    @if ( strcmp($campaign['verification_type'], 1) ===0 )
                        Unique-code only
                    @elseif ( strcmp($campaign['verification_type'], 2) ===0 )
                        Unique-code & last 6-digit (GCR)
                    @elseif ( strcmp($campaign['verification_type'], 3) ===0 )
                        Unique-code & last 6-digit (Bank)
                    @endif
                </td>

                @if ( in_array("download unique codes", $spData['permittedtasks']) )
                    <td>
                        @if ( $campaign['verification_type'] == 1 || $campaign['verification_type'] == 3 
                            || ($campaign['verification_type'] == 2 && $campaign['excel_uploaded'] == 1 )
                        )
                            Download
                        @elseif  ( $campaign['verification_type'] == 2 )
                            @if ( $campaign['excel_uploaded'] <> 1 )
                                N/A
                            @endif
                        @endif
                    </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection


@section('scripts')
    <script>
         $(document).ready(function() {
             // filter camapigns by active, expired, disabled
            $('.filter-checkbox').change(function() {
                if($('#chkbox-active').is(":checked")) {
                    $('#active-div').show();
                } else {
                    $('#active-div').hide();
                }   

                if($('#chkbox-expired').is(":checked")) {
                    $('#expired-div').show();
                } else {
                    $('#expired-div').hide();
                }    

                if($('#chkbox-disabled').is(":checked")) {
                    $('#disabled-div').show();
                } else {
                    $('#disabled-div').hide();
                }  
            });
        });
    </script>
@endsection


@extends('admin.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif
                <div class="card-header"><h2>Delete Campaign</h2></div>

                <div class="card-body">
                
                    <form method="post" action="{{ url('/campaign/delete') }}">
                        @csrf
                        <input type="hidden" id="campaign_id" name="campaign_id" value={{ $campaign[0]['campaign_id'] }}>
                        Are you sure you want to delete <b>{{ $campaign[0]['name'] }}</b> campaign? 
                        <br><br>
            
                        <button id="btnDeleteCampaign" type="submit" class="btn btn-primary">Delete Campaign</button>
                        &nbsp
                        <a href="{{ url('/campaign/all') }}" class="btn btn-default" >Cancel</a>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

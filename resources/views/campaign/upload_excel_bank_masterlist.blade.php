@extends('admin.layout')

@section('header')
<h3>Upload Bank Masterlist for Campaign: {{$campaign_name}}</h3>
@endsection

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
        
        <form id="upload_form" method="post" action="{{ url('/campaign/upload_excel_bank_masterlist')}}" enctype="multipart/form-data">
            @csrf           
            <input type="hidden" id="campaign_id" name="campaign_id" value={{ $campaign_id }}>       
            <div id="excel_upload_div" class="input-group">
                <b>Upload the excel file consisting of customers' last 6-digit of credit card and unique code</b><br><br>
                <div class="custom-file">
                    <input type="file" name="excelfile_upload" id="excelfile_upload" required>
                </div>
            </div>
            <br>
            <button id="btnUpload" type="submit" class="btn btn-primary">Upload</button>   
            &nbsp
            <a href="{{ url('/campaign/all') }}" class="btn btn-default" >Cancel</a> 
        </form>
        
    </div>
</div>


@endsection

@section('scripts')
    <script>
        $("#upload_form").validate();
    </script>
@endsection

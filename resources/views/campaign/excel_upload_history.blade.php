@extends('admin/layout')

@section('header')

@endsection

@section('content')
<div class="page_box">
    <h3>Excel Uploads History</h3>

        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Bank</th>
                <th>Campaign</th>
                <th>File Uploaded</th>
                <th>Unique Code Quantity</th>
                <th>Uploaded by</th>
                <th>Uploaded at</th>

            </tr>
            </thead>
            <tbody>            
            @foreach($uploads as $upload)
            <tr>
                <td>
                   {{ $upload['bank_name'] }}
                </td>
                <td>
                   {{ $upload['campaign_title'] }}
                </td>
                <td>
                   <a href="{{ route('campaign.uploaded-excel_download' , [ 'filename' => $upload['filename'] ] ) }}">{{ $upload['filename'] }}</a>
                </td>
                <td>
                   {{ $upload['unique_code_qty'] }}
                </td>
                <td>
                   {{ $upload['username'] }}
                </td>
                <td>
                   {{ $upload['created_on'] }}
                </td>       
            </tr>
            @endforeach
            </tbody>
        </table>  
</div>

@endsection
@extends('layouts.app')

@section('assets')
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- //custom-theme -->
<!-- css files -->
<link href="//fonts.googleapis.com/css?family=Helvetica Neue:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">  
<!-- //css files -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> <!-- Font-Awesome-Icons-CSS -->
 
 <!-- Google Recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<style>

</style>

<link rel="stylesheet" href="{{ asset('css/gift_redeem.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/hsbc/toc_css.css') }}" type="text/css">
@endsection

@section('content')

<!-- body starts -->


<div>
	<img width="100%" height="auto" src="{{ URL::to('/') }}/images/site_img/banner4.jpg">
</div>

<div class="row" style="margin-bottom:30px; margin-top:20px;">
	<div class="wrapper-progressBar col-sm-12 col-md-12">
		<p id="top" style="text-align:center;font-weight:bold; font-size:1.3em;">HOW IT WORKS</p>
		<br>
		<ul class="progressBar" style="font-size:0.8em;"> 
			<li class="active">Step 1:<br/> Select your gift</li>
			<li id="step2" class="active">Step 2:<br/>Fill in your Unique ID & last 6-digits of your credit card number for verification</li>
			<li id="step3" class="active" >Step 3:<br/>Fill in your mailing address</li>
			<li id="step4">Redemption successful</li>
		</ul>
	</div>
</div>

<!-- section -->
<section class="register">
	<div class="row" style="padding-top: 2rem; padding-bottom: 2rem;">
	
		<div class="col-sm-2" ></div>

		<div class="col-xs-12 col-sm-5 col-md-3" >
			<div style="text-align:center;margin-bottom:15px;font-size:20px;padding-top:20px;padding-bottom:20px;">You have selected</div>
			<div style="height:auto; padding:0; border:0px solid grey; box-shadow: 0 10px 18px 0 rgba(0, 0, 0, 0.2), 0 6px 50px 0 rgba(0, 0, 0, 0.19);">
				<div style="height:200px; text-align:center; background-color:#cfcfcf">
					<img src="{{ URL::to('/') }}/{{ $product[0]['filepath'] }}" alt="product" style="width:auto;min-height:190px; max-height:200px;">  
				</div> 
				<div class="text-center" style="padding-top:10px; margin:5px; margin-bottom:15px; text-align:center;height:auto;">
					<b style="color:#707070;font-size:28pts;">{{ $product[0]['product_name'] }}</b>
				</div>
				<hr style="margin: 0 15%;color:#000;border-top: solid 1px #CFCFCF;" >
				<div style="color:#707070; padding-top:13px; margin:5px; text-align:center; font-size:0.8em; margin-bottom:18px;">
				{{ $product[0]['description'] }}
				</div>
			
			</div>
		</div>
		<div class="col-sm-1"></div>

   		<div class="col-xs-12 col-sm-5 col-md-4" style="text-align:center;">
			<div style="padding-left:10px;box-shadow: 0 10px 18px 0 rgba(0, 0, 0, 0.2), 0 6px 50px 0 rgba(0, 0, 0, 0.19);">
				<div class="register-in">
					
					<div class="register-form">
						<form id="user_fill_form" method="post" action="{{ url('/redeem/post')}}" enctype="multipart/form-data">
							@csrf
							<input type="hidden" id="campaign_id" name="campaign_id" value="{{ $campaign_id }}">
							<input type="hidden" id="product_id" name="product_id" value="{{ $product_id }}">
							<div class="fields-grid">
							
								<div style="text-align:left;">
									<span>Last 6-digit credit card</span>
									<input type="text" id="ic_code" name="ic_code" value="{{$ic_code}}" readonly>
									<span style="color:green;" class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									<span></span>
								</div>
								<br>
								<div style="margin-bottom:5px;text-align:left;">
									<span>Unique ID</span>
									<input type="text" id="unique_code" name="unique_code" value="{{$unique_code}}" readonly> 
									<span style="color:green;" class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								</div><br>

								<div class="styled-input agile-styled-input-top">
									<input type="text" id="name" name="name" value="{{ old('name') }}" required>
									<label>Name</label>
									<span></span>
								</div>
								<div class="styled-input">
										<input class="country_code_image_input" type="text" id="country_code" name="country_code" value="+60" required disabled style="width:18%;">
										<input type="hidden" id="mobile_country_code" name="mobile_country_code" value="60">
										<input type="tel" id="mobile" name="mobile" required style="width:71%;" value="{{ old('mobile') }}" maxlength="10" placeholder="Mobile no">									
									<!-- <label>Mobile no</label> -->
									<span></span>
								</div>
								<div class="styled-input">
									<input type="email" id="email" name="email" value="{{ old('email') }}" required>
									<label>Email</label>
									<span></span>
								</div>
								<div class="styled-input">
									<input type="text" id="address1" name="address1" value="{{ old('address1') }}" required>
									<label>Address 1 (Unit No./ House No.)</label>
									<span></span>
								</div>
								<div class="styled-input">
									<input type="text" id="address2" name="address2" value="{{ old('address2') }}" required>
									<label>Address 2 (Street, Town)</label>
									<span></span>
								</div>
								<div class="styled-input">
									<input type="text" id="zipcode" name="zipcode"  value="{{ old('zipcode') }}" maxlength="5" required>
									<label>Zip/Postal code</label>
									<span></span>
								</div>

								<div class="styled-input agile-styled-input-top">
									<input type="text" id="city" name="city" value="{{ old('city') }}" required>
									<label>City</label>
									<span></span>
								</div>

								<div style="text-align:left; padding-left: 15px; margin-top: -9px;">
									<div style="display:inline-block;">State</div>
									<select id="state" name="state">
										@foreach($states as  $state)
											<option value="{{ $state['state_name'] }}" >{{ $state['state_name'] }}</option>
										@endforeach
									</select>
									<span></span>
								</div>

								<div class="styled-input" style="margin-top: 25px;text-align:left; padding-left: 15px;">
									<span>Country</span>
									<input style="width: 96%;" type="text" id="country" name="country" value="Malaysia" readonly>
								</div>

					
								<input type="hidden" name="newsletter" id="newsletter" value="1" checked>
								
								<div class="clear"> </div>
								
							</div>
							<div style="text-align:center;padding-bottom:20px;">
								<a href="{{ route( 'redeem.userform' ,  [ 'campaign_id' => $campaign_id, 'product_id' => $product_id ] ) }}#top" style="width:40%;font-size:25pts;" class="btn btn-danger">Back</a>
								&nbsp
								<button type="button" id="btnSubmit" data-toggle="modal" data-target="#agreeModal" style="width:40%;font-size:25pts;text-align: center;" >Submit</button>
							</div>


							<!-- Modal -->
								<div id="agreeModal" class="modal fade" role="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content" style="text-align:left;">
									<div class="modal-body">
										<p>By submitting this Form, you hereby agree that Tri-e marketing may collect, obtain, store and process your personal data that you provide in this form.</p>
										<p>You hereby give your consent to Tri-e marketing: -</p>
										<p>To fulfil the gift redemption;<p>
										<p>For verification of the details given with respect to the delivery of the redeemed gift;<p>
										<p>To store and process your Personal Data;<p>
										<p>To carry out your instructions or to respond to any enquiries, comments or feedbacks that you have submitted to us;<p>
										<p>Disclose your Personal Data to the relevant governmental authorities or third parties where required by law or for legal purposes;<p>
										<p>For the avoidance of doubt, Personal Data includes all data defined within the Personal Data Protection Act 2010 including all data you had disclosed to Tri-e marketing in this Form.<p>
									</div>
									<div class="modal-footer" style="text-align:center !important;">
										<input style="width:80px;height: 38px;" id="btnSubmit" data-toggle="modal" data-target="#agreeModal" type="submit" value="OK">
									</div>
									</div>

								</div>
								</div>
						</form>
					</div>
				</div>
				<div class="clear"> </div>
			</div>
		</div>
	<div class="clear"> </div>
	</div>

	<div class="text-center" style="padding-top:30px; padding-bottom:30px; font-size:0.7em; text-align:center;"> 
	HSBC Get A Gift Promotion
	<a href="https://sp.hsbc.com.my/projects/superstart_product/files/HSBC%20Get%20A%20Gift%20Campaign%20TnC%20-%20Final.pdf"  target="_blank" style="text-decoration:underline;"> Terms and conditions</a> apply
	</div> 	
	
</section>
<!-- //section -->


@if(session()->has('fail_message'))
	<input type="hidden" id="error_val" name="error_val" value="{{ session()->get('fail_message') }}">
	<input type="hidden" id="error_ticket_no" name="error_ticket_no" value="{{ session()->get('error_ticket_no') }}">
@else
 	<input type="hidden" id="success" name="success" value="no_error">
@endif				


@endsection


@section('scripts')
    <script>
		function setInputFilter(textbox, inputFilter) {
			["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
				textbox.addEventListener(event, function() {
				if (inputFilter(this.value)) {
					this.oldValue = this.value;
					this.oldSelectionStart = this.selectionStart;
					this.oldSelectionEnd = this.selectionEnd;
				} else if (this.hasOwnProperty("oldValue")) {
					this.value = this.oldValue;
					this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
				}
				});
			});
		}

		location.hash = "#top"
		$( document ).ready(function() {
			
			if ($('#error_val').length) {
				var msg = $('#error_val').val();
				var error_ticket_no = $('#error_ticket_no').val(); 
				var err_span = document.createElement("span");
				err_span.innerHTML = msg + "<br />" + "Tel: +603 8076 1313 <br/>" + "Ticket number: "  + error_ticket_no;

				swal({
					dangerMode: true,
					title: "Redemption Failed",
					type: "error",
					content: err_span,
					button: "OK",
				});
			}				
		});

		$.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
		});

		$.validator.messages.alpha = 'Only alphabets and spaces allowed';

        $('#user_fill_form').validate({
            rules: {
                mobile: {
                    maxlength: 11,
                    minlength: 9,
                    digits: true,
                },
                ic_code: {
                    maxlength: 6,
                    minlength: 4,
                    digits: true,
                },
                zipcode: {
                    maxlength: 5,
                    minlength: 5,
                    digits: true,
                },
				name: {
					alpha: true,
				},
				city: {
					alpha: true,
				},
            },
            messages: {
			},
            errorElement : 'div',
            errorLabelContainer: '.errorTxt'
        });


		$( document ).ready(function() {

            if ($('#error_val').length) {
				if (localStorage.getItem("fail_attampts") === null) {
					//localStorage.setItem("fail_attampts", "0")
				}

				var error_count = parseInt(localStorage.getItem("fail_attampts"));
				error_count = error_count + 1;
				localStorage.setItem("fail_attampts", error_count);
						
			} else if ($('#success').length) {
				localStorage.setItem("fail_attampts", "0"); // reset back to 0
			}
			
			// Show Google Recaptcha if failed 3 times in a row
			var fail_count = localStorage.getItem("fail_attampts");
			if (fail_count >= 3) {
				$('#google-recaptcha').show();
				$('#g-captcha-required').val('yes');
			}
			//alert(fail_count);


			$("#name").bind("keypress", function (event) {
				var inputValue = event.which;
				// allow letters and whitespaces, backspace keys only.
				if(!(inputValue >= 65 && inputValue <= 120) 
					&& (inputValue != 32 && inputValue != 0 && inputValue != 8 )) { 
					event.preventDefault(); 
				}
			});

			$("#city").bind("keypress", function (event) {
				var inputValue = event.which;
				// allow letters and whitespaces, backspace keys only.
				if(!(inputValue >= 65 && inputValue <= 120) 
					&& (inputValue != 32 && inputValue != 0 && inputValue != 8 )) { 
					event.preventDefault(); 
				}
			});


			// Restrict input to digits and '.' by using a regular expression filter.
			setInputFilter(document.getElementById("mobile"), function(value) {
				return /^\d*\.?\d*$/.test(value);
			});

			// Restrict input to digits and '.' by using a regular expression filter.
			setInputFilter(document.getElementById("zipcode"), function(value) {
				return /^\d*\.?\d*$/.test(value);
			});
		});
				
    </script>
@endsection
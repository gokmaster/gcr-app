@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> <!-- Font-Awesome-Icons-CSS -->

<link rel="stylesheet" href="{{ asset('css/gift_redeem.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/hsbc/toc_css.css') }}" type="text/css">

<style>

.register-full {
    width: 100%;
	margin: 2em auto 0;
	padding-left: 150px;
}

.register-right {
    background: #FFFFFF;
    background: rgba(0, 0, 0, 0);
    -webkit-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    -moz-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    box-shadow: 2px 2px 2px 1px rgba(253, 249, 249, 0.75);
}
.register-in {
    padding: 1em;
}
.register-left{
	width: 40%;
	height:350px;
	float: left;
    border: 1px solid #d3d3d3;
    border-radius: 10px;
	margin-right:5px;
	background: rgba(0, 0, 0, 0);
    -webkit-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    -moz-box-shadow: 0px 0px 2px 1px rgba(253, 249, 249, 0.75);
    box-shadow: 1px 1px 9px 2px rgba(114, 111, 111, 0.75)
}

.register-right {
    width: 40%;
    float: left;
}
.register-left p {
    margin: 2em 0;
    line-height: 28px;
    font-size: 15px;
    font-weight: 100;
	padding:0 4em;
    letter-spacing: 1px;
    color: #000000;
    text-align: center;
 
}
.register-left h1 {
    font-size: 3em;
    text-transform: uppercase;
    margin-top: .5em;
    margin-bottom: .5em;
    color: #000000;
    text-align: center;
}
.register-right h2 {
    text-transform: uppercase;
    font-size: 2em;
    font-weight: 300;
    letter-spacing: 1px;
    word-spacing: 5px;
	color: #000;
	font-size:1.0em;
}
/*-- //input-effect --*/
.logo{
	text-align: center;
	margin-top: 2em;
	
}
.logo span.fa {
    color: #ffffff;
    font-size: 6em;
}
/*-- //copyright --*/

/* responsive design */
@media (max-width: 320px) {
	.register-full {
		padding-left: 0px;
	}
}	

.register-left{
	width:100%;
}

.register-right {
    width: 100%;
}

/* //responsive design */

</style>

@endsection

@section('content')

<div class="row" style="margin-bottom:30px; margin-top:20px;">
	<div class="wrapper-progressBar col-sm-12 col-md-12">
		<p style="text-align:center;font-weight:bold; font-size:1.3em;">HOW IT WORKS</p>
		<br>
		<ul class="progressBar" style="font-size:0.8em;"> 
			<li class="active" icon="pencil">Step 1:<br/> Select your gift</li>
			<li class="active" id="step2" class="active">Step 2:<br/>Fill in your Unique ID & last 6-digits of your credit card number for verification</li>
			<li class="active" id="step3">Step 3:<br/>Fill in your mailing address</li>
			<li class="active" id="step4">Redemption successful</li>
		</ul>
	</div>
</div>

<section class="successful">
<div style="text-align:center; margin-top: 1px; margin-bottom: 50px;">
	<h5  style="font-weight: bold;font-size: 1.5em;"> 
		<img src="{{ URL::to('/') }}/images/icons/tick.png" width="30px" height="30px" alt="success-icon"> 
		REDEMPTION SUCCESSFUL 
	</h5><br>
	<h3  style="font-size: 1.5em; color: #787878;"> Your Gift Redemption is Successful! </h3><br>
	<h5  style="font-weight: bold;font-size: 1.0em;">YOUR ORDER NUMBER IS: {{ $order_no }}</h5><br>
</div>
</section>
 
<section class="register">
	<div class="register-full">
		<div class="register-left">
			<div class="register">
				<div class="logo">                 
                    <div class="box row" style="height: auto;">
                        <div class="col-sm-5" style="text-align:left;padding-left:35px;">
                            <h3 style="font-size:1.0em; font-weight: bold;"><b> NAME: </b></h3>
                                <h5>{{ $user_data['name'] }}</h5> <br> 
                            <h3 style="font-size:1.0em; font-weight: bold"><b> MOBILE NO:</b></h3>
                                <h5>{{ $user_data['mobile'] }}</h5> <br> 
                            <h3 style="font-size:1.0em; font-weight: bold"><b> ADDRESS:</b></h3>
                                <h5>  {{ $user_data['address1'] }}<br>
                                    {{ $user_data['address2'] }}<br> 
                                    {{ $user_data['city'] }}<br>   
                                    {{ $user_data['state'] }}</h5><br>
                        </div>
                        <div class="col-sm-6" style="text-align:center;">
                            <img src="{{ URL::to('/') }}/{{ $product[0]['filepath'] }}" alt="product" class="img-thumbnail img-check" width="100%">
							<br>{{$product[0]['product_name']}}
                        </div>
                    </div> 
				</div>
			</div>
        </div>

        <div class="register-right">
			<div class="register-in">
                <div class="box" style="height: 200px; text-align: left; font-size:1.0em; font-weight: bold; margin-top: 1em;">
                        <h3><b>Thank you for your submission.</b></h3><br><br>
                        <h3><b>Your gifts will be processed for delivery to your <br>shipping address.</b></h3><br><br>
                        <h3><b>Please do allow 2-4 weeks time for delivery.</b></h3><br><br>
                        <h3><b>For more enquiries, please contact Tri-e Marketing<br>at 03-8076 1313 or info@tri-e.com.my</b></h3><br><br>
                        <h3><b>For more details please refer to HSBC terms and<br>conditions at <a href="http://www.hsbc.com.my/gf">www.hsbc.com.my/gf</a></b></h3></span>
                </div>   
			</div>
		</div>
		<div style="clear:both;"> </div>
    </div> 
   		                                 
</section>


 	
<div style="text-align:center;padding-top:50px;">						
	<!-- <a style="margin-right:50px;" href="{{ route( 'campaign.view' ,  ['campaign_name' => $campaign_name] ) }}" style="padding-bottom:width:50%;font-size:25pts;" class="btn btn-danger">Redeem another product</a> -->
	<a href="https://sp.hsbc.com.my/gifts" style="padding-bottom:width:50%;font-size:25pts;" class="btn btn-danger">HSBC Home</a>
	
</div>
	


<div style="padding-top:50px; padding-bottom:30px; font-size:0.7em;text-align:center;"> 
	HSBC Get A Gift Promotion
	<a href="https://sp.hsbc.com.my/projects/superstart_product/files/HSBC%20Get%20A%20Gift%20Campaign%20TnC%20-%20Final.pdf"  target="_blank" style="text-decoration:underline;">Terms and conditions</a> apply
	<br>
	<a href="{{ url('/contact/message_form')}}" style="text-decoration:underline;">Contact Us</a>
</div>
<div class="text-center" style="padding-top:40px; font-size:0.7em; text-align:center;"> 
	
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
	  	<button type="button" class="btn btn-super-danger btn-lg"  data-dismiss="modal"><i class="fa fa-times"></i> </button>
        <div class="modal-body">

		<div class="toc_box">
        <DIV id="page_1">
<DIV id="p1dimg1">
<IMG src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAMeAl4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD5/ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA9//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigD5//wCGZf8Aqbv/ACm//baP+GZf+pu/8pv/ANtr6AooA+f/APhmX/qbv/Kb/wDbaP8AhmX/AKm7/wApv/22voCigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooArf2ha/wDPX/x0/wCFH9oWv/PX/wAdP+FYVFAG7/aFr/z1/wDHT/hR/aFr/wA9f/HT/hWFRQBu/wBoWv8Az1/8dP8AhR/aFr/z1/8AHT/hWFRQBu/2ha/89f8Ax0/4Uf2ha/8APX/x0/4VhUUAbv8AaFr/AM9f/HT/AIUf2ha/89f/AB0/4VhUUAbv9oWv/PX/AMdP+FH9oWv/AD1/8dP+FYVFAG7/AGha/wDPX/x0/wCFH9oWv/PX/wAdP+FYVFAG7/aFr/z1/wDHT/hR/aFr/wA9f/HT/hWFRQBu/wBoWv8Az1/8dP8AhR/aFr/z1/8AHT/hWFRQBu/2ha/89f8Ax0/4Uf2ha/8APX/x0/4VhUUAbv8AaFr/AM9f/HT/AIUf2ha/89f/AB0/4VhUUAbv9oWv/PX/AMdP+FH9oWv/AD1/8dP+FYVFAG7/AGha/wDPX/x0/wCFH9oWv/PX/wAdP+FYVFAG7/aFr/z1/wDHT/hR/aFr/wA9f/HT/hWFRQBu/wBoWv8Az1/8dP8AhR/aFr/z1/8AHT/hWFRQBu/2ha/89f8Ax0/4Uf2ha/8APX/x0/4VhUUAbv8AaFr/AM9f/HT/AIUf2ha/89f/AB0/4VhUUAbv9oWv/PX/AMdP+FH9oWv/AD1/8dP+FYVFAG7/AGha/wDPX/x0/wCFH9oWv/PX/wAdP+FYVFAG7/aFr/z1/wDHT/hR/aFr/wA9f/HT/hWFRQBu/wBoWv8Az1/8dP8AhR/aFr/z1/8AHT/hWFRQBu/2ha/89f8Ax0/4Uf2ha/8APX/x0/4VhUUAbv8AaFr/AM9f/HT/AIUf2ha/89f/AB0/4VhUUAbv9oWv/PX/AMdP+FH9oWv/AD1/8dP+FYVFAG7/AGha/wDPX/x0/wCFH9oWv/PX/wAdP+FYVFAG7/aFr/z1/wDHT/hR/aFr/wA9f/HT/hWFRQBu/wBoWv8Az1/8dP8AhR/aFr/z1/8AHT/hWFRQBu/2ha/89f8Ax0/4Uf2ha/8APX/x0/4VhUUAbv8AaFr/AM9f/HT/AIUf2ha/89f/AB0/4VhUUAbv9oWv/PX/AMdP+FH9oWv/AD1/8dP+FYVFAG7/AGha/wDPX/x0/wCFH9oWv/PX/wAdP+FYVFAG7/aFr/z1/wDHT/hR/aFr/wA9f/HT/hWFRQBu/wBoWv8Az1/8dP8AhR/aFr/z1/8AHT/hWFRQBu/2ha/89f8Ax0/4Uf2ha/8APX/x0/4VhUUAbv8AaFr/AM9f/HT/AIUf2ha/89f/AB0/4VhUUAbv9oWv/PX/AMdP+FH9oWv/AD1/8dP+FYVFAG7/AGha/wDPX/x0/wCFH9oWv/PX/wAdP+FYVFAG7/aFr/z1/wDHT/hR/aFr/wA9f/HT/hWFRQBa/s+6/wCeX/jw/wAaP7Puv+eX/jw/xrcooAw/7Puv+eX/AI8P8aP7Puv+eX/jw/xrcooAw/7Puv8Anl/48P8AGj+z7r/nl/48P8a3KKAMP+z7r/nl/wCPD/Gj+z7r/nl/48P8a3KKAMP+z7r/AJ5f+PD/ABo/s+6/55f+PD/GtyigDD/s+6/55f8Ajw/xo/s+6/55f+PD/GtyigDD/s+6/wCeX/jw/wAaP7Puv+eX/jw/xrcooAw/7Puv+eX/AI8P8aP7Puv+eX/jw/xrcooAw/7Puv8Anl/48P8AGj+z7r/nl/48P8a3KKAMP+z7r/nl/wCPD/Gj+z7r/nl/48P8a3KKAMP+z7r/AJ5f+PD/ABo/s+6/55f+PD/GtyigDD/s+6/55f8Ajw/xo/s+6/55f+PD/GtyigDD/s+6/wCeX/jw/wAaP7Puv+eX/jw/xrcooAw/7Puv+eX/AI8P8aP7Puv+eX/jw/xrcooAw/7Puv8Anl/48P8AGj+z7r/nl/48P8a3KKAMP+z7r/nl/wCPD/Gj+z7r/nl/48P8a3KKAMP+z7r/AJ5f+PD/ABo/s+6/55f+PD/GtyigDD/s+6/55f8Ajw/xo/s+6/55f+PD/GtyigDD/s+6/wCeX/jw/wAaP7Puv+eX/jw/xrcooAw/7Puv+eX/AI8P8aP7Puv+eX/jw/xrcooAw/7Puv8Anl/48P8AGj+z7r/nl/48P8a3KKAMP+z7r/nl/wCPD/Gj+z7r/nl/48P8a3KKAMP+z7r/AJ5f+PD/ABo/s+6/55f+PD/GtyigDD/s+6/55f8Ajw/xo/s+6/55f+PD/GtyigDD/s+6/wCeX/jw/wAaP7Puv+eX/jw/xrcooAw/7Puv+eX/AI8P8aP7Puv+eX/jw/xrcooAw/7Puv8Anl/48P8AGj+z7r/nl/48P8a3KKAMP+z7r/nl/wCPD/Gj+z7r/nl/48P8a3KKAMP+z7r/AJ5f+PD/ABo/s+6/55f+PD/GtyigDD/s+6/55f8Ajw/xo/s+6/55f+PD/GtyigDD/s+6/wCeX/jw/wAaP7Puv+eX/jw/xrcooAw/7Puv+eX/AI8P8aP7Puv+eX/jw/xrcooAw/7Puv8Anl/48P8AGj+z7r/nl/48P8a3KKAMP+z7r/nl/wCPD/Gj+z7r/nl/48P8a3KKAMP+z7r/AJ5f+PD/ABo/s+6/55f+PD/GtyigDD/s+6/55f8Ajw/xo/s+6/55f+PD/GtyigDD/s+6/wCeX/jw/wAaP7Puv+eX/jw/xrcooAw/7Puv+eX/AI8P8aP7Puv+eX/jw/xrcooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgDoaK577TP/z2k/76NH2mf/ntJ/30aAOhornvtM//AD2k/wC+jR9pn/57Sf8AfRoA6Giue+0z/wDPaT/vo0faZ/8AntJ/30aAOhornvtM/wDz2k/76NH2mf8A57Sf99GgCKiuh+zQf88Y/wDvkUfZoP8AnjH/AN8igDnqK6H7NB/zxj/75FH2aD/njH/3yKAOeorofs0H/PGP/vkUfZoP+eMf/fIoA56iuh+zQf8APGP/AL5FH2aD/njH/wB8igDnqK6H7NB/zxj/AO+RR9mg/wCeMf8A3yKAOeorofs0H/PGP/vkUfZoP+eMf/fIoA56iuh+zQf88Y/++RR9mg/54x/98igDnqK6H7NB/wA8Y/8AvkUfZoP+eMf/AHyKAOeorofs0H/PGP8A75FH2aD/AJ4x/wDfIoA56iuh+zQf88Y/++RR9mg/54x/98igDnqK6H7NB/zxj/75FH2aD/njH/3yKAOeorofs0H/ADxj/wC+RR9mg/54x/8AfIoA56iuh+zQf88Y/wDvkUfZoP8AnjH/AN8igDnqK6H7NB/zxj/75FH2aD/njH/3yKAOeorofs0H/PGP/vkUfZoP+eMf/fIoA56iuh+zQf8APGP/AL5FH2aD/njH/wB8igDnqK6H7NB/zxj/AO+RR9mg/wCeMf8A3yKAOeorofs0H/PGP/vkUfZoP+eMf/fIoA56iuh+zQf88Y/++RR9mg/54x/98igDnqK6H7NB/wA8Y/8AvkUfZoP+eMf/AHyKAOeorofs0H/PGP8A75FH2aD/AJ4x/wDfIoA56iuh+zQf88Y/++RR9mg/54x/98igDnqK6H7NB/zxj/75FH2aD/njH/3yKAOeorofs0H/ADxj/wC+RR9mg/54x/8AfIoA56iuh+zQf88Y/wDvkUfZoP8AnjH/AN8igDnqK6H7NB/zxj/75FH2aD/njH/3yKAOeorofs0H/PGP/vkUfZoP+eMf/fIoA56iuh+zQf8APGP/AL5FH2aD/njH/wB8igDnqK6H7NB/zxj/AO+RR9mg/wCeMf8A3yKAOeorofs0H/PGP/vkUfZoP+eMf/fIoA56iuh+zQf88Y/++RR9mg/54x/98igDnqK6H7NB/wA8Y/8AvkUfZoP+eMf/AHyKAOeorofs0H/PGP8A75FH2aD/AJ4x/wDfIoA56iuh+zQf88Y/++RR9mg/54x/98igDnqK6H7NB/zxj/75FH2aD/njH/3yKAOeorofs0H/ADxj/wC+RR9mg/54x/8AfIoA56iuh+zQf88Y/wDvkUfZoP8AnjH/AN8igDnqK6H7NB/zxj/75FH2aD/njH/3yKAJaKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAorP8A7b07/n4/8cb/AAo/tvTv+fj/AMcb/CgDQorP/tvTv+fj/wAcb/Cj+29O/wCfj/xxv8KANCis/wDtvTv+fj/xxv8ACj+29O/5+P8Axxv8KANCis/+29O/5+P/ABxv8KP7b07/AJ+P/HG/woA0KKz/AO29O/5+P/HG/wAKP7b07/n4/wDHG/woA0KKz/7b07/n4/8AHG/wo/tvTv8An4/8cb/CgDQorP8A7b07/n4/8cb/AAo/tvTv+fj/AMcb/CgDQorP/tvTv+fj/wAcb/Cj+29O/wCfj/xxv8KANCis/wDtvTv+fj/xxv8ACj+29O/5+P8Axxv8KANCis/+29O/5+P/ABxv8KP7b07/AJ+P/HG/woA0KKz/AO29O/5+P/HG/wAKP7b07/n4/wDHG/woA0KKz/7b07/n4/8AHG/wo/tvTv8An4/8cb/CgDQorP8A7b07/n4/8cb/AAo/tvTv+fj/AMcb/CgDQorP/tvTv+fj/wAcb/Cj+29O/wCfj/xxv8KANCis/wDtvTv+fj/xxv8ACj+29O/5+P8Axxv8KANCis/+29O/5+P/ABxv8KP7b07/AJ+P/HG/woA0KKz/AO29O/5+P/HG/wAKP7b07/n4/wDHG/woA0KKz/7b07/n4/8AHG/wo/tvTv8An4/8cb/CgDQorP8A7b07/n4/8cb/AAo/tvTv+fj/AMcb/CgDQorP/tvTv+fj/wAcb/Cj+29O/wCfj/xxv8KANCis/wDtvTv+fj/xxv8ACj+29O/5+P8Axxv8KANCis/+29O/5+P/ABxv8KP7b07/AJ+P/HG/woA0KKz/AO29O/5+P/HG/wAKP7b07/n4/wDHG/woA0KKz/7b07/n4/8AHG/wo/tvTv8An4/8cb/CgDQorP8A7b07/n4/8cb/AAo/tvTv+fj/AMcb/CgDQorP/tvTv+fj/wAcb/Cj+29O/wCfj/xxv8KANCis/wDtvTv+fj/xxv8ACj+29O/5+P8Axxv8KANCis/+29O/5+P/ABxv8KP7b07/AJ+P/HG/woA0KKz/AO29O/5+P/HG/wAKP7b07/n4/wDHG/woA0KKz/7b07/n4/8AHG/wo/tvTv8An4/8cb/CgDQorP8A7b07/n4/8cb/AAo/tvTv+fj/AMcb/CgDQorP/tvTv+fj/wAcb/Cj+29O/wCfj/xxv8KANCis/wDtvTv+fj/xxv8ACj+29O/5+P8Axxv8KANCis/+29O/5+P/ABxv8KP7b07/AJ+P/HG/woA0KKz/AO29O/5+P/HG/wAKP7b07/n4/wDHG/woA0KKz/7b07/n4/8AHG/wo/tvTv8An4/8cb/CgDQorP8A7b07/n4/8cb/AAo/tvTv+fj/AMcb/CgDQorP/tvTv+fj/wAcb/Cj+29O/wCfj/xxv8KAOQooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//Z" id="p1img1"></DIV>


<DIV class="dclr"></DIV>
<P class="p0 ft0">TERMS & CONDITIONS</P>
<P class="p1 ft1">HSBC Get A Gift Campaign (“Promotion”)</P>
<P class="p2 ft5"><SPAN class="ft2">1.</SPAN><SPAN class="ft3">HSBC Bank Malaysia Berhad (Company No. </SPAN>127776-V) is referred to as <SPAN class="ft4">“HSBC Bank” </SPAN>and HSBC Amanah Malaysia Berhad (Company No. 807705-X) is referred to as <SPAN class="ft4">“HSBC Amanah”</SPAN>, both collectively referred to as <SPAN class="ft4">“HSBC”</SPAN>. HSBC Bank Credit Card and HSBC Amanah Credit Card-i will be collectively referred to as <SPAN class="ft4">“HSBC</SPAN></P>
<P class="p3 ft4">Credit Card(s)/-i”.</P>
<P class="p4 ft4">PROMOTION PERIOD</P>
<P class="p5 ft5"><SPAN class="ft2">2.</SPAN><SPAN class="ft3">The following periods are applicable for this Promotion:</SPAN></P>
<P class="p6 ft5"><SPAN class="ft6">a.</SPAN><SPAN class="ft7">Sign-Up</SPAN><SPAN class="ft8"> Period</SPAN> shall run from <SPAN class="ft4">23 November 2018 – 28 February 2019</SPAN>, both dates inclusive (“<SPAN class="ft4">Sign-Up</SPAN><SPAN class="ft4"> Period</SPAN>”); and</P>
<P class="p7 ft5"><SPAN class="ft6">b.</SPAN><SPAN class="ft7">Welcome Period</SPAN> shall be <SPAN class="ft9">60 days from the date of your welcome letter for your Participating HSBC Credit </SPAN><SPAN class="ft9">Card(s)/-i</SPAN> (“<SPAN class="ft4">Welcome Period</SPAN>”);</P>
<P class="p8 ft5">(collectively, the “<SPAN class="ft4">Promotion Period</SPAN>”).</P>
<P class="p5 ft4">ELIGIBILITY</P>
<P class="p9 ft5"><SPAN class="ft2">3.</SPAN><SPAN class="ft3">Subject to Clause 4 and 5 below, this Promotion is open to any individual^ who during the </SPAN><SPAN class="ft4">Sign-Up</SPAN><SPAN class="ft4"> Period</SPAN>, <SPAN class="ft9">applies for any one of the following </SPAN><SPAN class="ft10">primary </SPAN><SPAN class="ft9">HSBC Credit </SPAN><SPAN class="ft9">Card(s)/-i</SPAN>:</P>
<P class="p10 ft5"><SPAN class="ft5">i.</SPAN><SPAN class="ft11">HSBC Bank Credit Card(s):</SPAN> HSBC Premier World MasterCard Credit Card, HSBC Premier Travel Credit Card, HSBC Visa Signature, HSBC Advance Visa Platinum Credit Card, HSBC Visa Platinum; and</P>
<P class="p11 ft5"><SPAN class="ft6">ii.</SPAN><SPAN class="ft11">HSBC Amanah Credit </SPAN><SPAN class="ft10">Card-i(s):</SPAN> HSBC Amanah Premier World MasterCard Credit Card-i, HSBC Amanah MPower Platinum Credit Card-i, HSBC Amanah MPower Credit Card-i;</P>
<P class="p12 ft2">(collectively referred to as the “<SPAN class="ft12">Participating HSBC Credit </SPAN><SPAN class="ft12">Card(s)/-i</SPAN>”).</P>
<P class="p13 ft5">^Refers to individual who does not have existing Primary HSBC Credit Card/-i(s) during the Sign-Up Period.</P>
<P class="p5 ft5"><SPAN class="ft2">4.</SPAN><SPAN class="ft3">The following categories of persons are </SPAN><SPAN class="ft9">not eligible</SPAN> to participate in this Promotion:</P>
<P class="p14 ft5"><SPAN class="ft5">i.</SPAN><SPAN class="ft13">Existing HSBC Primary Cardholders;</SPAN></P>
<P class="p15 ft5"><SPAN class="ft6">ii.</SPAN><SPAN class="ft13">Cardholder(s) who have cancelled his/her HSBC Credit </SPAN>Card(s)/-i within last six (6) months prior to the date of application for any HSBC Credit Card(s)/-i under this Promotion;</P>
<P class="p16 ft5"><SPAN class="ft6">iii.</SPAN><SPAN class="ft13">Cardholder(s) of invalid or cancelled HSBC Credit </SPAN>Card(s)/-i and/or whose accounts are delinquent within HSBC’s definition at any time during the Promotion Period;</P>
<P class="p17 ft5"><SPAN class="ft5">iv.</SPAN><SPAN class="ft13">Cardholder(s) of company and/or corporate HSBC Credit </SPAN>Card(s)/-i; and</P>
<P class="p18 ft5"><SPAN class="ft5">v.</SPAN><SPAN class="ft13">Cardholder(s) who are participating in any other concurrent HSBC Credit </SPAN>Card(s)/-i sign-up promotions via any channels either by HSBC or authorized third parties, <SPAN class="ft9">EXCEPT</SPAN> the <SPAN class="ft4">RM25 Cash Back Acquisition Promotion </SPAN>and <SPAN class="ft4">Greater Together Promotion</SPAN>.</P>
<P class="p19 ft5">(collectively referred to as the “<SPAN class="ft4">Eligible Cardholders</SPAN>”).</P>
<P class="p20 ft5">“<SPAN class="ft4">Existing HSBC Primary Cardholder</SPAN>” means a customer who currently holds any existing Primary HSBC Credit Card(s)/-i during the Sign-Up Period.</P>
<P class="p21 ft4">PARTICIPATION CRITERIA</P>
<P class="p5 ft5"><SPAN class="ft2">5.</SPAN><SPAN class="ft3">Eligible Cardholders must, during the Promotion Period:</SPAN></P>
<P class="p22 ft5"><SPAN class="ft5">a.</SPAN><SPAN class="ft14">Apply for any Participating HSBC Credit </SPAN>Card/-i via any HSBC sales channel;</P>
<P class="p23 ft5"><SPAN class="ft5">b.</SPAN><SPAN class="ft14">Activate his/her newly approved Participating HSBC Credit </SPAN>Card/-i and create PIN (Personal Identification Number) according to the activation steps in the welcome letter; and</P>
<P class="p24 ft5"><SPAN class="ft5">c.</SPAN><SPAN class="ft3">Use his/her Participating HSBC Credit </SPAN>Card/-i on Eligible Spend (as per Clause 6 below) within the Welcome Period in accordance with the Eligibility Criteria as set out in the Table below;</P>
<P class="p25 ft4"><SPAN class="ft5">(the “</SPAN>Participation Criteria<SPAN class="ft5">”).</SPAN></P>
</DIV>
<DIV id="page_2">
<DIV id="p2dimg1">
<IMG src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCANJAl4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA7+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA5j/hI7z/nnB/3yf8AGj/hI7z/AJ5wf98n/GseigDY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xrHooA2P+EjvP+ecH/fJ/wAaP+EjvP8AnnB/3yf8ax6KANj/AISO8/55wf8AfJ/xo/4SO8/55wf98n/GseigDY/4SO8/55wf98n/ABo/4SO8/wCecH/fJ/xrHooA2P8AhI7z/nnB/wB8n/Gj/hI7z/nnB/3yf8ax6KANj/hI7z/nnB/3yf8AGj/hI7z/AJ5wf98n/GseigDY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xrHooA2P+EjvP+ecH/fJ/wAaP+EjvP8AnnB/3yf8ax6KANj/AISO8/55wf8AfJ/xo/4SO8/55wf98n/GseigDY/4SO8/55wf98n/ABo/4SO8/wCecH/fJ/xrHooA2P8AhI7z/nnB/wB8n/Gj/hI7z/nnB/3yf8ax6KANj/hI7z/nnB/3yf8AGj/hI7z/AJ5wf98n/GseigDY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xrHooA2P+EjvP+ecH/fJ/wAaP+EjvP8AnnB/3yf8ax6KANj/AISO8/55wf8AfJ/xo/4SO8/55wf98n/GseigDY/4SO8/55wf98n/ABo/4SO8/wCecH/fJ/xrHooA2P8AhI7z/nnB/wB8n/Gj/hI7z/nnB/3yf8ax6KANj/hI7z/nnB/3yf8AGj/hI7z/AJ5wf98n/GseigDY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xrHooA2P+EjvP+ecH/fJ/wAaP+EjvP8AnnB/3yf8ax6KANj/AISO8/55wf8AfJ/xo/4SO8/55wf98n/GseigDY/4SO8/55wf98n/ABo/4SO8/wCecH/fJ/xrHooA2P8AhI7z/nnB/wB8n/Gj/hI7z/nnB/3yf8ax6KANj/hI7z/nnB/3yf8AGj/hI7z/AJ5wf98n/GseigDY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xrHooA2P+EjvP+ecH/fJ/wAaP+EjvP8AnnB/3yf8ax6KANj/AISO8/55wf8AfJ/xo/4SO8/55wf98n/GseigDY/4SO8/55wf98n/ABo/4SO8/wCecH/fJ/xrHooA2P8AhI7z/nnB/wB8n/Gj/hI7z/nnB/3yf8ax6KANj/hI7z/nnB/3yf8AGj/hI7z/AJ5wf98n/GseigDY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xrHooA2P+EjvP+ecH/fJ/wAaP+EjvP8AnnB/3yf8ax6KANj/AISO8/55wf8AfJ/xo/4SO8/55wf98n/GseigDY/4SO8/55wf98n/ABo/4SO8/wCecH/fJ/xrHooA2P8AhI7z/nnB/wB8n/Gj/hI7z/nnB/3yf8ax6KANj/hI7z/nnB/3yf8AGj/hI7z/AJ5wf98n/GseigDY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xrHooA6f/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKAMf/hHLP/npP/30P8KP+Ecs/wDnpP8A99D/AArYooAx/wDhHLP/AJ6T/wDfQ/wo/wCEcs/+ek//AH0P8K2KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAx77xZ4b0y8ks7/xBpVpdR43wz3scbrkAjKk5GQQfxqv/wAJ34P/AOhr0P8A8GMP/wAVR4e/5Dniz/sKx/8ApFa10FAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFUf8J34P/wChr0P/AMGMP/xVdBRQBz//AAnfg/8A6GvQ/wDwYw//ABVH/Cd+D/8Aoa9D/wDBjD/8VXQUUAc//wAJ34P/AOhr0P8A8GMP/wAVR/wnfg//AKGvQ/8AwYw//FV0FFAHP/8ACd+D/wDoa9D/APBjD/8AFVoaZruj635v9k6rY3/k48z7JcJLsznGdpOM4PX0NaFc/Z/8lD1n/sFWH/o27oAPD3/Ic8Wf9hWP/wBIrWugrn/D3/Ic8Wf9hWP/ANIrWugoAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK5+z/5KHrP/AGCrD/0bd10Fc/Z/8lD1n/sFWH/o27oAPD3/ACHPFn/YVj/9IrWugr5A+Nv/ACV7Xf8At3/9J468/oA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCivgCigD7/AKK+AKKAPv8Aor4AooA+/wCufs/+Sh6z/wBgqw/9G3dfEFe//sy/8zT/ANun/tagD//Z" id="p2img1"></DIV>


<DIV class="dclr"></DIV>
<P class="p26 ft4">ELIGIBLE SPEND</P>
<P class="p9 ft5"><SPAN class="ft2">6.</SPAN><SPAN class="ft3">Eligible Spend for the Promotion are those that are charged to the Participating HSBC Credit </SPAN>Card(s)/-i subject to Clause 7 below; and</P>
<P class="p27 ft5"><SPAN class="ft5">a.</SPAN><SPAN class="ft15">includes:</SPAN> local and overseas retail transactions (including online transactions), standing instructions / auto-billing; and</P>
<P class="p24 ft5"><SPAN class="ft5">b.</SPAN><SPAN class="ft15">excludes:</SPAN> Cash Advance, Balance Transfer (BT), Cash Instalment Plan (CIP), interest charges, finance charges/management fees, credit card annual fees and the Credit Card Service Tax;</P>
<P class="p25 ft4"><SPAN class="ft5">(the “</SPAN>Eligible Spend<SPAN class="ft5">”).</SPAN></P>
<P class="p4 ft4">GIFT & ELIBILITY CRITERIA</P>
<P class="p21 ft17">WELCOME GIFT (<SPAN class="ft16">New Primary Cardholders)</SPAN></P>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p28 ft18">&nbsp;</P></TD>
	<TD rowspan=2 class="tr1 td1"><P class="p29 ft19">Type</P></TD>
	<TD class="tr0 td2"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr0 td3"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr0 td4"><P class="p30 ft19">Eligible</P></TD>
	<TD class="tr0 td5"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr0 td3"><P class="p28 ft18">&nbsp;</P></TD>
	<TD rowspan=2 class="tr1 td6"><P class="p31 ft19">Gift</P></TD>
	<TD class="tr0 td2"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr0 td3"><P class="p28 ft18">&nbsp;</P></TD>
	<TD rowspan=2 class="tr1 td7"><P class="p32 ft19">Eligibility Criteria</P></TD>
	<TD class="tr0 td2"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr0 td8"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr0 td9"><P class="p33 ft20">Maximum</P></TD>
	<TD class="tr0 td2"><P class="p28 ft18">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td10"><P class="p28 ft21">&nbsp;</P></TD>
	<TD class="tr2 td11"><P class="p28 ft21">&nbsp;</P></TD>
	<TD class="tr2 td12"><P class="p28 ft21">&nbsp;</P></TD>
	<TD rowspan=2 class="tr3 td13"><P class="p34 ft20">Cardholder</P></TD>
	<TD class="tr2 td14"><P class="p28 ft21">&nbsp;</P></TD>
	<TD class="tr2 td12"><P class="p28 ft21">&nbsp;</P></TD>
	<TD class="tr2 td11"><P class="p28 ft21">&nbsp;</P></TD>
	<TD class="tr2 td12"><P class="p28 ft21">&nbsp;</P></TD>
	<TD class="tr2 td11"><P class="p28 ft21">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p28 ft21">&nbsp;</P></TD>
	<TD rowspan=2 class="tr3 td16"><P class="p35 ft19">Units</P></TD>
	<TD class="tr2 td11"><P class="p28 ft21">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr4 td17"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td18"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td19"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td20"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td21"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td20"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td22"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td19"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td20"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td23"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td19"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td24"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td19"><P class="p28 ft22">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr5 td25"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td26"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td27"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td29"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td30"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 class="tr5 td31"><P class="p36 ft4">1x unit of</P></TD>
	<TD class="tr5 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td32"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr5 td33"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr6 td34"><P class="p37 ft23">3,000</P></TD>
	<TD class="tr5 td33"><P class="p28 ft18">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr7 td25"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td26"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td27"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td28"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td29"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td30"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td28"><P class="p28 ft24">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td31"><P class="p38 ft4">Samsonite Astra 55cm</P></TD>
	<TD class="tr7 td28"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td32"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td33"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td33"><P class="p28 ft24">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr8 td25"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td26"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td27"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td28"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td29"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td30"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td28"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td28"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td32"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td33"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td35"><P class="p28 ft25">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr3 td36"><P class="p38 ft4">Units</P></TD>
</TR>
<TR>
	<TD class="tr9 td25"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td26"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td27"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td28"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td29"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td30"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td28"><P class="p28 ft26">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td31"><P class="p39 ft4">Luggage; OR</P></TD>
	<TD class="tr9 td28"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td32"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td33"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td35"><P class="p28 ft26">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr10 td25"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td26"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td27"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td28"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td29"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td30"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td28"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td28"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td32"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td33"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td35"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td37"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td33"><P class="p28 ft27">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr11 td38"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td39"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td40"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr12 td28"><P class="p28 ft29">&nbsp;</P></TD>
	<TD class="tr12 td29"><P class="p28 ft29">&nbsp;</P></TD>
	<TD class="tr12 td30"><P class="p28 ft29">&nbsp;</P></TD>
	<TD class="tr11 td41"><P class="p28 ft28">&nbsp;</P></TD>
	<TD colspan=2 class="tr11 td42"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr12 td28"><P class="p28 ft29">&nbsp;</P></TD>
	<TD class="tr12 td32"><P class="p28 ft29">&nbsp;</P></TD>
	<TD class="tr12 td33"><P class="p28 ft29">&nbsp;</P></TD>
	<TD colspan=2 class="tr11 td43"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td44"><P class="p28 ft28">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td25"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td26"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td27"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td29"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td30"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 class="tr6 td31"><P class="p39 ft4">1x unit of RM300 Lazada</P></TD>
	<TD class="tr6 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr13 td45"><P class="p28 ft5">Spend a minimum of RM2,500 (or</P></TD>
	<TD colspan=2 class="tr6 td34"><P class="p40 ft30">3,000</P></TD>
	<TD class="tr6 td33"><P class="p28 ft18">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr4 td25"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td26"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td27"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td28"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td29"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td30"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td28"><P class="p28 ft22">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td31"><P class="p39 ft4">e-Voucher; OR</P></TD>
	<TD class="tr4 td28"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td35"><P class="p28 ft22">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td36"><P class="p38 ft4">Units</P></TD>
</TR>
<TR>
	<TD class="tr14 td25"><P class="p28 ft31">&nbsp;</P></TD>
	<TD rowspan=2 class="tr0 td26"><P class="p28 ft32">Welcome</P></TD>
	<TD class="tr14 td27"><P class="p28 ft31">&nbsp;</P></TD>
	<TD class="tr14 td28"><P class="p28 ft31">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td46"><P class="p41 ft10">New Primary</P></TD>
	<TD class="tr14 td28"><P class="p28 ft31">&nbsp;</P></TD>
	<TD class="tr14 td28"><P class="p28 ft31">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td45"><P class="p28 ft5">equivalent in foreign currency) on</P></TD>
	<TD class="tr14 td35"><P class="p28 ft31">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr4 td25"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td27"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td28"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td28"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td47"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td33"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td28"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td35"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td37"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td33"><P class="p28 ft22">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr4 td38"><P class="p28 ft22">&nbsp;</P></TD>
	<TD rowspan=2 class="tr0 td26"><P class="p42 ft4">Offer</P></TD>
	<TD class="tr4 td40"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr15 td28"><P class="p28 ft33">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td46"><P class="p43 ft10">Cardholders</P></TD>
	<TD class="tr4 td41"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td48"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td44"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr15 td28"><P class="p28 ft33">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td45"><P class="p28 ft5">Eligible Spend (single/cumulative</P></TD>
	<TD class="tr4 td49"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td50"><P class="p28 ft22">&nbsp;</P></TD>
	<TD class="tr4 td44"><P class="p28 ft22">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr16 td25"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td27"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td28"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td28"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td47"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td33"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td28"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td35"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td37"><P class="p28 ft34">&nbsp;</P></TD>
	<TD class="tr16 td33"><P class="p28 ft34">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td25"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td26"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td27"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td29"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td30"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr17 td31"><P class="p44 ft4">1x unit of Huawei</P></TD>
	<TD class="tr3 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 class="tr3 td45"><P class="p28 ft5">receipts) within the Welcome</P></TD>
	<TD colspan=2 rowspan=3 class="tr18 td34"><P class="p37 ft23">2,000</P></TD>
	<TD class="tr3 td33"><P class="p28 ft18">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr19 td25"><P class="p28 ft35">&nbsp;</P></TD>
	<TD class="tr19 td26"><P class="p28 ft35">&nbsp;</P></TD>
	<TD class="tr19 td27"><P class="p28 ft35">&nbsp;</P></TD>
	<TD class="tr19 td28"><P class="p28 ft35">&nbsp;</P></TD>
	<TD class="tr19 td29"><P class="p28 ft35">&nbsp;</P></TD>
	<TD class="tr19 td30"><P class="p28 ft35">&nbsp;</P></TD>
	<TD class="tr19 td28"><P class="p28 ft35">&nbsp;</P></TD>
	<TD class="tr19 td28"><P class="p28 ft35">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td45"><P class="p28 ft5">Period</P></TD>
	<TD class="tr19 td33"><P class="p28 ft35">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr11 td25"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td26"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td27"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td28"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td29"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td30"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td28"><P class="p28 ft28">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr3 td31"><P class="p38 ft4">Mediapad T3 7.0 (Tablet);</P></TD>
	<TD class="tr11 td28"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td33"><P class="p28 ft28">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr10 td25"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td26"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td27"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td28"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td29"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td30"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td28"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td28"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td32"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td33"><P class="p28 ft27">&nbsp;</P></TD>
	<TD class="tr10 td35"><P class="p28 ft27">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr20 td36"><P class="p38 ft4">Units</P></TD>
</TR>
<TR>
	<TD class="tr7 td25"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td26"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td27"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td28"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td29"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td30"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td28"><P class="p28 ft24">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr0 td31"><P class="p39 ft4">OR</P></TD>
	<TD class="tr7 td28"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td32"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td33"><P class="p28 ft24">&nbsp;</P></TD>
	<TD class="tr7 td35"><P class="p28 ft24">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr8 td25"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td26"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td27"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td28"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td29"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td30"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td28"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td28"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td32"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td33"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td35"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td37"><P class="p28 ft25">&nbsp;</P></TD>
	<TD class="tr8 td33"><P class="p28 ft25">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr9 td38"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td39"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td40"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr11 td28"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td29"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td30"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr9 td41"><P class="p28 ft26">&nbsp;</P></TD>
	<TD colspan=2 class="tr9 td42"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr11 td28"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td32"><P class="p28 ft28">&nbsp;</P></TD>
	<TD class="tr11 td33"><P class="p28 ft28">&nbsp;</P></TD>
	<TD colspan=2 class="tr9 td43"><P class="p28 ft26">&nbsp;</P></TD>
	<TD class="tr9 td44"><P class="p28 ft26">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td25"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td26"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td27"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td29"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td30"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 class="tr6 td31"><P class="p39 ft4">1x unit of iROVA K6S</P></TD>
	<TD class="tr6 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td32"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr6 td33"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 class="tr6 td34"><P class="p40 ft30">3,000</P></TD>
	<TD class="tr6 td33"><P class="p28 ft18">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td25"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td26"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td27"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td29"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td30"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 class="tr3 td31"><P class="p39 ft4">Robotic Vacuum Cleaner</P></TD>
	<TD class="tr3 td28"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td32"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td33"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td35"><P class="p28 ft18">&nbsp;</P></TD>
	<TD colspan=2 class="tr3 td36"><P class="p38 ft4">Units</P></TD>
</TR>
<TR>
	<TD class="tr3 td51"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td52"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td53"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td41"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td54"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td55"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td41"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td48"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td44"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td41"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td56"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td44"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td49"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td50"><P class="p28 ft18">&nbsp;</P></TD>
	<TD class="tr3 td44"><P class="p28 ft18">&nbsp;</P></TD>
</TR>
</TABLE>
<P class="p9 ft5"><SPAN class="ft2">7.</SPAN><SPAN class="ft3">The Gifts under this Promotion are separate and distinct, with the same applicable Eligibility Criteria, to be selected by Eligible Cardholders as per Table above, subject to the following:</SPAN></P>
<P class="p45 ft5"><SPAN class="ft5">a.</SPAN><SPAN class="ft14">Upon fulfilment of the Participation Criteria and Eligibility Criteria, an Eligible Cardholder stands a chance to select and receive one (1) unit of Gift.</SPAN></P>
<P class="p46 ft5"><SPAN class="ft5">b.</SPAN><SPAN class="ft36">Each Eligible Cardholder is only entitled to receive </SPAN><SPAN class="ft9">one (1) unit of Gift of own choice, which is subject to availability and on first come first serve basis</SPAN> throughout the Promotion Period;</P>
<P class="p47 ft5"><SPAN class="ft5">c.</SPAN><SPAN class="ft3">The maximum units of Gift to be given out under this Promotion are pooled together with the</SPAN></P>
<P class="p48 ft5">“HSBC Amanah Get A Gift Campaign” and are listed as per Table above;</P>
<P class="p49 ft5"><SPAN class="ft5">d.</SPAN><SPAN class="ft36">The first year annual fee of the Participating HSBC Credit </SPAN>Card/-i will be waived (if applicable). Subsequent years annual fee will be auto-waived if the Eligible Cardholder(s) swipe the Participating HSBC Credit Card/-i** at least once in a month for consecutive 12 months. No minimum spend amount is required.</P>
<P class="p50 ft5"><SPAN class="ft4">**</SPAN>For HSBC Premier Travel Credit Card, subsequent years annual fee is waived upon annual minimum spending of RM45,000 per annum (primary and supplementary spend included)</P>
</DIV>
<DIV id="page_3">
<DIV id="p3dimg1">
<IMG src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAKbAl4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA7+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoA6eiuY/wCEjvP+ecH/AHyf8aP+EjvP+ecH/fJ/xoAx6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD/2Q==" id="p3img1"></DIV>


<DIV class="dclr"></DIV>
<P class="p51 ft4">GIFT TERMS AND CONDITIONS & FULFILLMENT</P>
<P class="p4 ft5"><SPAN class="ft2">8.</SPAN><SPAN class="ft3">The following terms and conditions apply to the Gift:</SPAN></P>
<P class="p52 ft5"><SPAN class="ft5">a.</SPAN><SPAN class="ft14">The Gift is given on an “As Is” basis;</SPAN></P>
<P class="p22 ft5"><SPAN class="ft5">b.</SPAN><SPAN class="ft14">The Gift is not transferable and cannot be exchanged for cash, credit or in kind;</SPAN></P>
<P class="p53 ft39"><SPAN class="ft5">c.</SPAN><SPAN class="ft37">The Eligible Cardholders under this Promotion will be notified via SMS within four (4) to eight (8) weeks upon fulfilling the Eligibility Criteria (“Winner”). The SMS(s) will contain a unique code and will be sent to the contact details of the Eligible Cardholders maintained in HSBC’s records. Eligible Cardholders will be able to select their choice of Gift using the given unique code at </SPAN><A href="http://www.hsbc.com.my/"><SPAN class="ft38">www.hsbc.com.my/gift</SPAN></A> within thirty (30) days upon receiving the SMS. The unique code will expire after thirty 30 days and after this, no redemption will be possible.</P>
<P class="p52 ft5"><SPAN class="ft5">d.</SPAN><SPAN class="ft14">HSBC Bank reserves the right, at its sole discretion, to provide the Gift in any colour that is available;</SPAN></P>
<P class="p53 ft39"><SPAN class="ft5">e.</SPAN><SPAN class="ft40">The Gift will be couriered within sixteen (16) weeks after the Promotion Period to the Winner’s address as maintained in HSBC’s records. HSBC Bank will not entertain any request to deliver the Gift to an overseas address, a P.O Box address and/or address other than that maintained in HSBC’s record. During the call for delivery address confirmation, the Winners with an overseas address shall nominate a proxy in Malaysia with a Malaysian address who will receive the Gift on behalf of the said Winners;</SPAN></P>
<P class="p54 ft5"><SPAN class="ft5">f.</SPAN><SPAN class="ft41">HSBC may process Eligible Cardholder’s information, for purposes as provided for in HSBC’s Notice to Customers relating to the Personal Data Protection Act 2010 (the “Notice”) and HSBC’s Universal Terms and Conditions and disclose pertinent information to the fulfillment agency to facilitate delivery of the Gift to the Winners. A copy of the Notice can be viewed or downloaded at </SPAN><SPAN class="ft42">www.hsbc.com.my</SPAN></P>
<P class="p55 ft5"><SPAN class="ft5">g.</SPAN><SPAN class="ft14">HSBC Bank reserves the right to substitute the Gift with any other item of similar value at any time with three (3) days prior notice;</SPAN></P>
<P class="p56 ft5"><SPAN class="ft5">h.</SPAN><SPAN class="ft14">HSBC Bank will not be held liable for any mishaps, injuries or accidents that may occur in the course of delivery or usage of the Gift under this Promotion;</SPAN></P>
<P class="p52 ft5"><SPAN class="ft6">i.</SPAN><SPAN class="ft43">Any loss or damage to the Gift is passed on to the Gift Winners upon delivery of the Gift;</SPAN></P>
<P class="p53 ft5"><SPAN class="ft5">j.</SPAN><SPAN class="ft41">To the fullest extent permitted by law, HSBC Bank expressly excludes and disclaims any representations, warranties or endorsement, express or implied, written or oral, including but not limited to, any warranty of quality, merchantability or fitness for a particular purpose in respect of the Gift;</SPAN></P>
<P class="p55 ft5"><SPAN class="ft5">k.</SPAN><SPAN class="ft3">The Gift does not include any accessories or items that are shown in the leaflet or website or any marketing materials, as they are for illustration purposes only;</SPAN></P>
<P class="p57 ft39"><SPAN class="ft2">9.</SPAN><SPAN class="ft37">During the Promotion Period and at the time of Gift fulfilment, the Participating HSBC Credit </SPAN>Card/-i <SPAN class="ft44">MUST BE PIN ACTIVATED </SPAN>and MUST NOT be delinquent, closed, and/or invalid/inactive, dormant or cancelled within HSBC Bank’s definition. Otherwise, they will be disqualified from receiving the Gift from this Promotion.</P>
<P class="p5 ft4">GENERAL TERMS & CONDITIONS</P>
<P class="p58 ft5"><SPAN class="ft2">10.</SPAN><SPAN class="ft45">HSBC reserves the right at its absolute discretion to vary, delete or add to any of these Terms and Conditions with 3 days prior notice. The amended Terms and Conditions shall prevail over any provisions or representations contained in any other promotional materials advertising this Promotion.</SPAN></P>
<P class="p59 ft5"><SPAN class="ft2">11.</SPAN><SPAN class="ft45">HSBC may use any of the following modes to communicate notices in relation to this Promotion to the Eligible Cardholder:</SPAN></P>
<P class="p60 ft5"><SPAN class="ft5">a.</SPAN><SPAN class="ft45">individual notice to the Eligible Cardholder (whether by written notice or via electronic means) sent to the Eligible Cardholder’s latest address/email address as maintained in the HSBC’s records;</SPAN></P>
<P class="p61 ft5"><SPAN class="ft5">b.</SPAN><SPAN class="ft45">press advertisements;</SPAN></P>
<P class="p62 ft5"><SPAN class="ft5">c.</SPAN><SPAN class="ft46">notice in the Eligible Cardholder’s credit card statement(s) or composite statement;</SPAN></P>
<P class="p62 ft5"><SPAN class="ft5">d.</SPAN><SPAN class="ft45">display at its business premises; or</SPAN></P>
<P class="p62 ft5"><SPAN class="ft5">e.</SPAN><SPAN class="ft45">notice on HSBC internet website(s);</SPAN></P>
<P class="p63 ft5">where such notices shall be deemed to be effective on and from the 4<SPAN class="ft47">th </SPAN>day after its delivery/publication/display as per the manner described herein. Save and except notices sent via ordinary mail which will be deemed delivered on the 3<SPAN class="ft47">rd </SPAN>day after posting, notices sent via other modes as described herein are deemed delivered immediately after posting/publication/display.</P>
</DIV>
<DIV id="page_4">


<P class="p64 ft5"><SPAN class="ft2">12.</SPAN><SPAN class="ft45">This Promotion’s Terms and Conditions are in addition to the respective Universal Terms and Conditions (“</SPAN><SPAN class="ft4">UTCs</SPAN>”) for HSBC Bank of which the respective Cardholder Agreements are a part of and which regulate the provision of credit card facilities by HSBC. The UTCs are available at <SPAN class="ft42">www.hsbc.com.my</SPAN>. In the event of inconsistency between this Promotion’s Terms and Conditions and any of the aforementioned Terms and Conditions, this Promotion’s Terms and Conditions shall prevail insofar as they apply to this Promotion.</P>
<P class="p65 ft5"><SPAN class="ft2">13.</SPAN><SPAN class="ft45">HSBC shall not be liable for any default due to any act of God, war, riot, strike, terrorism, epidemic, lockout, industrial action, fire, flood, drought, storm or any event beyond the reasonable control of HSBC.</SPAN></P>
<P class="p66 ft5"><SPAN class="ft2">14.</SPAN><SPAN class="ft45">HSBC reserves the right to cancel, terminate or suspend this Promotion with 3 days prior notice. For the avoidance of doubt, cancellation, termination or suspension by HSBC of this Promotion shall not entitle the Eligible Cardholder to any claim or compensation against HSBC for any and all losses or damages suffered or incurred by the Eligible Cardholder as a direct or indirect result of the act of cancellation, termination or suspension.</SPAN></P>
<P class="p67 ft5"><SPAN class="ft2">15.</SPAN><SPAN class="ft45">HSBC shall only be liable for any loss or damage suffered or incurred as a direct result of HSBC’s gross negligence and shall not be liable for any other loss or damage of any kind such as loss of income, profit, goodwill or indirect, incidental, exemplary, punitive, consequential or special loss or damage howsoever arising, whether or not HSBC have been advised of the possibility of such loss or damage.</SPAN></P>
<P class="p68 ft5"><SPAN class="ft2">16.</SPAN><SPAN class="ft45">The Eligible Cardholder shall be personally responsible for all taxes, rates, government fees or any other charges that may be levied against them under applicable laws, if any, in relation to this Promotion.</SPAN></P>
<P class="p69 ft5"><SPAN class="ft2">17.</SPAN><SPAN class="ft45">HSBC’s decision on all matters relating to this Promotion shall be final and binding.</SPAN></P>
</DIV>

	<div class="text-center">
		<button type="button" class="btn btn-super-danger2 text-center"  data-dismiss="modal">Close </button>
	</div>

	</div>
	
        </div>
       
      </div>
      
    </div>
  </div>
  
</div>
@endsection




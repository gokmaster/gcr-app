@extends('admin.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        Image upload failed.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-header"><h2>Edit Product</h2></div>

                <div class="card-body">
                
                    <form id="product_form" method="post" action="{{ url('/product/update')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="product_id" name="product_id" value="{{ $product[0]['product_id'] }}">
                        <div class="form-group">
                            <label for="product_name">Product Name: </label>
                            <input type="text" class="form-control" id="product_name" name="product_name" required 
                                @if(session()->has('success_message') || session()->get('fail_message')  )
                                    value="{{ old('product_name') }}"
                                @else
                                    value="{{ $product[0]['product_name'] }}"
                                @endif
                            >
                        </div>

                        <div class="form-group shadow-textarea">
                            <label for="exampleFormControlTextarea6">Product Description</label>
                            <textarea class="form-control z-depth-1" id="description" name="description" rows="3" placeholder="Write product description here">@if(session()->has('success_message') || session()->get('fail_message')  ){{ old('description') }} @else {{ $product[0]['description'] }} @endif</textarea>
                        </div>
                                              
                        <div class="form-group">
                            <label for="sku">SKU: </label>
                            <input type="text" class="form-control" id="sku" name="sku" required 
                                @if(session()->has('success_message') || session()->get('fail_message')  )
                                    value="{{ old('sku') }}"
                                @else
                                    value="{{ $product[0]['sku'] }}"
                                @endif
                            >
                        </div>

                        <div class="form-group">
                            <label for="category">Category: </label>
                            <input type="text" class="form-control" id="category" name="category" required 
                                @if(session()->has('success_message') || session()->get('fail_message')  )
                                    value="{{ old('category') }}"
                                @else
                                    value="{{ $product[0]['categories'] }}"
                                @endif
                            >
                        </div>

                        <div class="form-group">
                            <label for="price">Price: </label>
                            <input type="number" class="form-control" id="price" name="price" min="0" step="0.01" required 
                                @if(session()->has('success_message') || session()->get('fail_message')  )
                                    value="{{ old('price') }}"
                                @else
                                    value="{{ $product[0]['price'] }}"
                                @endif
                            >
                        </div>

                        <div class="form-group">
                            <label for="price">Quantity remaining: </label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="0" step="1" required disabled 
                                    value="{{ $product[0]['quantity'] }}"
                            >
                        </div>

                        <div class="form-group">
                            <label for="price">Additional Quantity Ordered: </label>
                            <input type="number" class="form-control" id="quantity_ordered" name="quantity_ordered" step="1" 
                            >
                        </div>

                        @if ( count($productsOrdered) > 0 ) 
                            <h4>History of Product Inventory Orders Added</h4>
                            <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Date Added</th>
                                    <th>Quantity</th>
                                    <th>Added by</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($productsOrdered as $ft)
                                <tr>
                                    <td>{{ date('d-m-Y h:i:s', strtotime( $ft['created_at'])) }}</td>
                                    <td>{{ $ft['quantity'] }}</td>
                                    <td>{{ $ft['username'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01">Upload Picture</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="imageupload" id="imageupload">
                            </div>
                        </div>
                        <br>

                        <button id="btnAddProduct" type="submit" class="btn btn-primary">Save Edits</button>  
                        &nbsp
                        <a href="{{ url('/product/all') }}" class="btn btn-default" >Cancel</a>  
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
    <script>
         $('#product_form').validate({
            rules: {
                description: {
                    maxlength: 255,                 
                },
                
            },
            messages: {},
            errorElement : 'div',
            errorLabelContainer: '.errorTxt'
        });
    </script>
@endsection

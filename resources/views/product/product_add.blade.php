@extends('admin.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif

                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        Image upload failed.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-header"><h2>Add Product for Campaign {{ $campaign[0]['name'] }}</h2></div>

                <div class="card-body">
                
                    <form id="product_form" method="post" action="{{url('/product/insert') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="campaign_id" name="campaign_id" value="{{ $campaign[0]['campaign_id'] }}">
                        <div class="form-group">
                            <label for="product_name">Product Name: </label>
                            <input type="text" class="form-control" id="product_name" name="product_name" value="{{ old('product_name') }}" required>
                        </div>

                        <div class="form-group shadow-textarea">
                            <label for="description">Product Description</label>
                            <textarea class="form-control z-depth-1" id="description" name="description" rows="3" placeholder="Write product description here">{{ old('description') }}</textarea>
                        </div>
                                              
                        <div class="form-group">
                            <label for="sku">SKU: </label>
                            <input type="text" class="form-control" id="sku" name="sku"  value="{{ old('sku') }}" required>
                        </div>

                        <div class="form-group">
                            <label for="category">Category: </label>
                            <input type="text" class="form-control" id="category" name="category"  value="{{ old('category') }}" required>
                        </div>

                        <div class="form-group">
                            <label for="price">Price: </label>
                            <input type="number" class="form-control" id="price" name="price" min="0" step="0.01"  value="{{ old('price') }}" required>
                        </div>

                        <div class="form-group">
                            <label for="price">Ordered Quantity: </label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="0" step="1"  value="{{ old('quantity') }}" required>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01">Upload Picture</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="imageupload" id="imageupload" required>
                            </div>
                        </div>
                        <br>

                        @if(session()->has('success_message'))
                            <a href="{{ route('product.add', ['campaign_id' => $campaign[0]['campaign_id'] ]) }}" class="btn btn-primary" >Add another product</a> 
                            &nbsp
                            <a href="{{ route('campaign.all') }}" class="btn btn-primary" >Back to Campaigns</a> 
                            &nbsp
                            <a href="{{ route('product.all') }}" class="btn btn-primary" >Back to Products</a> 
                            &nbsp
                        @else
                            <button id="btnAddProduct" type="submit" class="btn btn-primary">Add Product</button>
                            &nbsp
                            <a href="{{ url('/campaign/all') }}" class="btn btn-default" >Cancel</a> 
                        @endif  

                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
    <script>
        $('#product_form').validate({
            rules: {
                description: {
                    maxlength: 255,                 
                },
                
            },
            messages: {},
            errorElement : 'div',
            errorLabelContainer: '.errorTxt'
        });
    </script>
@endsection

@extends('admin.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @elseif(session()->has('fail_message'))
                    <div class="alert alert-danger">
                        {{ session()->get('fail_message') }}
                    </div>
                @endif
                <div class="card-header"><h2>Delete Product</h2></div>

                <div class="card-body">
                
                    <form method="post" action="{{ action('ProductController@delete_post') }}">
                        @csrf
                        <input type="hidden" id="product_id" name="product_id" value={{ $product[0]['product_id'] }}>
                        Are you sure you want to delete <b>{{ $product[0]['product_name'] }}</b> product? 
                        <br><br>
                                           
                        <button id="btnDeleteProduct" type="submit" class="btn btn-primary">Delete Product</button>
                        &nbsp
                        <a href="{{ url('/product/all') }}" class="btn btn-default" >Cancel</a>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@extends('admin/layout')

@section('header')
<h3>Products for Campaign {{ $products[0]['campaign_name'] }} </h3>
@endsection

@section('content')

    <div class="box-body table-responsive no-padding">
        <table class="table table-hover customsorting">
            <thead>
            <tr>
                    <th>Product Name</th>
                    <th>Campaign Name</th>
                    <th>Bank Name</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>SKU</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Quantity</th>
                @if ( in_array("edit product", $spData['permittedtasks']) || in_array("delete product", $spData['permittedtasks']))
                    <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $product['product_name'] }}</a></td>
                <td>{{ $product['campaign_name'] }}</td>
                <td>{{ $product['name'] }}</td>
                <td><img width="auto" height="90px" src="{{ URL::to('/') }}/{{ $product['filepath'] }}"></td>
                <td>{{ $product['description'] }}</td>
                <td>{{ $product['sku'] }}</td>
                <td>{{ $product['categories'] }}</td>
                <td>{{ number_format((float)$product['price'], 2, '.', '') }}</td>
                <td>{{ $product['quantity'] }}</td>
                @if ( in_array("edit product", $spData['permittedtasks']) || in_array("delete product", $spData['permittedtasks']))
                <td> 
                    <form method="post" class="form-no-margin" action="{{ action('ProductController@delete_post') }}">
                        @csrf  
                        @if ( in_array("edit product", $spData['permittedtasks']) )
                            <a href="{{route('product.edit', ['product_id' => $product['product_id']]) }}" class="btn btn-link inline-button" >Edit</a>
                        @endif

                        @if ( in_array("edit product", $spData['permittedtasks']) && in_array("delete product", $spData['permittedtasks']))
                            |
                        @endif

                        @if ( in_array("delete product", $spData['permittedtasks']) )        
                            <input type="hidden" id="product_id" name="product_id" value="{{ $product['product_id'] }}">                                   
                            <button id="btnDeleteProduct" type="submit" class="btn btn-link inline-button">Disable</button>
                        @endif
                    </form>
                </td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
        {{ $products->links() }}

    </div>


@endsection
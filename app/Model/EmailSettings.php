<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use Config;

  
class EmailSettings extends Model {

    public function insert($form_param) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/emailsettings/add"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    }

    public function deleteEmailSettings($email_settings_id) {
        $a = new Admin_id;
        $admin_id = $a->admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/emailsettings/delete"; 

        $form_param = [
            'email_settings_id' => $email_settings_id,
            'admin_id' => $admin_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    }

    public function fetchEmailSettings() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/emailsettings"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);
        
        return $data;
    } 
        
    public function updateEmailSettings($form_param) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/emailsettings/update"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    }

    public function fetchSystemSenderEmail() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/emailsettings/senderemail"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);
        
        return $data['sender_email']['email'];
    }

    public function fetchSystemReceiverEmail() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/emailsettings/receiveremail"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);
        
        return $data['receiver_email']['email'];
    }

    public function fetchGeneralReceiverEmail() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/emailsettings/receiveremail/general"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        $email_array = array();

        foreach ($data['receiver_emails'] as $d) {
            array_push($email_array , $d['email']);
        }
        
        return $email_array;
    }

}
?>
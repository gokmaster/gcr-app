<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use Config;
use Illuminate\Http\Request;
  
class Unique_code extends Model
{
    public function unique_code_batches_of_campaign($campaign_id) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/batches/". $campaign_id; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);
        
        return $data;
    }


    public function fetchApprovedBatchesOfCampaign($campaign_id) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/approvedbatches"; 

        $form_param = [
            'campaign_id' => $campaign_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data['batches'];
    }

    
    public function approve_unique_code_for_campaign($campaign_id, $batch, $unique_code_qty, $unique_codes_pending_id) {
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/approve"; 

        $form_param = [
            'campaign_id' => $campaign_id,
            'batch' => $batch,
            'unique_code_qty' => $unique_code_qty,
            'unique_codes_pending_id' => $unique_codes_pending_id,
            'admin_id' => $admin_user_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    }


    public function rejectUniqueCode($unique_codes_pending_id) {
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/reject"; 

        $form_param = [
            'unique_codes_pending_id' => $unique_codes_pending_id,
            'admin_id' => $admin_user_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    }    


    public function uniquecodeDownloadLogInsert($campaign_id, $batch) {
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/download/log-insert"; 

        $form_param = [
            'admin_id' => $admin_user_id,
            'campaign_id' => $campaign_id,
            'batch' => $batch,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    } 
    
    
    public function importBankMasterlist($ic_codes_array, $campaign_id, $filename) {
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/uniquecode/import_bank_masterlist"; 
                            
        $form_param = [
            'ic_codes_array' => $ic_codes_array,
            'campaign_id' => $campaign_id,
            'admin_id' => $admin_user_id,
            'filename' => $filename,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        return $data;
    }

    
    public function excelUploadHistoryforCampaign($campaign_id) {
        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/uniquecode/exceluploadhistory-campaign/$campaign_id"; 
                            
        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        return $data['uploads'];
    }
     
}
?>
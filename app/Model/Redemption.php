<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use Config;
use Illuminate\Http\Request;
  
class Redemption extends Model
{
    public function count_of_redemption_today() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/report/count_redemptions_todays"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);
        
        return $data['count'];
    }
   
}
?>
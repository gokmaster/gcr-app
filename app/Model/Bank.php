<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use Config;
  
class Bank extends Model
{
    public function all_bank_with_campaign() {
        $form_param = [];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/bank/all"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        $bank_array = array();

        $i = 0;
       
        // group by bank
        foreach ($data['banks'] as $d) {

            if (!isset($bank_array[$d['bank_name']]) ) { 
                $bank_array[$d['bank_name']] = array();
            }

            array_push($bank_array[$d['bank_name']] , $d);

            $i++;
        }

        $now = strtotime(date('Y-m-d H:i:s'));

        foreach ($bank_array as $bank) {

            foreach ($bank as $campaign)
            
            if (strcmp($campaign['bank_deleted'], 1) === 0) {
                // if bank is already disabled, do not show disable
                $bank_array[$campaign['bank_name']][0]['show_disable'] = 0;
            } elseif (strtotime($campaign['end_date']) >= $now && strcmp($campaign['bank_deleted'],0) === 0) {
                // if bank has active campaigns, do not show "disable"
                $bank_array[$campaign['bank_name']][0]['show_disable'] = 0;
                break; // exit foreach if bank has atleast one active campaign
            } else {
                $bank_array[$campaign['bank_name']][0]['show_disable'] = 1;
            }
        }

        //dd($bank_array);
        
        return $bank_array;
    } 
}
?>
<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use Config;
use Illuminate\Http\Request;
  
class Users extends Model
{
    public function users_not_unassigned_roles($assigned_users = array() ) {
        $userdomain = Config::get('globalvariables.user_api'); // gcr_api is defined in app/config/globalvariables.php
        $user_url = $userdomain . "/api/v1/users/all"; 

        $HttpReq = new HttpRequest;
        $userdata = $HttpReq->getJson($user_url);

        $assigned_users_array = array();

        foreach ($assigned_users as $assigned_user) {
            array_push($assigned_users_array ,  $assigned_user['username']);
        }

        $data = array();

        foreach ($userdata['users'] as $user) {

            if ( !in_array( $user['username'] , $assigned_users_array ) )
                $data[] = [
                    'id' => $user['id'],
                    'username' => $user['username'],
                ];
        }

        return $data;
    }


    public function loggedInUser() {
        $a = new Admin_id;
        $admin_id = $a->admin_id();

        $userdomain = Config::get('globalvariables.user_api'); // gcr_api is defined in app/config/globalvariables.php
        $user_url = $userdomain . "/api/v1/user/loggedin"; 

        $form_param = [
            'admin_id' => $admin_id,
        ];

        $HttpReq = new HttpRequest;
        $userdata = $HttpReq->post($user_url, $form_param);

        return $userdata['user'];
    }


    
     
}
?>
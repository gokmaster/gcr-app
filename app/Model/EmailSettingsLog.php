<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mylib\HttpRequest;
use Config;

  
class EmailSettingsLog extends Model {

    public function fetch() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/auditlog/emailsettingslog"; 

        $form_param = [];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    } 
        
}
?>
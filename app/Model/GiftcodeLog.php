<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mylib\HttpRequest;
use Config;

  
class GiftcodeLog extends Model {

    public function fetch() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/auditlog/giftcodelog"; 

        $form_param = [];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        $data = (array) $data;

        $a = 0;

        foreach ($data['logs'] as $d) {
            if (strcmp($d['status'], 0) === 0) {
                $data['logs'][$a]['status'] = 'pending approval';
            } elseif (strcmp($d['status'], 1) === 0) {
                $data['logs'][$a]['status'] = 'approved';
            } elseif (strcmp($d['status'], 2) === 0) {
                $data['logs'][$a]['status'] = 'rejected';
            }
            $a++;
        }
        
        return $data;
    } 
        
}
?>
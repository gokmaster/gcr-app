<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mylib\HttpRequest;
use Config;

  
class ProductLog extends Model {

    public function fetchCampaignProductLog() {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/auditlog/campaign-product"; 

        $form_param = [];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data['logs'];
    } 
        
}
?>
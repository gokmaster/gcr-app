<?php 

namespace App\Model;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use Config;
  
class Roles extends Model
{
    public function delete_role($role_id) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/delete"; 

        $form_param = [
            'role_id' => $role_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
        
        return $data;
    }


    public function fetch_users_of_role($role_id) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/users_of_role/". $role_id; 

  
        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);
        
        return $data; 
    }


    public function usersPermittedTasks() {
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/user_tasks"; 

        $form_param = [
            'admin_id' => $admin_user_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return $data;
    }

    public function usersPermittedTaskList() {
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/user_tasklist"; 

        $form_param = [
            'admin_id' => $admin_user_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        $task_array = array();

        foreach ($data['tasks'] as $task) {
            array_push($task_array, trim($task['task_name']) );
        }

        return $task_array;
    }  
    
    
    public function last5ChangesforUser($admin_id) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/user/lastfivechanges"; 

        $form_param = [
            'admin_id' => $admin_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return $data['log'];    
    }


    public function unassignUserRole($userId) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/unassign-user-role"; 

        $form_param = [
            'user_id' => $userId,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return $data;    
    }

}
?>
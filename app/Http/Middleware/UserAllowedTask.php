<?php

namespace App\Http\Middleware;

use Closure;

class UserAllowedTask
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$request->route()->getPath();
        //$name = $request->route()->getPath();
        //return "hello";
        return $next($request);
    }
}

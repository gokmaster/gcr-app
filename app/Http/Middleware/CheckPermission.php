<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Route;
use App\Mylib\HttpRequest;
use App\Model\Roles;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rolesModel = new Roles;
        $data = $rolesModel->usersPermittedTasks();

        $allRoutes = $data['all_tasks'];
             
        $destinationURL = class_basename(Route::currentRouteAction());
                        
        $userPermissions = $data['user_task_ids']; 
      
        $allowed = false; //initialized to false
        
        foreach($allRoutes as $key => $route){ //go through each route...
            if(strcmp($destinationURL, $route['frontend_view']) === 0 ){ //...in order to find out if your destination is in the list
                //if it IS, check if the key is in the allowed user permission
                $allowed = in_array($route['task_id'], $userPermissions); //return true if the key (i.e permissionId) is also in the $userPermission array

                if($allowed && strcmp($destinationURL, $route['frontend_view']) == 0 ){ //if it is allowed, let the user through
                    //dd($route['frontend_view']);
                    return $next($request);    
                }
            }
        }

        //if it is not redirect to whereversafe (or error message if you disabled the front end).
        return redirect()->route('admin.permission_denied');
        
    }
}
<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use App\Mylib\HttpRequest;

class Authenticate {
    public function handle($request, Closure $next) {
        $auth = new \SimpleSAML\Auth\Simple('example-sql');

        // if not logged-in
        if ( !$auth->isAuthenticated() ) {
            return redirect( action('LoginTestController@index') );
        } else {

            /* $domain = Config::get('globalvariables.gcr_api');
            $url = $domain . "/api/v1/admin/store_admin_id"; 
            
            $login_data = $auth->getAttributes();
            $admin_user_id = $login_data['id'];
                              
            $form_param = [
                'admin_user_id' => $admin_user_id,
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $form_param); */
        }

        return $next($request);
       
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;
use App\Http\Middleware\Authenticate;
use App\Mylib\Admin_id;
use App\Model\Bank;

class BankController extends Controller {

    public function __construct() {
        $this->middleware(Authenticate::class); // is user logged-in?
    }
    
    public function fetch_all() {   
        $model = new Bank;
        $data = $model->all_bank_with_campaign();

        $today = strtotime(date("Y-m-d H:i:s") );
    
        return view('admin/bank/all_banks', [
            'banks' => $data ,
            'today' => $today,
        ]);         
    }


    public function insert(Request $request) {   
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/bank/insert"; 
     
        $name =  $request->input('name');     
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'name' => $name, 
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }   
    }


    public function add_bank_form() {   

        return view('admin/bank/add_bank');         
    }


    public function edit_bank_form($bank_name, $bank_id) {   

        return view('admin/bank/edit_bank',  ['bankname' => $bank_name, 'bank_id' => $bank_id ]);         
    }


    public function delete_bank_form($bank_name, $bank_id) {   

        return view('admin/bank/delete_bank',  ['bankname' => $bank_name, 'bank_id' => $bank_id ]);         
    }


    public function update(Request $request) {
        
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/bank/update"; 
        
        $bank_id =  $request->input('bank_id'); 
        $name =  $request->input('name');     
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'bank_id' => $bank_id,
            'name' => $name, 
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }           
    }


    public function delete(Request $request) {   
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/bank/delete"; 
        
        $bank_id =  $request->input('bank_id'); 
          
        $form_param = [
            'admin_id' => $admin_user_id,
            'bank_id' => $bank_id, 
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message']); 
        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }           
       
    }


}
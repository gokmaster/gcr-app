<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;
use App\Http\Middleware\Authenticate;
use Illuminate\Pagination\LengthAwarePaginator;
use Excel;
use App\Mylib\Admin_id;

class ReportController extends Controller {

    public function __construct() {
        $this->middleware(Authenticate::class); // is user logged-in?
    }


    public function newsletter(Request $request) {

        $form_param = [
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/successful_redemption";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/report/newsletter', [
            'redemptions' => $data['redemptions'] ,
        ]);
    }


    public function successful_redemptions(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/successful_redemption";

        $form_param = [];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

       /*  // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['redemptions'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 10;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url()); */

        return view('admin/report/successful_redemptions', [
            'redemptions' => $data['redemptions'] ,
        ]);
    }

    public function successful_redemptions_export_excel(Request $request) {

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/successful_redemption/export/excel";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);


        /* if ( !array_key_exists("access_allowed", $data) ) {
            return "Permission denied";
        } elseif ($data['access_allowed'] == 0) {
            return "Permission denied";
        } */

        if ($data['status'] == 'success') {                
                $headers = [
                    'Date of transaction',   //date_redeemed
                    'Bank',//bank_name
                    'Campaign',//campaign_name
                    'Transaction ID',//order_no
                    'Name',//name
                    'Contact Number',//contact_no
                    'Address line 1',//address1
                    'Address line 2',//address2
                    'Address line 3',//address3
                    'Address line 4',//address4
                    'Postcode',//zip_code
                    'City',//city
                    'State',//state
                    'Home no',//home_no
                    'Office no',//office_no
                    'Mobile no',//mobile
                    'Last 6-digit Credit Card/IC',//ic_creditcard_no
                    'Unique Code',//unique_code
                    'Gift Code',//giftcode_id
                ];
                $data_array = array();
                //$a = 0;
                array_push($data_array, $headers); // excel header
                if (count($data['redemptions']) > 0) {
                    foreach( $data['redemptions'] as $redemption )  {
                        $each_row = [
                            'date_redeemed' => $redemption['date_redeemed'],
                            'bank_name' => $redemption['bank_name'],
                            'campaign_name' => $redemption['campaign_name'],
                            'order_no' => $redemption['order_no'],
                            'name' => $redemption['name'],
                            'contact_no' => $redemption['contact_no'],
                            'address1' => $redemption['address1'],
                            'address2' => $redemption['address2'],
                            'address3' => $redemption['address3'],
                            'address4' => $redemption['address4'],
                            'zip_code' => $redemption['zip_code'],
                            'city' => $redemption['city'],
                            'state' => $redemption['state'],
                            'home_no' => $redemption['home_no'],
                            'office_no' => $redemption['office_no'],
                            'mobile' => $redemption['mobile'],
                            'ic_creditcard_no' => $redemption['ic_creditcard_no'],
                            'unique_code' => $redemption['unique_code'],
                            'giftcode_id' => $redemption['giftcode_id'],
                        ];
                        array_push($data_array, $each_row);
                        //$a++;
                    } // foreach
        
                    $filename = "successful_redemptions".date("ymdhis");
                    $filename_with_ext = $filename.".xlsx";
                    Excel::create($filename, function($excel) use ($data_array) {
                        $excel->setTitle('Successful Redemptions');
                        $sheetname = "successful-redemptions";
                        $excel->sheet($sheetname , function($sheet) use ($data_array) {
                            $from = "A1"; // or any value
                            $to = "S1"; // or any value
                            $sheet->getStyle("$from:$to")->getFont()->setBold( true );
                            $sheet->fromArray($data_array, null, 'A1', false, false);
                            //$sheet->protect('password123');
                        });
                    })->download();
                } // foreach

        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }
        
    } //  successful_redemptions_export_excel

   
    public function search_successful_redemptions(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/successful_redemption/search";

        $keyword = $request->input('keyword');
		$search_option = $request->input('search_option');
		
		$form_param = [
            'keyword' => $keyword,
			'search_option' => $search_option
		];
		
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
		
		// Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['redemptions'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = (count($data['redemptions']) == 0) ? 10 : count($data['redemptions']);
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/successful_redemptions', [
            'redemptions' => $paginatedItems ,
        ]);
    }


    public function failed_redemptions(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/failed_transactions";

        $form_param = [];
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

         // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['failed_transactions'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 10;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/failed_transactions', [
            'failed_transactions' => $paginatedItems ,
        ]);
    }


    public function search_failed_redemptions(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/failed_transactions/search";

        $keyword = $request->input('keyword');
		$search_option = $request->input('search_option');
		
		$form_param = [
            'keyword' => $keyword,
			'search_option' => $search_option
		];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
		// Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['failed_transactions'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = (count($data['failed_transactions']) == 0) ? 10 : count($data['failed_transactions']);
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/failed_transactions', [
            'failed_transactions' => $paginatedItems ,
        ]);
    }


    public function failed_redemptions_export_excel(Request $request) { 

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
        ];


        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/failed_redemption/export/excel";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        /* if ( !array_key_exists("access_allowed", $data) ) {
            return "Permission denied";
        } elseif ($data['access_allowed'] == 0) {
            return "Permission denied";
        } */

        if ($data['status'] == 'success') { 
            $headers = [
                'Date',   //created_on
                'Bank',//bank_name
                'Campaign',//campaign_name
                'Ticket ID',//ticket_number
                'Name',//name
                'Last 6-digit Credit Card/IC',//ic_creditcard_no
                'Contact Number',//mobile
                'Unique Code Input',//unique_code_entered
                'Error Code',//error_code
                'Error Description',//error_description
            ];
            $data_array = array();
            //$a = 0;
            array_push($data_array, $headers); // excel header
            if (count($data['failed_transactions']) > 0) {
                foreach( $data['failed_transactions'] as $failed_transaction )  {
                    $each_row = [
                        'created_on' => $failed_transaction['created_on'],
                        'bank_name' => $failed_transaction['bank_name'],
                        'campaign_name' => $failed_transaction['campaign_name'],
                        'ticket_number' => $failed_transaction['ticket_number'],
                        'name' => $failed_transaction['name'],
                        'ic_creditcard_no' => $failed_transaction['ic_creditcard_no'],
                        'mobile' => $failed_transaction['mobile'],
                        'unique_code_entered' => $failed_transaction['unique_code_entered'],
                        'error_code' => $failed_transaction['error_code'],
                        'error_description' => $failed_transaction['error_description'],
                        ];
                    array_push($data_array, $each_row);
                    //$a++;
                } // foreach

                $filename = "failed_transactions".date("ymdhis");
                $filename_with_ext = $filename.".xlsx";
                Excel::create($filename, function($excel) use ($data_array) {
                    $excel->setTitle('Failed Transactions');
                    $sheetname = "Failed Transactions";
                    $excel->sheet($sheetname , function($sheet) use ($data_array) {
                        $from = "A1"; // or any value
                        $to = "J1"; // or any value
                        $sheet->getStyle("$from:$to")->getFont()->setBold( true );
                        $sheet->fromArray($data_array, null, 'A1', false, false);
                        //$sheet->protect('password123');
                    });
                })->download();
            } // if

        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }
    }


    public function redemptionEmailStatus(Request $request) {

        $form_param = [
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/emailstatus";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/report/emailstatus', [
            'emailstatus' => $data['emailstatus'] ,
        ]);
    }


    public function sms_status(Request $request) {

        $form_param = [
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/sms_status";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

          // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['sms_status'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 10;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/sms_status', [
            'sms_status' => $paginatedItems ,
        ]);
    }


    public function sms_status_export_excel(Request $request) {

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
        ];


        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/sms_status/export/excel";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        /* if ( !array_key_exists("access_allowed", $data) ) {
            return "Permission denied";
        } elseif ($data['access_allowed'] == 0) {
            return "Permission denied";
        } */
        if ($data['status'] == 'success') { 
            $headers = [
                'SMS Sent Date',   //sent_date
                'Bank',//bank_name
                'Campaign',//campaign_name
                'SMS Transaction ID',//sms_transact_id
                'Name',//name
                'Contact Number',//mobile_no
                'Last 6-digit Credit Card/IC',//ic_creditcard_no
                'Unique Code Input',//unique_code
                'SMS status',//sms_status
                'Gift Code',//gift_code
                'SMS content',//sms_content
                'Redemption Transaction ID',//redemption_id
            ];
            $data_array = array();
            //$a = 0;

            array_push($data_array, $headers); // excel header
            if (count($data['sms_status']) > 0) {
                foreach( $data['sms_status'] as $sms_status)  {
                    $each_row = [
                        'sent_date' => $sms_status['sent_date'],
                        'bank_name' => $sms_status['bank_name'],
                        'campaign_name' => $sms_status['campaign_name'],
                        'sms_transact_id' => $sms_status['sms_transact_id'],
                        'name' => $sms_status['customer_name'],
                        'mobile_no' => $sms_status['mobile_no'],
                        'ic_creditcard_no' => $sms_status['ic_creditcard_no'],
                        'unique_code' => $sms_status['unique_code'],
                        'sms_status' => $sms_status['sms_status'],
                        'gift_code' => $sms_status['gift_code'],
                        'sms_content' => $sms_status['sms_content'],
                        'redemption_id' => $sms_status['redemption_id'],
                        ];
                    array_push($data_array, $each_row);
                    //$a++;
                } // foreach

                $filename = "sms_status".date("ymdhis");
                $filename_with_ext = $filename.".xlsx";
                Excel::create($filename, function($excel) use ($data_array) {
                    $excel->setTitle('SMS Status');
                    $sheetname = "SMS Status";
                    $excel->sheet($sheetname , function($sheet) use ($data_array) {
                        $from = "A1"; // or any value
                        $to = "L1"; // or any value
                        $sheet->getStyle("$from:$to")->getFont()->setBold( true );
                        $sheet->fromArray($data_array, null, 'A1', false, false);
                        //$sheet->protect('password123');
                    });
                })->download();
            }
        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }

    }


    public function search_sms_status(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/sms_status/search";

        $keyword = $request->input('keyword');
		$search_option = $request->input('search_option');
		
        $form_param = [
            'keyword' => $keyword,
			'search_option' => $search_option
		];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
		
		// Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['sms_status'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = (count($data['sms_status']) == 0) ? 10 : count($data['sms_status']);
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/sms_status', [
            'sms_status' => $paginatedItems ,
        ]);
    }


    public function excelDownloadHistory(Request $request) {

        $uploadedFileId = $request->input('uploadedfileID');
        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/excel/downloadhistory";

        $form_param = [
            'uploaded_file_id' => $uploadedFileId
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return $data;

        //return "testing";

    }


    public function excel_files(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/excelfiles";

        $form_param = [];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        //dd($data);
         
        // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['excelfiles'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 15;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/excelfiles', [
            'excelfiles' => $paginatedItems,
        ]);
    }


    public function search_excel_files(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/excelfiles";

        $keyword = $request->input('keyword');

        $form_param = [
            'keyword' => $keyword,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

         // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['excelfiles'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 15;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/excelfiles', [
            'excelfiles' => $paginatedItems,
        ]);
    }


    /* Report Inventory */
    public function inventory(Request $request) {
        $form_param = [
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/inventory";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

          // Pagination
		/* $currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['inventory'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 10;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url()); */

        return view('admin/report/inventory', [
            'inventory' => $data['inventory'],
        ]);
    }


    public function search_inventory(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/inventory/search";

        $keyword = $request->input('keyword');
		$search_option = $request->input('search_option');
		
        $form_param = [
            'keyword' => $keyword,
			'search_option' => $search_option
		];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

         // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['inventory'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = (count($data['inventory']) == 0) ? 10 : count($data['inventory']);
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/inventory', [
            'inventory' => $paginatedItems ,
        ]);
    }


    public function inventory_export_excel(Request $request) { 

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/inventory/export/excel?inventory_export=".$request->get('inventory_export');

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        $headers = [
            'Bank',//bank_name
            'Campaign',//campaign_name
            'Item',//product_name
            'Ordered',//quantity
            'Total Redeemed',//total_redeemed
            'Balance',// qty - total_redeemed
        ];
        $data_array = array();
        //$a = 0;
        array_push($data_array, $headers); // excel header
        if (count($data['inventory']) > 0) {
            foreach( $data['inventory'] as $inventory)  {
                $each_row = [
                    'bank_name' => $inventory['bank_name'],
                    'campaign_name' => $inventory['campaign_name'],
                    'product_name' => $inventory['product_name'],
                    'quantity' => $inventory['quantity'],
                    'total_redeemed' => $inventory['total_redeemed'],
                    'balance' => $inventory['quantity'] - $inventory['total_redeemed']];
                array_push($data_array, $each_row);
                //$a++;
            } // foreach

            $filename = "inventory".date("ymdhis");
            $filename_with_ext = $filename.".xlsx";
            Excel::create($filename, function($excel) use ($data_array) {
                $excel->setTitle('Inventory');
                $sheetname = "Inventory";
                $excel->sheet($sheetname , function($sheet) use ($data_array) {
                    $from = "A1"; // or any value
                    $to = "F1"; // or any value
                    $sheet->getStyle("$from:$to")->getFont()->setBold( true );
                    $sheet->fromArray($data_array, null, 'A1', false, false);
                    //$sheet->protect('password123');
                });
            })->download();
        }
    }


    public function unusedUniqueCodesForCampaign(Request $request, $campaign_id) {
        $form_param = [
            'campaign_id' => $campaign_id,
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/unuseduniquecodesforcampaign";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/report/unuseduniquecodeforcampaign', [
            'unused_codes' => $data['unused_codes'] ,
        ]);
    }


    public function unused_unique_codes(Request $request) {
        $form_param = [
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/unused_unique_codes";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/report/unused_unique_code', [
            'unused_codes' => $data['unused_codes'] ,
        ]);
    }


    public function search_unused_codes(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/unused_unique_codes/search";

        $keyword = $request->input('keyword');
		$search_option = $request->input('search_option');
		
        $form_param = [
            'keyword' => $keyword,
			'search_option' => $search_option
		];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

         // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['unused_codes'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = (count($data['unused_codes']) == 0) ? 10 : count($data['unused_codes']);
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return view('admin/report/unused_unique_code', [
            'unused_codes' => $paginatedItems ,
        ]);
    }


    public function unused_unique_codes_export_excel(Request $request) { 

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/report/unused_unique_codes/export/excel";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        if ($data['status'] == 'success') { 
            $headers = [
                'Bank',//campaign_name
                'Campaign',//product_name
                'Number of Unutilised Codes',//quantity
            ];


            $data_array = array();
            //$a = 0;
            array_push($data_array, $headers); // excel header
            if (count($data['unused_codes']) > 0) {
                foreach( $data['unused_codes'] as $uc)  {
                    $each_row = [
                        'bank_name' => $uc['bank_name'],
                        'campaign_name' => $uc['campaign_name'],
                        'unused_qty' => $uc['unused_qty'],
                    ];
                    array_push($data_array, $each_row);
                    //$a++;
                } // foreach

                $filename = "unused_codes_".date("ymdhis");
                $filename_with_ext = $filename.".xlsx";
                Excel::create($filename, function($excel) use ($data_array) {
                    $excel->setTitle('Unutilised Codes');
                    $sheetname = "Unutilised Codes";
                    $excel->sheet($sheetname , function($sheet) use ($data_array) {
                        $from = "A1"; // or any value
                        $to = "G1"; // or any value
                        $sheet->getStyle("$from:$to")->getFont()->setBold( true );
                        $sheet->fromArray($data_array, null, 'A1', false, false);
                        //$sheet->protect('password123');
                    });
                })->download();

        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }

        }
    }

}
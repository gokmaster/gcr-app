<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\Mylib\HttpRequest;
use Illuminate\Support\Facades\DB;
use App\Mylib\Admin_id;
use App\Mylib\ExcelGenerate;
use App\Mylib\utils\CliCheck;

class ExcelController extends Controller
{   
    public function redemption_groupby_product(Request $request) 
    {
        $secretkey = $request->input('secretkey');

        if ( strcmp($secretkey, "eencvnm56123455vbnn") ===0 ) {
            //if the script is being accessed from cli
            $domain = Config::get('globalvariables.gcr_api'); 
            $url = $domain . "/api/v1/admin/report/redemption_groupby_product"; 
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->getJson($url);
    
            $redemption_date = $data['redemption_date'];
    
            $date_generated = date('Y-m-d H:i:s');
    
            $excel = new ExcelGenerate;
    
            $excel->redemption_groupby_product_excel_generate($data, $date_generated, $redemption_date);
        } else {
            //if the script is being accessed from the browser
            echo "Permission denied";
        }         
    } // function redemption_groupby_product


    public function send_email() {
        $data = array('name'=>"3ex");

        $email_model = new EmailSettings;
        $mail_from = strval($email_model->fetchSystemSenderEmail() ); 

        $mail_to = $email_model->fetchGeneralReceiverEmail();

        //Mail::to('work.gkpg@gmail.com')->send(new ExcelGeneratedMail);
     
        Mail::send('mail', $data, function($message) use($mail_from, $mail_to) {
           $message->to($mail_from , 'System Generated Email')->subject('ExcelFile Generated');
           $message->from($mail_to, '3ex');
        });
        echo "Excel file generated. Please check email.";
    }


}

?>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;
use App\Http\Middleware\Authenticate;
use App\Http\Middleware\PermissionCheck;
use Illuminate\Support\Facades\Redis;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Mylib\Admin_id;



class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(Authenticate::class);  // is user logged-in?
    }

    public function fetch_all(Request $request) {
       
        $form_param = [
        ];
		
        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/product/all"; 
                     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
    
        // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['products'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 10;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());
       
        return view('product/all_products',  ['products' => $paginatedItems]);
    }


    public function show_campaign_products($campaign_id, Request $request) {
		
        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/product/campaign_products/show/$campaign_id"; 
                     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);
        
        // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data['products'];
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
        // Define how many items we want to be visible in each page
        $perPage = 10;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());
       
        return view('product/campaign_products',  ['products' => $paginatedItems]);
    }


    public function add($campaign_id)
    {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/campaign/" . $campaign_id; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        return view('product/product_add',  ['campaign' => $data['campaign']]);
    }

    public function edit_form($product_id)
    {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/product/fetch/" . $product_id; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        return view('product/product_edit',  ['product' => $data['products'], 'productsOrdered' => $data['productsOrdered'] ]);
    }


    public function edit_post(Request $request) {
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
       
        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/product/update"; 
        
        $product_id =  $request->input('product_id');
        $product_name =  $request->input('product_name');
        $description =  $request->input('description');
        $sku =  $request->input('sku');
        $category =  $request->input('category');
        $price =  $request->input('price');
        $campaign_id =  $request->input('campaign_id');
        $quantityOrdered;
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'product_id' => $product_id,
            'product_name' => $product_name, 
            'description' => $description, 
            'sku' => $sku,
            'category' => $category,
            'price' => $price,
            'campaign_id' => $campaign_id,
        ];

        if(isset($request->quantity_ordered) && !empty($request->quantity_ordered)) {
            $quantityOrdered = $request->input('quantity_ordered');
            $form_param['quantity_ordered'] = $quantityOrdered;
        }

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        // image upload
        if (!$_FILES['imageupload']['size'] == 0) {
            // if file input not empty

            request()->validate([
                'imageupload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $product_id =  $data['product_id'];
            $imageName = time(). $product_id . '.'.request()->imageupload->getClientOriginalExtension();
            request()->imageupload->move(public_path('images'), $imageName);

            $original_filename = $_FILES['imageupload']['name'];

            $product_image_data = [
                'product_id' => $product_id,
                'original_filename' => $original_filename,
                'filepath' => 'images/'.$imageName,
            ];

            $this->insert_productimage_data($product_image_data);
        }
      
        if ($data['status'] == 'success') {
            Redis::del("gcr.product.campaign_products".$campaign_id );
            return redirect()->back()->with('success_message', $data['message'])->withInput(); 
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput(); 
        } 
    }


    public function add_post(Request $request) {
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
       
        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/product/insert"; 
     
        $product_name =  $request->input('product_name');
        $description =  $request->input('description');
        $sku =  $request->input('sku');
        $category =  $request->input('category');
        $price =  $request->input('price');
        $campaign_id =  $request->input('campaign_id');
        $quantity = $request->input('quantity');
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'product_name' => $product_name, 
            'description' => $description, 
            'sku' => $sku,
            'category' => $category,
            'price' => $price,
            'campaign_id' => $campaign_id,
            'quantity' => $quantity,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        
        if ($data['status'] == 'success') {
            Redis::del("gcr.product.campaign_products".$campaign_id );

            // image upload
            request()->validate([
                'imageupload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $product_id =  $data['product_id'];
            $imageName = time(). $product_id . '.'.request()->imageupload->getClientOriginalExtension();
            request()->imageupload->move(public_path('images'), $imageName);


            $original_filename = $_FILES['imageupload']['name'];

            $product_image_data = [
                'product_id' => $product_id,
                'original_filename' => $original_filename,
                'filepath' => 'images/'.$imageName,
            ];

            $this->insert_productimage_data($product_image_data);

            return redirect()->back()->with('success_message', $data['message'])->withInput(); 
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        } 
    }


    public function delete_confirm($product_id) {
	    $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/product/fetch/" . $product_id; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        return view('product/product_delete_confirm',  ['product' => $data['products'] ]);
	}
	
	public function delete_post(Request $request)
    { 
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $product_id = $request->input('product_id');
        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/product/delete"; 
                           
        $form_param = [
            'admin_id' => $admin_user_id,
            'product_id' => $product_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message']); 
        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }            
    }


    private function insert_productimage_data($data = array()) {

        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/product/insert/imagedata"; 
                     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $data);
    }
   
}
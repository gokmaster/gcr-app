<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;
use Session;
use Excel;
use File; 
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redis;



class CampaignFrontendController extends Controller
{
    protected $redis_expire_time = 9000;

    public function fetch($campaign_name) {
        $redis_expire_time = 19000;
        $domain = Config::get('globalvariables.gcr_api'); 
        $url_campaign = $domain . "/api/v1/campaign/name/" . $campaign_name; 

        $HttpReq = new HttpRequest;
        $campaignData = $HttpReq->getJson($url_campaign);

        $products;

        // check if Redis has key set
        // if key is found then it will return data without querying the database
        /* if ( $products_redis = Redis::get("gcr.product.campaign_products".$campaignData['campaign'][0]['campaign_id']) ) {
            $products = json_decode($products_redis , true);
        } else { */
            $url_products = $domain . "/api/v1/product/campaign_products/" .  $campaignData['campaign'][0]['campaign_id']; 
            $HttpReq = new HttpRequest;
            $productData = $HttpReq->getJson($url_products);
            $products = $productData['products'];

            // store data into redis for a limited time only
           /*  Redis::setex("gcr.product.campaign_products".$campaignData['campaign'][0]['campaign_id'] , $this->redis_expire_time , json_encode($products));
        } */

        if (strcmp($campaignData['campaign'][0]['deleted'], 1) === 0 ) {
            return abort(404);
        }

        $today = strtotime(date("Y-m-d H:i:s") );
        $startdate = strtotime($campaignData['campaign'][0]['start_date']);
        $enddate = strtotime($campaignData['campaign'][0]['end_date']);
            
        if ($startdate <= $today && $enddate >= $today) {
            return view('campaign/campaign_view',  
                ['campaign' => $campaignData['campaign'], 
                'products' => $products,
                ]
            );
        } else {
            return view('campaign/campaign_inactive');
        }
    }

   
}
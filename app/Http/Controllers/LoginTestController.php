<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;

class LoginTestController extends Controller
{
    public function index() {
        
        $auth = new \SimpleSAML\Auth\Simple('example-sql');

        $login_url = $auth->getLoginURL();

        if ( $auth->isAuthenticated() ) {
            // if logged in
            return redirect( action('AdminController@index') );
        } else {
            return redirect( $login_url );
        }

        //$attrs->requireAuth();
        //$attributes = $auth->getAttributes();       
    }


    public function logout() {
        
       /*  $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/admin/clear_admin_id"; 
                                  
        $form_param = [
            'admin_user_id' => "cleared",
        ];

        // call api to clear admin-id from redis
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param); */
        
        $auth = new \SimpleSAML\Auth\Simple('example-sql');
        $logoutUrl = $auth->getLogoutURL();

        return redirect( $logoutUrl );
    }


}

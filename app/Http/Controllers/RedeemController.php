<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input; 
use App\Http\Controllers\Controller;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;
use App\Model\EmailSettings;
use Config;
use Mail;

class RedeemController extends Controller {
    
    public function user_fill_form($campaign_id, $product_id) {   

        $domain = Config::get('globalvariables.gcr_api'); 
        $url_states = $domain . "/api/v1/address/states/all"; 

        $url_prod = $domain . "/api/v1/product/fetch/" . $product_id;
        $HttpReq = new HttpRequest;
        $productData = $HttpReq->getJson($url_prod);

        $url_campaign = $domain . "/api/v1/campaign/get_campaign_name/".$campaign_id; 
        $HttpReq = new HttpRequest;
        $data_campaign = $HttpReq->getJson($url_campaign);

        $campaign_name = $data_campaign['campaign']['name'];

        return view('redeem/user_fill_form', [
            'product_id' => $product_id , 
            'campaign_id' => $campaign_id , 
            'product' =>  $productData['products'],
            'campaign_name' => $campaign_name,
        ]);         
    }


    public function user_fill_form_2( $campaign_id, $product_id, $ic_code, $unique_code) {   

        $domain = Config::get('globalvariables.gcr_api'); 
        $url_states = $domain . "/api/v1/address/states/all"; 

        $url_prod = $domain . "/api/v1/product/fetch/" . $product_id;

        //$url_countries = $domain . "/api/v1/address/country/all"; 

        $HttpReq = new HttpRequest;
        $statesData = $HttpReq->getJson($url_states);

        $HttpReq = new HttpRequest;
        $productData = $HttpReq->getJson($url_prod);

        return view('redeem/user_fill_form_2', [
            'product_id' => $product_id , 
            'campaign_id' => $campaign_id , 
            'states' => $statesData['states'] , 
            'product' =>  $productData['products'],
            'ic_code' => $ic_code, 
            'unique_code' => $unique_code, 
        ]);         
    }


    public function redeem_post(Request $request) { 
                
        $domain = Config::get('globalvariables.gcr_api'); 
        
        $campaign_id = $request->input('campaign_id');
        $product_id = $request->input('product_id');
        $name = $request->input('name');
        $mobile_country_code = $request->input('mobile_country_code');
        $email = $request->input('email');
        $address1 = $request->input('address1');
        $address2 = $request->input('address2');
        $city =  $request->input('city');
        $state =  $request->input('state');
        $zipcode =  $request->input('zipcode');
        $country =  $request->input('country');
        $ic_code =  $request->input('ic_code');
        $unique_code =  $request->input('unique_code');
        $newsletter = $request->input('newsletter');

        /* $validatedData = $request->validate([
            'email' => 'required',
        ]); */

        $phonenumber = null;

        if($request->input('mobile') !== null){
            $phonenumber = $request->input('mobile');
            switch($phonenumber[0]){
                case '+' : 
                    $phonenumber = substr($phonenumber , 2);  // +60 123675 => 0 12 675
                break;

                case '1' :
                    $phonenumber = '0'.$phonenumber;
                break;
                
                case '6' :
                    $phonenumber = substr($phonenumber , 1);
                break;
                
                case '0' :
                    $phonenumber =  $phonenumber;
                break;
                default:
            }
    
        }
                      
        $form_param = [
            'campaign_id' => $campaign_id,
            'product_id' => $product_id, 
            'name' => $name,
            'email' => $email,
            'mobile' => $phonenumber, 
            'address1' => $address1,
            'address2' => $address2, 
            'city' => $city,
            'state' => $state, 
            'zipcode' => $zipcode,
            'country' => $country, 
            'ic_code' => $ic_code, 
            'unique_code' => $unique_code,
            'newsletter' => $newsletter,
        ];

        $url_campaign = $domain . "/api/v1/campaign/get_campaign_name/".$campaign_id; 
        $HttpReq = new HttpRequest;
        $data_campaign = $HttpReq->getJson($url_campaign);

        $campaign_name = $data_campaign['campaign']['name'];
       
        $url = $domain . "/api/v1/redeem/post"; 
        $HttpReq = new HttpRequest;
        $redeemData = $HttpReq->post($url , $form_param);

        $url_prod = $domain . "/api/v1/product/fetch/" . $product_id;
        $HttpReq = new HttpRequest;
        $productData = $HttpReq->getJson($url_prod);
                      
        if ( strcmp($redeemData['redeem_status'] ,'failed') === 0) {
            return redirect()->back()
                ->with('fail_message', $redeemData['message'] )
                ->with('error_ticket_no', $redeemData['error_ticket_no'] )
                ->withInput();
        } elseif ( strcmp($redeemData['redeem_status'] ,'success') === 0) {

            $sms_msg_text = "RM0 HSBC. Your redemption is successful! Your order reference is " . $redeemData['order_no'] . ". Please contact 03-8076 1313 for more information.";
			$subject = "Gift Redemption Successful - ".$redeemData['order_no'];
            $smslog_data = [
                'campaign_id' => $campaign_id,
                'sent_date' => date('Y-m-d H:i:s'),
                'name' => $name,
                'mobile_no' => $phonenumber, 
                'ic_creditcard_no' => $ic_code,
                'unique_code' => $unique_code, 
                'gift_code' => $redeemData['gift_code_id'], 
                'sms_content' => $sms_msg_text,
                'redemption_id' => $redeemData['redeem_id'], 
            ];

            //$this->send_success_sms($phonenumber , $sms_msg_text, $smslog_data);
			
			$dataEmail = [
                'user_data' => $redeemData['user_data'], 
                'order_no' => $redeemData['order_no'],
                'product' => $productData['products'], 
            ];

            $emailLogData = [
                'campaign_id' => $campaign_id,
                'date_redeemed' => $redeemData['date_redeemed'],
                'order_no' => $redeemData['order_no'],
                'customer_name' => $name,
                'contact_no' => $phonenumber,
                'email' => $email,
            ];

            $this->sendemail($email,$name,$subject, $dataEmail, $emailLogData);

            return view('redeem/redeem_success', [
                'user_data' => $dataEmail['user_data'], 
                'order_no' => $dataEmail['order_no'],
                'product' => $productData['products'],
                'campaign_name' => $campaign_name,
            ]); 
        }
    }

    private function sendemail($email,$name,$subject,  $dataEmail = array(), $form_param = array()) {

        $email_model = new EmailSettings;
        $mail_from = $email_model->fetchSystemSenderEmail();

        Mail::send('mail_redeem_success', $dataEmail, function($message) use($email,$name,$subject,$mail_from) {
            $message->to($email, $name)->subject($subject);
            $message->from($mail_from,'3ex');
        });

        $domain = Config::get('globalvariables.gcr_api'); 
        $url_emailsent = $domain . "/api/v1/redeem/emailsent/insert"; 

        $HttpReq = new HttpRequest;

        $emailLogStatus = $HttpReq->post($url_emailsent, $form_param);
    }


    public function validate_unique_code(Request $request) {  
        
        // Check if Google Recaptcha is required
        $g_captcha_required = $request->input('g-captcha-required');

        if ($g_captcha_required == 'yes' && isset($_POST['g-recaptcha-response'])) {
            $captcha=$_POST['g-recaptcha-response'];

            if(!$captcha){
                return redirect()->back()
                    ->with('fail_message', "Please check Recaptcha.")
                    ->withInput();
            }
            
            $secretKey = Config::get('globalvariables.google_recaptcha_secretkey');
            $ip = $_SERVER['REMOTE_ADDR'];
            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
            $responseKeys = json_decode($response,true);
    
            /* if (intval($responseKeys["success"]) !== 1) {
                return redirect()->back()
                    ->with('fail_message', "Invalid Recaptcha.");
            }  */
        }

        $domain = Config::get('globalvariables.gcr_api'); 
        
        $ic_code =  $request->input('ic_code');
        $unique_code =  $request->input('unique_code');
        $campaign_id = $request->input('campaign_id');
        $product_id = $request->input('product_id');
                      
        $form_param = [
            'campaign_id' => $campaign_id,
            'product_id' => $product_id, 
            'ic_code' => $ic_code, 
            'unique_code' => $unique_code,
        ];

        $url = $domain . "/api/v1/redeem/validate_unique_code"; 
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        if ($data['unique_code_validated'] == "no") {
            return redirect()->back()
            ->with('fail_message', $data['message'] )
            ->with('error_ticket_no', $data['error_ticket_no'] )
            ->withInput();
        } else {

            return redirect()->route('redeem.userform2', 
                [   'campaign_id' => $campaign_id , 
                    'product_id' => $product_id,
                    'ic_code' => $ic_code,
                    'unique_code' => $unique_code,
                ]);

        }

    }


    public function send_success_sms($to_mobile_number , $msg_text, $smslog_data) {

        $country_code = "60";
        $to_mobile_number_full = $country_code . $to_mobile_number;
        $url = Config::get('globalvariables.sms_api');
        
        $fields = array(
            'Gw-Username' => urlencode(Config::get('globalvariables.sms_username')),
            'Gw-Password' => urlencode(Config::get('globalvariables.sms_password')),
            'Gw-From' => urlencode(Config::get('globalvariables.sms_from')),
            'Gw-To' => urlencode($to_mobile_number_full),
            'Gw-Coding' => urlencode("1"),
            'Gw-Text' => urlencode($msg_text),
        );

        //url-ify the data for the POST
        $fields_string = '';
        foreach($fields as $key=>$value) { 
            $fields_string .= $key.'='.$value.'&'; 
        }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //execute post
        
        $result = curl_exec($ch);
        //$status = $_GET["status"];

        //close connection
        curl_close($ch);

        preg_match_all("/^([^=]+)=(.*)$/m", $result, $regs, PREG_SET_ORDER);

        $sms_response = [];
        foreach($regs as $reg) {
            $sms_response[$reg[1]] = $reg[2];
        }

        $sms_status_code = $sms_response['status'];

        //$sms_status_code = 999; // for testing

        $sms_status = "Fail";

        if ($sms_status_code == 0) {
            $sms_status = "sent";
        }

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/redeem/sms_sent/insert"; 

        $form_param = [
            'campaign_id' =>  $smslog_data['campaign_id'],
            'sent_date' => $smslog_data['sent_date'],
            'name' => $smslog_data['name'],
            'mobile_no' => $smslog_data['mobile_no'], 
            'ic_creditcard_no' => $smslog_data['ic_creditcard_no'],
            'unique_code' => $smslog_data['unique_code'], 
            'sms_status' => $sms_status,
            'gift_code' => $smslog_data['gift_code'], 
            'sms_content' => $smslog_data['sms_content'],
            'redemption_id' => $smslog_data['redemption_id'], 
        ];

        // record sms send status in database
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        //return $result;
    }

}
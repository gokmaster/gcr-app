<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;


class ImageUploadController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUpload($product_id)
    {
        return view('product/imageUpload', ['product_id' => $product_id ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUploadPost(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $product_id =  $request->input('product_id');

        $imageName = time(). $product_id . '.'.request()->image->getClientOriginalExtension();

        request()->image->move(public_path('images'), $imageName);

        return back()
            ->with('success','Successfully uploaded image.')
            ->with('image',$imageName);

    }

}
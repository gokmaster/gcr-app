<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\EmailSettings;
use App\Mylib\Admin_id;

class EmailSettingsController extends Controller {
   
    public function emailSettingsEditForm() {
       $model = new EmailSettings;
       $data = $model->fetchEmailSettings();

       return view('admin/systems/emailsettings',  ['emailsettings' => $data['email_settings'] ]);     
    }


    public function updateEmailSettings(Request $request) {
        
        $emailsettings_id =  $request->input('emailsettings_id');
        $email =  $request->input('email');
      
        $a = new Admin_id;
        $admin_id = $a->admin_id();

        $form_param = [
            'email_settings_id' => $emailsettings_id,
            'email' => $email, 
            'updated_by' => $admin_id,
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        $model = new EmailSettings;
        $data = $model->updateEmailSettings($form_param);
     
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }      
    }


    public function addGeneralReceiverEmail(Request $request) {
        $email =  $request->input('email');

        $a = new Admin_id;
        $admin_id = $a->admin_id();

        $form_param = [
            'email' => $email, 
            'email_type' => 'general-receiver-email',
            'updated_by' => $admin_id,
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        $model = new EmailSettings;
        $data = $model->insert($form_param);

        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }      
    }


    public function delete(Request $request) {
        
        $emailSettingsID = $request->input('emailsettings_id');

        $model = new EmailSettings;
        $data = $model->deleteEmailSettings($emailSettingsID);

        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }      
    }

}

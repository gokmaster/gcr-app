<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;
use App\Model\EmailSettings;
use Mail;

class ContactController extends Controller
{
    public function contact_form() {
        
        return view('contact/contact_us'); 
    }


    public function send_message(Request $request) {

        // Check if Google Recaptcha is required
        $g_captcha_required = $request->input('g-captcha-required');

        if (isset($_POST['g-recaptcha-response'])) {
            $captcha=$_POST['g-recaptcha-response'];

            if(!$captcha){
                return redirect()->back()
                    ->with('fail_message', "Please check Recaptcha.");
            }
            
            $secretKey = Config::get('globalvariables.google_recaptcha_secretkey');
            $ip = $_SERVER['REMOTE_ADDR'];
            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
            $responseKeys = json_decode($response,true);
    
           /*  if (intval($responseKeys["success"]) !== 1) {
                return redirect()->back()
                    ->with('fail_message', "Invalid Recaptcha.");
            }  */
        }

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/contact/send"; 
     
        $name =  $request->input('name');  
        $email =  $request->input('email'); 
        $receivedMessage =  $request->input('message');
                
        $form_param = [
            'name' => $name, 
            'email' => $email,
            'message' => $receivedMessage,
        ];

        $subject = "Message from customer: ". $name . " - ". $email;

        $email_model = new EmailSettings;
        $mail_from = $email_model->fetchSystemSenderEmail();
        $mail_to =  $email_model->fetchGeneralReceiverEmail();
        
        Mail::send('mail_contact_us', ["emaildata"=> $form_param] , function($message) use($mail_to,$name,$subject,$mail_from) {
            $message->to($mail_to , $name)->subject($subject);
            $message->from($mail_from,'3ex');
        });

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }   
    }

   

 
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use GuzzleHttp\Client;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Middleware\Authenticate;
use App\Model\EmailSettingsLog;
use App\Model\GiftcodeLog;
use App\Model\ProductLog;


class AuditLogController extends Controller
{

    public function __construct() {
        $this->middleware(Authenticate::class);  // is user logged-in?

    }

    public function campaign_logs(Request $request) { 

        $form_param = [];

        $url = Config::get('globalvariables.gcr_api') . "/api/v1/auditlog/campaign_log"; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/auditlogs/campaign_log',  ['logs' => $data['logs'] ]);              
    }


    public function product_logs(Request $request) { 

        $form_param = [];

        $url = Config::get('globalvariables.gcr_api') . "/api/v1/auditlog/product_log"; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/auditlogs/product_log',  ['logs' => $data['logs'] ]);              
    }


    public function bank_logs(Request $request) { 

        $form_param = [];

        $url = Config::get('globalvariables.gcr_api') . "/api/v1/auditlog/bank_log"; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
      
        
        return view('admin/auditlogs/bank_log',  ['logs' => $data['logs'] ]);              
    }


    public function role_logs(Request $request) { 

        $form_param = [];

        $url = Config::get('globalvariables.gcr_api') . "/api/v1/auditlog/role_log"; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/auditlogs/role_log',  ['logs' => $data['logs'] , 'userRolesLog' => $data['userRolesLog'] ]);              
    }


    public function manual_excel_generate_logs(Request $request) { 

        $form_param = [];

        $url = Config::get('globalvariables.gcr_api') . "/api/v1/auditlog/uploaded_file_log"; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/auditlogs/manual_excel_generate_log',  ['logs' =>  $data['logs']]);              
    }

    public function emailSettingsLog() {
        $model = new EmailSettingsLog;

        $data = $model->fetch();
        return view('admin/auditlogs/emailSettingsLog',  ['logs' =>  $data['logs']]);  
    }

    public function giftcodeLog() {
        $model = new GiftcodeLog;

        $data = $model->fetch();

        return view('admin/auditlogs/giftcodeLog',  [
            'logs' =>  $data['logs'], 
            'downloadlogs' => $data['downloadlogs'],
            'uploadlogs' => $data['uploadlogs'],
        ]);  
    }

    public function campaignProductLog() {
        $model = new ProductLog;

        $data = $model->fetchCampaignProductLog();

        return view('admin/auditlogs/campaign_product_log', [
            'logs' =>  $data, 
        ]);  
    }

      
   
}
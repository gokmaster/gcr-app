<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\Mylib\HttpRequest;
use Illuminate\Support\Facades\DB;
use Excel;
use Mail;
use App\Http\Middleware\Authenticate;
use App\Model\Unique_code;

class UniqueCodeController extends Controller
{
    public function __construct() {
        $this->middleware(Authenticate::class); // is user logged-in?
    }
    
    public function generate_excel_of_campaign_unique_codes($campaign_id) {

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/campaign_unique_codes/".$campaign_id; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        $headers = [
            'last_6_digit_of_credit_card',
            'unique_code',
        ];
       
        $data_array = array();
 
        //$a = 0;
        
        array_push($data_array, $headers); // excel header

        if (count($data['unique_codes']) > 0) {
            foreach( $data['unique_codes'] as $unique_code )  {
            
                $each_row = [
                    'ic_creditcard_no' => $unique_code['ic_code'],
                    'unique_code' => $unique_code['unique_code'],
                ];

                array_push($data_array, $each_row);
        
                //$a++;
            } // foreach

            $filename = "uc".date("ymd")."_". $campaign_id;
            $filename_with_ext = $filename.".xlsx";
                         
            Excel::create($filename, function($excel) use ($data_array) {
                $excel->setTitle('Unique codes');
                $sheetname = "unique_codes";
            
                $excel->sheet($sheetname , function($sheet) use ($data_array) {
                    $from = "A1"; // or any value
                    $to = "B1"; // or any value
                    $sheet->getStyle("$from:$to")->getFont()->setBold( true ); 
                    $sheet->fromArray($data_array, null, 'A1', false, false);
                    $sheet->protect('password123');
                });
            })->download();
        
        }
      
    } // function


    public function generateExcelOfCampaignUniqueCodesforOneBatch($campaign_id, $batch) {
        // log download
        $model = new Unique_code;
        $response = $model->uniquecodeDownloadLogInsert($campaign_id, $batch);
      
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/campaign_unique_codes/batch/".$campaign_id."/".$batch; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        $headers = [
            'last_6_digit_of_credit_card',
            'unique_code',
        ];
       
        $data_array = array();
 
        //$a = 0;
        
        array_push($data_array, $headers); // excel header

        if (count($data['unique_codes']) > 0) {
            foreach( $data['unique_codes'] as $unique_code )  {
            
                $each_row = [
                    'ic_creditcard_no' => $unique_code['ic_code'],
                    'unique_code' => $unique_code['unique_code'],
                ];

                array_push($data_array, $each_row);
            } // foreach

            $filename = "uc".date("ymd")."_". $campaign_id;
            $filename_with_ext = $filename.".xlsx";
                         
            Excel::create($filename, function($excel) use ($data_array) {
                $excel->setTitle('Unique codes');
                $sheetname = "unique_codes";
            
                $excel->sheet($sheetname , function($sheet) use ($data_array) {
                    $from = "A1"; // or any value
                    $to = "B1"; // or any value
                    $sheet->getStyle("$from:$to")->getFont()->setBold( true ); 
                    $sheet->fromArray($data_array, null, 'A1', false, false);
                    $sheet->protect('password123');
                });
            })->download();
        
        }
      
    } // function


    public function uniqueCodeDownloadPage($campaign_id) {
        $model = new Unique_code;

        $batches = $model->fetchApprovedBatchesOfCampaign($campaign_id);

        return view('campaign/download_unique_code',  
            ['batches' => $batches ]);    
    }


    public function excelUploadHistoryforCampaignPage($campaign_id) {
        $model = new Unique_code;

        $uploads = $model->excelUploadHistoryforCampaign($campaign_id);

        return view('campaign/excel_upload_history',  
            ['uploads' => $uploads ]);    
    }


}

?>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\Mylib\HttpRequest;
use Illuminate\Support\Facades\DB;
use App\Mylib\Admin_id;


class FileCheckController extends Controller
{   
    public function check_excel() {

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uploadedfile/check"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        return ($data); 
    } 


}

?>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use GuzzleHttp\Client;
use Session;
use Excel;
use File; 
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redis;
use App\Http\Middleware\Authenticate;
use App\Model\Unique_code;
use App\Mylib\utils\Paginate;

class CampaignController extends Controller
{

    public function __construct() {
        $this->middleware(Authenticate::class);  // is user logged-in?
    }

    public function create() { 
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/bank/all_without_check"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
    
        return view('campaign/create_campaign',  ['banks' => $data['banks'] ]);
    }


    public function add_unique_code_form($campaign_id, $campaign_name) { 

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];
    
        return view('campaign/add_unique_code', ['campaign_id' => $campaign_id , 'campaign_name' => $campaign_name] );
    }


    public function editUniqueCodeExpiry($campaign_id, $batch) {

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/detailsforcampaignbatch"; 
                     
        $form_param = [
            'campaign_id' => $campaign_id,
            'batch' => $batch,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        return view('campaign/edit_unique_code_expiry', ['details' => $data['details'] ] );
    }

    public function editUniqueCodeExpiryPost(Request $request) {

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/expiry_update";
        
        $campaign = $request->input('campaign');
        $campaign_id = $request->input('campaign_id');
                     
        $form_param = $request->all();

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
         
        if ( $data['status'] == "success") {
            return redirect()->route('campaign.approve_unique_code' , ['campaign_name' => $campaign , 'campaign_id' => $campaign_id ])
            ->with('success_message', $data['message']);
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput(); 
        }
       
    }

    public function add_unique_code_post(Request $request) { 
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();
      
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uniquecode/add_pending"; 
     
        //$campaign_id = $request->input('campaign_id');
        //$unique_code_qty =  $request->input('unique_code_qty');
                   
       /*  $form_param = [
            'admin_id' => $admin_user_id,
            'campaign_id' => $campaign_id,
            'unique_code_qty' => $unique_code_qty, 
        ]; */

        $form_param = $request->except('_token', 'expiry_startdate');

        if ($request->has('expiry_startdate')) {
            $startdate = $request->input('expiry_startdate');
            $startdate = date('Y-m-d', strtotime($startdate));
            $form_param['expiry_startdate'] =  $startdate;
        }

       
        $form_param['admin_id'] =  $admin_user_id;

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput(); 
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput(); 
        }
    }

    public function all(Request $request) { 

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];

        $url = Config::get('globalvariables.gcr_api') . "/api/v1/campaign/all"; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

       /*  $perPage = 10;
        
        $p = new Paginate;
        $paginatedItems = $p->makePaginate($data['campaigns'], $perPage, $request);
        $paginatedItems2 = $p->makePaginate($data['disabledCampaigns'], $perPage, $request);
        $expiredCampaigns = $p->makePaginate($data['expiredCampaigns'], $perPage, $request); */

        $today = strtotime(date("Y-m-d H:i:s") );

        return view('campaign/all_campaign',  [
            'campaigns' => $data['campaigns'], 
            'expiredCampaigns' =>   $data['expiredCampaigns'] ,
            'disabledCampaigns' => $data['disabledCampaigns'] , 
            'today' => $today]);              
    }

    public function edit($campaign_id)
    {     
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];
      
        $domain = Config::get('globalvariables.gcr_api');

        $url = $domain . "/api/v1/campaign/" . $campaign_id; 

        $HttpReq = new HttpRequest;
        $campaignData = $HttpReq->getJson($url);

        $url_bank = $domain . "/api/v1/bank/all_without_check";

        $HttpReq = new HttpRequest;
        $bankData = $HttpReq->post($url_bank, $form_param);

        return view('campaign/campaign_edit',  
            ['campaign' => $campaignData['campaign'], 
            'banks' => $bankData['banks'],
            'gcr_url' => $domain ]);    
    }

    public function edit_post(Request $request)
    {   
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
       
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/campaign/update"; 
     
        $campaign_id = $request->input('campaign_id');
        $title = $request->input('title');
        $description = $request->input('description');
        //$campaign_name =  $request->input('campaign_name');
        $campaign_provider = $request->input('campaign_provider');
        $start_date =  $request->input('start_date');
        $end_date =  $request->input('end_date');
        $reason = $request->input('reason');
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'campaign_id' => $campaign_id,
            'title' => $title,
            'description' => $description,
            //'name' => $campaign_name, 
            'bank_id' => $campaign_provider,
            'start_date' =>  $start_date,
            'end_date' => $end_date,
            'reason' => $reason,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            $url_products = $domain . "/api/v1/product/campaign_products/" .  $campaign_id; 
            $HttpReq = new HttpRequest;
            $productData = $HttpReq->getJson($url_products);
            $products = $productData['products'];

            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }
    }

    public function insert(Request $request) {
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/campaign/insert"; 
     
        $campaign_title =  $request->input('campaign_title');
        $campaign_name =  $request->input('campaign_name');
        $description =  $request->input('description');
        $campaign_provider =  $request->input('campaign_provider');
        $verification_type =  $request->input('verification_type');
        $start_date =  $request->input('start_date');
        $end_date =  $request->input('end_date');
        $uniquecodeType =  $request->input('unique-code-type');
        $uniquecodeLength =  $request->input('unique-code-length');
                        
        $form_param = [
            'admin_id' => $admin_user_id,
            'name' => $campaign_name, 
            'title' => $campaign_title,
            'description' => $description,
            'bank_id' => $campaign_provider,
            'verification_type' => $verification_type,
            'start_date' =>  $start_date,
            'end_date' => $end_date,
            'unique-code-type' => $uniquecodeType,
            'unique-code-length' => $uniquecodeLength,
        ];

        
        if ($verification_type == 1 || $verification_type == 3) {
            $unique_code_qty = $request->input('unique_code_qty');

            $form_param['unique_code_qty'] = $unique_code_qty;
        }

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'] )->withInput();

        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }              
    }

    public function delete_confirm($campaign_id)
    { 
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/campaign/" . $campaign_id; 
     
        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        return view('campaign/campaign_delete_confirm',  ['campaign' => $data['campaign'] ]);              
    }

    public function delete_post(Request $request) { 
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();

        $campaign_id = $request->input('campaign_id');
        $domain = Config::get('globalvariables.gcr_api');
        $url = $domain . "/api/v1/campaign/delete"; 
                           
        $form_param = [
            'admin_id' => $admin_user_id,
            'campaign_id' => $campaign_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message']); 
        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }            
    }


    public function upload_excel_form_bank_masterlist($campaign_name, $campaign_id, $verificationType) {

        if ( strcmp($verificationType, 3) === 0 ) {
            return view('campaign/upload_excel_bank_masterlist',  ['campaign_name' => $campaign_name , 'campaign_id' => $campaign_id] );
        } else {
            return view('campaign/upload_excel',  ['campaign_name' => $campaign_name , 'campaign_id' => $campaign_id] );
        }
        
    } 


    public function upload_excel(Request $request) {
        //validate the xls file
        $this->validate($request, array(
            'excelfile_upload' => 'required'
        ));
    
        if($request->hasFile('excelfile_upload')) {
            $extension = File::extension($request->excelfile_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
    
                $path = $request->excelfile_upload->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()) {
    
                    foreach ($data as $key => $value) {
                        $insert[] = [
                        'last_6_digit_of_credit_card' => $value->last_6_digit_of_credit_card,
                        ];
                    }

                    $domain = Config::get('globalvariables.gcr_api');
                    $url = $domain . "/api/v1/uniquecode/import_ic_codes"; 

                    $campaign_id = $request->input('campaign_id');
                                       
                    $form_param = [
                        'ic_codes_array' => $insert,
                        'campaign_id' => $campaign_id,
                    ];
            
                    $HttpReq = new HttpRequest;
                    $data = $HttpReq->post($url , $form_param);

                    if ($data['status'] == 'success') {
                        return redirect()->back()->with('success_message', $data['message']); 
                    } else {
                        return redirect()->back()->with('fail_message', $data['message']); 
                    }     
                }
                       
            } else {
                return redirect()->back()->with('fail_message', "Invalid file. Please upload .xlsx, .xls, or csv file"); 
            }
        }
    } // import_excel


    public function upload_excel_bank_masterlist(Request $request) {
        //validate the xls file
        $this->validate($request, array(
            'excelfile_upload' => 'required'
        ));
    
        if($request->hasFile('excelfile_upload')) {
            $extension = File::extension($request->excelfile_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
    
                $path = $request->excelfile_upload->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()) {

                    $campaign_id = $request->input('campaign_id');

                    $filename = time(). mt_rand() . $campaign_id . '.'.request()->excelfile_upload->getClientOriginalExtension();
                    request()->excelfile_upload->move(storage_path('excel/bankmasterlist'), $filename);

                    foreach ($data as $key => $value) {
                        $insert[] = [
                        'last_6_digit_of_credit_card' => $value->last_6_digit_of_credit_card,
                        'unique_code' => $value->unique_code,
                        ];
                    }

                    // insert data from excel file to database
                    $model = new Unique_code;
                    $data = $model->importBankMasterlist($insert, $campaign_id, $filename);
                 
                    if ($data['status'] == 'success') {
                        return redirect()->back()->with('success_message', $data['message']); 
                    } else {
                        return redirect()->back()->with('fail_message', $data['message']); 
                    }     
                }
                       
            } else {
                return redirect()->back()->with('fail_message', "Invalid file. Please upload .xlsx, .xls, or csv file"); 
            }
        }
    } // import_excel


    public function unique_code_approve_form($campaign_name, $campaign_id) {
        $model = new Unique_code;

        $data = $model->unique_code_batches_of_campaign($campaign_id);
       
        return view('campaign/approve_unique_code',  ['batches' => $data['batches'] , 'campaign_id' => $campaign_id,  'campaign_name' => $campaign_name]);
    }


    public function unique_code_approve_post(Request $request) {
            
        $campaign_id = $request->input('campaign_id');
        $batch =  $request->input('batch');
        $unique_code_qty =  $request->input('unique_code_qty');
        $unique_codes_pending_id =  $request->input('unique_codes_pending_id');

        $model = new Unique_code;

        $data = $model->approve_unique_code_for_campaign($campaign_id, $batch, $unique_code_qty, $unique_codes_pending_id);

        return json_encode($data);
        /* if ($data['status'] == 'success') {
            //return redirect()->back()->with('success_message', $data['message']); 
            return $data['message'];
        } else {
            //return redirect()->back()->with('fail_message', $data['message']); 
            return $data['message'];
        }     */ 
    }


    public function uniqueCodeRejectPost(Request $request) {
            
        $unique_codes_pending_id =  $request->input('unique_codes_pending_id');

        $model = new Unique_code;

        $data = $model->rejectUniqueCode($unique_codes_pending_id);

        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message']); 
        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }     
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;



class MailController extends Controller {
   public function basic_email(){
      $data = array('name'=>"user444");

      $mail_from = Config::get('globalvariables.mail_from'); 

      Mail::to($email)
         ->cc(['name1@domain.com','name2@domain.com'])
         ->send(new document());
   
     /*  Mail::send('mail', $data, function($message) {
         $message->to('go2012.gp@gmail.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
         $message->from($mail_from,'user444');
      }); */
      echo "Basic Email Sent. Check your inbox.";
   }
   
   public function html_email(){
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('go2012.gp@gmail.com', 'Tutorials Point')->subject
            ('Laravel HTML Testing Mail');
         $message->from('work.gkpg@gmail.com','Virat Gandhi');
      });
      echo "HTML Email Sent. Check your inbox.";
   }
   public function attachment_email(){
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('go2012.gp@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
         $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
         $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
         $message->from('work.gkpg@gmail.com','Virat Gandhi');
      });
      echo "Email Sent with attachment. Check your inbox.";
   }
}
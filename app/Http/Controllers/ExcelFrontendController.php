<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\Mylib\HttpRequest;
use Illuminate\Support\Facades\DB;
use App\Mylib\Admin_id;
use App\Http\Middleware\Authenticate;
use App\Mylib\ExcelGenerate;

class ExcelFrontendController extends Controller
{   
    public function __construct() {
        $this->middleware(Authenticate::class); // is user logged-in?
    }

    
    public function manual_excel_generate_redemption_groupby_product() {

        return view('admin/manual_excel_generate'); 
    }


    public function manual_excel_generate_redemption_groupby_product_post(Request $request) {
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();

        $for_date = strtotime($request->input('end_date'));
        $for_date = date('Y-m-d', $for_date);
        
        $form_param = [
            'admin_id' => $admin_user_id,
            'end_date' => $for_date,
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/excel/manual_generate_redemption_groupby_product";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        $redemption_date = $data['redemption_date'];

        $excel = new ExcelGenerate;
        //$this->send_mail();

        $excel->redemption_groupby_product_excel_generate($data,  $for_date, $redemption_date);

        return redirect()->back()->with('success_message', "Generated Excel File");
    }


    public function excel_download($upload_file_id , $filename) {
        //custom logic
        //check if user is logged in or user have permission to download this file etc
        $path = "excel/redemption_groupby_product" .  "/" . $filename;

        $this->logExcelDownload($upload_file_id);
    
        return response()->download(
            storage_path($path)
        );
    }

    // Log everytime user downloads excel file
    private function logExcelDownload($uploaded_file_id) {
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();

        $form_param = [
            'admin_id' => $admin_user_id,
            'uploaded_file_id' => $uploaded_file_id,
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/excel-download/log-insert";

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);
    }


    public function uploadedBankMasterlistExcelDownload($filename) {
        //custom logic
        //check if user is logged in or user have permission to download this file etc
        $path = "excel/bankmasterlist" .  "/" . $filename;
    
        return response()->download(
            storage_path($path)
        );
    }


    public function send_email() {
        $data = array('name'=>"3ex");

        $email_model = new EmailSettings;
        $mail_from = strval($email_model->fetchSystemSenderEmail() ); 

        $mail_to = $email_model->fetchGeneralReceiverEmail();

        //Mail::to('work.gkpg@gmail.com')->send(new ExcelGeneratedMail);
     
        Mail::send('mail', $data, function($message) use($mail_from, $mail_to) {
           $message->to($mail_from , 'System Generated Email')->subject('ExcelFile Generated');
           $message->from($mail_to, '3ex');
        });
        echo "Excel file generated. Please check email.";
    }
}

?>
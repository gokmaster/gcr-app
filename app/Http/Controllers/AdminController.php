<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Middleware\Authenticate;
use App\Model\Redemption;
use App\Model\Users;


class AdminController extends Controller
{
    
    public function __construct() {
        $this->middleware(Authenticate::class); // is user logged-in?
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $model = new Redemption;
        $count_redemption = $model->count_of_redemption_today();
        return view('admin/home' , [
            'count_redemption' => $count_redemption,
        ]);  
    }


    public function permission_denied() {
        return view('admin/permission_denied');  
    }


    public function profile() {
        $model = new Users;

        $user = $model->loggedInUser();

        return view('admin/profile' , [
            'user' => $user,
        ]); 
    } 
}

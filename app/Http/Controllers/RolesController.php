<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Config;
use App\Mylib\HttpRequest;
use GuzzleHttp\Client;
use App\Http\Middleware\Authenticate;
use App\Mylib\Admin_id;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Roles;
use App\Model\Users;


class RolesController extends Controller {

    public function __construct() {
        $this->middleware(Authenticate::class); // is user logged-in?
    }
    
    public function create_role_form() {  
        
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $url = $domain . "/api/v1/admin/roles/tasks"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->getJson($url);

        return view('admin/roles/create_role', [
            'tasks' => $data['tasks'] ,
        ]);         
    }

    public function insert(Request $request) {   

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/insert"; 
     
        $role_name =  $request->input('role_name');
        $tasks =  $request->input('tasks');
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'role_name' => $role_name, 
            'tasks' => $tasks,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput(); 
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput(); 
        }  
       
    }

    public function view_role_tasks(Request $request,$role_id) { 
        
        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
         
        $url2 = $domain . "/api/v1/admin/roles/one"; 

        $form_param = [
            'id' => $role_id,
        ];

        $HttpReq = new HttpRequest;
        $data2 = $HttpReq->post($url2, $form_param);

        return view('admin/roles/view_role_tasks', [
            'tasks' => $data2['roles'] ,
            'selected' => $data2['role'] ,
            'role_id' => $role_id
        ]);         
    }

    public function edit_role_form(Request $request,$role_id) { 
        
        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
         
        $url2 = $domain . "/api/v1/admin/roles/one"; 

        $form_param = [
            'id' => $role_id,
        ];

        $HttpReq = new HttpRequest;
        $data2 = $HttpReq->post($url2, $form_param);

        return view('admin/roles/edit_role', [
            'tasks' => $data2['roles'] ,
            'selected' => $data2['role'] ,
            'role_id' => $role_id
        ]);         
    }

    public function edit_role_post(Request $request) {  
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/update"; 
     
        $role_name =  $request->input('role_name');
        $tasks =  $request->input('tasks');
        $role_id =  $request->input('role_id');
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'role_name' => $role_name, 
            'tasks' => $tasks,
            'role_id' => $role_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }  
       
    }

    public function user_role_assign_form($user_id, Request $request) {  

        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];
        
        $userdomain = Config::get('globalvariables.user_api'); // gcr_api is defined in app/config/globalvariables.php
        $user_url = $userdomain . "/api/v1/users/all"; 

        $HttpReq = new HttpRequest;
        $userdata = $HttpReq->getJson($user_url);

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $roles_url = $domain . "/api/v1/admin/roles/all"; 

        $HttpReq = new HttpRequest;
        $rolesdata = $HttpReq->post($roles_url, $form_param);

        return view('admin/roles/assign_user_role', [
            'users' => $userdata['users'],
            'roles' => $rolesdata['roles'],
            'user_id' => $user_id,
        ]);     
    }


    public function user_role_edit_form($user_id, $role_id, Request $request) { 
        
        $admin = new Admin_id;
        $admin_user_id = $admin->fetch_admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];
        
        $userdomain = Config::get('globalvariables.user_api'); // gcr_api is defined in app/config/globalvariables.php
        $user_url = $userdomain . "/api/v1/users/all"; 

        $HttpReq = new HttpRequest;
        $userdata = $HttpReq->getJson($user_url);

        $domain = Config::get('globalvariables.gcr_api'); // gcr_api is defined in app/config/globalvariables.php
        $roles_url = $domain . "/api/v1/admin/roles/all"; 

        $model = new Roles;
        $rolechanges = $model->last5ChangesforUser($user_id);

        $HttpReq = new HttpRequest;
        $rolesdata = $HttpReq->post($roles_url, $form_param);

        return view('admin/roles/edit_user_role', [
            'users' => $userdata['users'],
            'roles' => $rolesdata['roles'],
            'roleChanges' => $rolechanges,
            'user_id' => $user_id,
            'role_id' => $role_id,
        ]);     
    }


    public function user_role_assign_post(Request $request) {  

        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();
               
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/assign_user_role"; 
     
        $role_id =  $request->input('role');
        $user_id =  $request->input('username');
                
        $form_param = [
            'admin_id' => $admin_user_id,
            'role_id' => $role_id, 
            'user_id' => $user_id,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
        
        if ($data['status'] == 'success') {
            return redirect()
                ->back()
                //->route('user_role.edit', [ 'user_id' => $user_id , 'role_id' => $role_id ] )
                ->with('success_message', $data['message'])->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }  
    }


    public function unassignUserRolePost(Request $request) {  

        $userId = $request->input('user_id');

        $model = new Roles;
        $data = $model->unassignUserRole($userId);
        
        if ($data['status'] == 'success') {
            return redirect()
                ->back()
                ->with('success_message', $data['message'])
                ->with('assign_success', "success")
                ->withInput();
        } else {
            return redirect()->back()->with('fail_message', $data['message'])->withInput();
        }  
    }


    public function view_all_user_roles(Request $request) {
        $form_param = [
        ];

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/view_user_roles_map"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);

        $model = new Users;

        $all_users = $model->users_not_unassigned_roles( $data['user_roles'] );

        return view('admin/roles/user_roles', [
            'user_roles' => $data['user_roles'] ,
            'unasigned_users' => $all_users,
        ]); 
       
    }

    public function view_all_roles(Request $request){
        
        $form_param = [
        ];

        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/admin/roles/all"; 

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $form_param);

        return view('admin/roles/list_roles', [
            'roles' => $data['roles']
        ]); 
    }


    public function delete_role_form($role_name , $role_id) {  
        return view('admin/roles/delete_role',  ['role_id' => $role_id , 'role_name' => $role_name ] );
    }


    public function delete_role_post(Request $request) { 

        $role_id =  $request->input('role_id');
        
        $model = new Roles;

        $data = $model->delete_role($role_id);

        if ($data['status'] == 'success') {
            return redirect()->back()->with('success_message', $data['message']); 
        } else {
            return redirect()->back()->with('fail_message', $data['message']); 
        }  
    }


    public function fetch_users_of_role($role_name , $role_id) {

        $model = new Roles;

        $data = $model->fetch_users_of_role($role_id);

        return view('admin/roles/users_of_role',  ['role_users' => $data['role_users'], 'role_id' => $role_id , 'role_name' => $role_name ] );
    }

    
}
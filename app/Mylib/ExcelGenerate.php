<?php

namespace App\Mylib;

use Excel;
use Mail;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mylib\HttpRequest;
use App\Mylib\Admin_id;
use App\Mylib\ExcelGenerate;
use App\Model\EmailSettings;
use App\Mail\ExcelGeneratedMail;

class ExcelGenerate
{
    public function redemption_groupby_product_excel_generate($data = array() , $date_generated, $redemption_date) {
        $redemption = $data['redemptions'];

        $headers = [
            'No',
            'Batch No',
            'Customer Name',
            'Last 6-digit credit-card/IC',
            'Address 1',
            'Address 2',
            'Address 3',
            'Address 4',
            'Postcode',
            'City',
            'State',
            'Home No',
            'Office No',
            'Mobile No',
            'SKU'
        ];
       
        $redeem_array = array();
        $sku_array = array();

        $a = 0; // product count
        $total_row_count = 0;

        //if (count($data['redemptions']) > 0) {
            foreach( $data['redemptions'] as $product )  {

                $b = 1; // how many redemptions for each product
                $redeem_array[$a] = array();
    
                array_push($redeem_array[$a], $headers); // excel header
    
                foreach(  $product as $redeemed )  {
    
                    $each_redeem = [
                        'no' => $b,
                        'batch_no' => 1,
                        'customer' =>  $redeemed['name'] . " " . $redeemed['order_no'],
                        'ic_creditcard_no' => $redeemed['ic_creditcard_no'],
                        'address1' => $redeemed['address1'],
                        'address2' => $redeemed['address2'],
                        'address3' => "",
                        'address4' => "",
                        'zip_code' => $redeemed['zip_code'],
                        'city' => $redeemed['city'],
                        'state' => $redeemed['state'],
                        'home_no' => "",
                        'office_no' => "",
                        'mobile' => $redeemed['mobile'],
                        'sku' => $redeemed['sku'],
                    ];
    
                    // These are structured arrays meaning their keys corresspond with each other 
                    $sku_array[$a] = $redeemed['sku'];
                    array_push($redeem_array[$a], $each_redeem);
    
                    $b++;
                    $total_row_count++;
                } // inner foreach
    
                $a++;
            } // outer foreach

            $batch_no = 1;

            $filename = "GCR".date("ymd_his")."_". $batch_no;
            $filename_with_ext = $filename.".xlsx";
                         
            Excel::create($filename, function($excel) use ($redeem_array, $sku_array) {

                $excel->setTitle('Redemptions');
    
                $a = 0; // product count
    
                foreach ($redeem_array as $product) {
    
                    //$sheetname = $sku_array[$a];
                    $sheetname = "Sheet-$a";
              
                    $excel->sheet($sheetname , function($sheet) use ($product) {
                        $from = "A1"; // or any value
                        $to = "P1"; // or any value
                        $sheet->getStyle("$from:$to")->getFont()->setBold( true ); 
                        $sheet->fromArray($product, null, 'A1', false, false);
                        $sheet->protect('password123');
                    });
    
                    $a++;
                }
            })->store('xlsx', storage_path('excel/redemption_groupby_product'));

            $file_data;
            $email_status;

            try {
                //$this->send_email();
                $email_status = "success";
               
            } catch (\Exception $e) {
                $email_status = "fail";
            }

            $file_data = [
                'upload_date' => date('Y-m-d H:i:s'),
                'batch_no' => 1,
                'filename' => $filename_with_ext,
                'email_status' => $email_status,
                'email_time' => date('Y-m-d H:i:s'),
                'redemption_date' => $redemption_date,
                'excel_row_count' => $total_row_count,
            ];

            $a = new Admin_id;

            $admin_id = $a->admin_id();

            if ($admin_id !== null) {
                $file_data['updated_by'] = $admin_id;
                $file_data['created_by'] = $admin_id;
            }
           
            
            $this->add_file_upload_data($file_data);
         //} // if (count($data['redemptions']) > 0) 
    }

   /*  private function send_email() {
        $data = array('name'=>"3ex");

        $email_model = new EmailSettings;
        $mail_from = strval($email_model->fetchSystemSenderEmail() ); 

        $mail_to = $email_model->fetchGeneralReceiverEmail();

        //Mail::to('work.gkpg@gmail.com')->send(new ExcelGeneratedMail);
     
        Mail::send('mail', $data, function($message) use($mail_from, $mail_to) {
           $message->to($mail_from , 'System Generated Email')->subject('ExcelFile Generated');
           $message->from($mail_to, '3ex');
        });
        echo "Excel file generated. Please check email.";
    } */


    private function add_file_upload_data($data = array() ) {
        $domain = Config::get('globalvariables.gcr_api'); 
        $url = $domain . "/api/v1/uploadedfile/insert"; 
                             
        $form_param = [
            'data' => $data,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $form_param);
    }

    
  
}

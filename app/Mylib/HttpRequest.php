<?php

namespace App\Mylib;

use GuzzleHttp\Client;

class HttpRequest
{
    public function getJson($url) {
        $auth = new \SimpleSAML\Auth\Simple('example-sql');

        $client = new Client;

        $header =  [
            'X-XSRF-TOKEN' => csrf_token(),
        ];

        if ( $auth->isAuthenticated() ) {
            $login_data = $auth->getAttributes();
            $user_id = $login_data['id'];
           
            $header =  [
                'X-XSRF-TOKEN' => csrf_token(),
                'x-admin-user-id' => $user_id,
            ];
        }

        $response = $client->request('GET', $url , [
            'headers' => $header
        ]);

        return json_decode($response->getBody(), true);
    }


    public function getJson_param($url, $form_param) {
        $auth = new \SimpleSAML\Auth\Simple('example-sql');

        $client = new Client;

        $header =  [
            'X-XSRF-TOKEN' => csrf_token(),
        ];

        if ( $auth->isAuthenticated() ) {
            $login_data = $auth->getAttributes();
            $user_id = $login_data['id'];
           
            $header =  [
                'X-XSRF-TOKEN' => csrf_token(),
                'x-admin-user-id' => $user_id,
            ];
        }

        $response = $client->request('GET', $url , [
            'headers' => $header,
            'form_params' => $form_param,
        ]);

        return json_decode($response->getBody(), true);
    }


    public function post($url , $form_param) {
        $auth = new \SimpleSAML\Auth\Simple('example-sql');

        $client = new Client;

        $header =  [
            'X-XSRF-TOKEN' => csrf_token(),
        ];

        if ( $auth->isAuthenticated() ) {
            $login_data = $auth->getAttributes();
            $user_id = $login_data['id'];
           
            $header =  [
                'X-XSRF-TOKEN' => csrf_token(),
                'x-admin-user-id' => $user_id,
            ];
        }

        $response = $client->request('POST', $url , [
            'headers' => $header,
            'form_params' => $form_param,
        ]);
   
        return json_decode($response->getBody(), true);
    }

    
  
}

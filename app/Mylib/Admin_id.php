<?php

namespace App\Mylib;

use App\Mylib\Encrypt;

class Admin_id {

    function fetch_admin_id() {
        $auth = new \SimpleSAML\Auth\Simple('example-sql');

        if ( $auth->isAuthenticated() ) {
            $login_data = $auth->getAttributes();
            $admin_user_id = $login_data['id'];
    
            $crypt = new Encrypt;
            $admin_user_id = $crypt->my_simple_crypt( $admin_user_id , 'e' );
        
            return $admin_user_id;
        }
        
    }


    function admin_id() {
        $auth = new \SimpleSAML\Auth\Simple('example-sql');

        if ( $auth->isAuthenticated() ) {
            $login_data = $auth->getAttributes();
            $admin_user_id = $login_data['id'];
            
            return $admin_user_id;
        } 

        return null;
        
    }
}
 
 
 
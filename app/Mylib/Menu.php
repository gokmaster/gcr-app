<?php

namespace App\Mylib;

use GuzzleHttp\Client;
use App\Mylib\Admin_id;
use Config;

class Menu {
    public function MenuItems(){
        $admin = new Admin_id;
        $admin_user_id = $admin->admin_id();
        $form_param = [
            'admin_id' => $admin_user_id,
        ];

        $domain = Config::get('globalvariables.gcr_api');

        $HttpReq = new HttpRequest;
        $url = $domain . "/api/v1/admin/roles/menu"; 
        $menuData = $HttpReq->post($url, $form_param);

        return $menuData;
    }
}
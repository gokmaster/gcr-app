<?php

namespace App\Mylib\utils;

use Config;
use Illuminate\Pagination\LengthAwarePaginator;

class Paginate {

    public function makePaginate($data, $perPage, $request){
         // Pagination
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		$items = $data;
        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);
      
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());

        return $paginatedItems;
    }
}
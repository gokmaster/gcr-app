<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mylib\Menu;
use App\Model\Roles;
use App\Model\Users;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {      
       
        view()->composer('admin.layout', function($view) {
             // get menu for user's particular role and pass to main layout
            $m = new Menu;
            $data = $m->MenuItems();
            $menu = $data['menu'];

             // get user data and pass to main layout
            $m = new Users;
            $adminUser = $m->loggedInUser();

            $view->with('spData', array('menu' => $menu, 'adminUser' => $adminUser ));
        });

        // get an array of user's permitted tasks and pass to the views
        view()->composer('admin.bank.all_banks', function($view) {
            $m = new Menu;
            $data = $m->MenuItems();
            $menu = $data['menu'];

            $rolesModel = new Roles;
            $tasklist  = $rolesModel->usersPermittedTaskList();

            $view->with('spData', array('permittedtasks' => $tasklist));
        });

        view()->composer('product.all_products', function($view) {
            $m = new Menu;
            $data = $m->MenuItems();
            $menu = $data['menu'];

            $rolesModel = new Roles;
            $tasklist  = $rolesModel->usersPermittedTaskList();

            $view->with('spData', array('permittedtasks' => $tasklist));
        });

        
        view()->composer('product.campaign_products', function($view) {
            $m = new Menu;
            $data = $m->MenuItems();
            $menu = $data['menu'];

            $rolesModel = new Roles;
            $tasklist  = $rolesModel->usersPermittedTaskList();

            $view->with('spData', array('permittedtasks' => $tasklist));
        });

        view()->composer('campaign.all_campaign', function($view) {
            $m = new Menu;
            $data = $m->MenuItems();
            $menu = $data['menu'];

            $rolesModel = new Roles;
            $tasklist  = $rolesModel->usersPermittedTaskList();

            $view->with('spData', array('permittedtasks' => $tasklist));
        });

        view()->composer('admin.roles.list_roles', function($view) {
            $m = new Menu;
            $data = $m->MenuItems();
            $menu = $data['menu'];

            $rolesModel = new Roles;
            $tasklist  = $rolesModel->usersPermittedTaskList();

            $view->with('spData', array('permittedtasks' => $tasklist));
        });

        view()->composer('admin.roles.user_roles', function($view) {
            $m = new Menu;
            $data = $m->MenuItems();
            $menu = $data['menu'];

            $rolesModel = new Roles;
            $tasklist  = $rolesModel->usersPermittedTaskList();

            $view->with('spData', array('permittedtasks' => $tasklist));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
